﻿function OnLoad(executionContext) {
	var formContext = executionContext.getFormContext();
	formContext.getAttribute("tct_bankaccountnumber").addOnChange(ClearLinkserMessage)
	formContext.getAttribute("tct_branchcode").addOnChange(ClearLinkserMessage)
	formContext.getAttribute("tct_bankaccount").addOnChange(SetBranchCode)
	//formContext.getAttribute("customerid").addOnChange(SetPropertyType)
	//SetPropertyType(executionContext);
	//formContext.getAttribute("tct_alarmsameasabove").addOnChange(DisplayAlarmSection)
	//formContext.getAttribute("tct_customersignature").addOnChange(SetNewSignatureTextField)
}


function ClearLinkserMessage(executionContext) {
	Xrm.Page.data.entity.attributes.get("tct_linkservmessage").setValue(null);
}


function SetBranchCode(executionContext) {
	var formContext = executionContext.getFormContext();
	var branchCode = "";
	var bankName = formContext.getAttribute("tct_bankaccount").getValue();

	if (bankName != null) {
		switch (bankName) {
			case 1:
				branchCode = "632005"; //ABSA
				break;
			case 2:
				branchCode = "410506"; //Bank of Athens  
				break;
			case 3:
				branchCode = "462005"; //Bidvest Bank   
				break;
			case 4:
				branchCode = "470010"; //Capitec Bank   
				break;
			case 5:
				branchCode = "254005"; //FNB   
				break;
			case 6:
				branchCode = "580105"; //Investec Private Bank 
				break;
			case 7: 
				branchCode = "198765"; //Nedbank  
				break;
			case 8:
				branchCode = "460005"; // SA Post Bank (Post Office)  
				break;
			case 9:
				branchCode = "051001"; // Standard Bank
				break;
			default:
				branchCode = "";
		}

		formContext.getAttribute("tct_branchcode").setValue(branchCode);
	}
	else {
		formContext.getAttribute("tct_branchcode").setValue(null);
	}

}

function DisplayAlarmSection(executionContext) {
	var formContext = executionContext.getFormContext();

	var sameAddress = formContext.getAttribute("tct_alarmsameasabove").getValue();

	if (sameAddress == 0) {

		Xrm.Page.ui.tabs.get("service_level_agreement").sections.get("map_section").setVisible(true);
		Xrm.Page.ui.tabs.get("service_level_agreement").sections.get("ship_section").setVisible(true);

	}
	else {

		Xrm.Page.ui.tabs.get("service_level_agreement").sections.get("map_section").setVisible(false);
		Xrm.Page.ui.tabs.get("service_level_agreement").sections.get("ship_section").setVisible(false);
	}

}

function SetNewSignatureTextField(executionContext) {
	var formContext = executionContext.getFormContext();

	var newSignature;
	var signature = formContext.getAttribute("tct_customersignature").getValue();

	if (signature != null) {
		newSignature = signature.split(',')[1];
		//alert(newSignature);
		Xrm.Page.data.entity.attributes.get("tct_customersignaturetext").setValue(newSignature);
	}
	else {
		formContext.getAttribute("tct_customersignaturetext").setValue(null);
	}

}

function SetPropertyType(executionContext) {
	var formContext = executionContext.getFormContext();
	var CustomerField = formContext.getAttribute("customerid").getValue();

	if (CustomerField != null) {

		var EntityType = CustomerField[0].entityType;

		var CustomerGUID = CustomerField[0].id;
		CustomerGUID = CustomerGUID.replace("{", "");
		CustomerGUID = CustomerGUID.replace("}", "");

		Xrm.WebApi.online.retrieveRecord(EntityType, CustomerGUID,
			"?$select=tct_accounttype,_tct_territory_value,tct_idnumber,tct_premises,tct_vatno,tct_companyregistrationnumber,tct_ownershiptype,address1_postofficebox,address1_name,address1_line1,address1_line2,address1_line3,address1_city,address1_stateorprovince,address1_postalcode,address1_country,address1_longitude,address1_latitude").then(
				function success(result) {
					var IdNo = result["tct_idnumber"];
					var VatNo = result["tct_vatno"];
					var OwnershipType = result["tct_ownershiptype"];
					var CompanyRegNumber = result["tct_companyregistrationnumber"];
					var UnitNumber = result["address1_postofficebox"];
					var UnitName = result["address1_name"];
					var StreetAddress = result["address1_line1"];
					var Suburb = result["address1_line2"];
					var District = result["address1_line3"];
					var City = result["address1_city"];
					var Province = result["address1_stateorprovince"];
					var PostalCode = result["address1_postalcode"];
					var Country = result["address1_country"];
					var longitude = result["address1_longitude"];
					var latitude = result["address1_latitude"];
					var territoryId = result["_tct_territory_value"];
					var territoryName = result["_tct_territory_value@OData.Community.Display.V1.FormattedValue"];
					var territoryEntityName = result["_tct_territory_value@Microsoft.Dynamics.CRM.lookuplogicalname"];

					formContext.getAttribute("tct_idnumber").setValue(IdNo);
					formContext.getAttribute("tct_ownershiptype").setValue(OwnershipType);
					formContext.getAttribute("tct_companyregistrationnumber").setValue(CompanyRegNumber);
					formContext.getAttribute("tct_vatno").setValue(VatNo);
					formContext.getAttribute("shipto_fax").setValue(UnitNumber);
					formContext.getAttribute("shipto_name").setValue(UnitName);
					formContext.getAttribute("shipto_line1").setValue(StreetAddress);
					formContext.getAttribute("shipto_line2").setValue(Suburb);
					formContext.getAttribute("shipto_line3").setValue(District);
					formContext.getAttribute("shipto_city").setValue(City);
					formContext.getAttribute("shipto_stateorprovince").setValue(Province);
					formContext.getAttribute("shipto_postalcode").setValue(PostalCode);
					formContext.getAttribute("shipto_country").setValue(Country);
					formContext.getAttribute("tct_longitude").setValue(longitude);
					formContext.getAttribute("tct_latitude").setValue(latitude);
					formContext.getAttribute("tct_territory").setValue([{ id: territoryId, name: territoryName, entityType: territoryEntityName }]);


				},
				function (error) {
					Xrm.Utility.alertDialog(error.message);
				}
			);

	}
	else {
		//formContext.getAttribute("tct_propertytype").setValue(null);
		formContext.getAttribute("tct_idnumber").setValue(null);
		formContext.getAttribute("tct_ownershiptype").setValue(null);
		formContext.getAttribute("tct_companyregistrationnumber").setValue(null);
		formContext.getAttribute("tct_vatno").setValue(null);
		formContext.getAttribute("shipto_fax").setValue(null);
		formContext.getAttribute("shipto_name").setValue(null);
		formContext.getAttribute("shipto_line1").setValue(null);
		formContext.getAttribute("shipto_line2").setValue(null);
		formContext.getAttribute("shipto_line3").setValue(null);
		formContext.getAttribute("shipto_city").setValue(null);
		formContext.getAttribute("shipto_stateorprovince").setValue(null);
		formContext.getAttribute("shipto_postalcode").setValue(null);
		formContext.getAttribute("shipto_country").setValue(null);
		formContext.getAttribute("tct_longitude").setValue(null);
		formContext.getAttribute("tct_latitude").setValue(null);

	}

}

