﻿function OnLoad(executionContext) {
    var formContext = executionContext.getFormContext();
	formContext.getAttribute("tct_compaccountname").addOnChange(SetAccountName)

}
function SetAccountName(executionContext) {
	var formContext = executionContext.getFormContext();
	var CompAccountField = formContext.getAttribute("tct_compaccountname").getValue();

	if (CompAccountField != null && CompAccountField != "") {

		formContext.getAttribute("companyname").setValue(CompAccountField);
	}

}