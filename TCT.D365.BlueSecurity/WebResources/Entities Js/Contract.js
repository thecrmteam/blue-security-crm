﻿function OnLoad(executionContext) {
	var formContext = executionContext.getFormContext();
	formContext.getAttribute("tct_customer").addOnChange(SetPropertyType)
	formContext.getAttribute("tct_customersignature").addOnChange(SetNewSignatureTextField)
	//SetPropertyType(executionContext);

}

function SetPropertyType(executionContext) {
	var formContext = executionContext.getFormContext();
	var CustomerField = formContext.getAttribute("tct_customer").getValue();

	if (CustomerField != null) {

		var EntityType = CustomerField[0].entityType;

		var CustomerGUID = CustomerField[0].id;
		CustomerGUID = CustomerGUID.replace("{", "");
		CustomerGUID = CustomerGUID.replace("}", "");

		Xrm.WebApi.online.retrieveRecord(EntityType, CustomerGUID,
			"?$select=tct_accounttype,_tct_territory_value,tct_premises,address1_postofficebox,address1_name,address1_line1,address1_line2,address1_line3,address1_city,address1_stateorprovince,address1_postalcode,address1_country,address1_longitude,address1_latitude").then(
				function success(result) {
					var AccountType = result["tct_accounttype"];
					var Premises = result["tct_premises"];
					var UnitNumber = result["address1_postofficebox"];
					var UnitName = result["address1_name"];
					var StreetAddress = result["address1_line1"];
					var Suburb = result["address1_line2"];
					var District = result["address1_line3"];
					var City = result["address1_city"];
					var Province = result["address1_stateorprovince"];
					var PostalCode = result["address1_postalcode"];
					var Country = result["address1_country"];
					var longitude = result["address1_longitude"];
					var latitude = result["address1_latitude"];
					var territoryId = result["_tct_territory_value"];
					var territoryName = result["_tct_territory_value@OData.Community.Display.V1.FormattedValue"];
					var territoryEntityName = result["_tct_territory_value@Microsoft.Dynamics.CRM.lookuplogicalname"];
				
					formContext.getAttribute("tct_propertytype").setValue(AccountType);
					formContext.getAttribute("tct_premises").setValue(Premises);
					formContext.getAttribute("tct_buildingunitnumber").setValue(UnitNumber);
					formContext.getAttribute("tct_buildingunitname").setValue(UnitName);
					formContext.getAttribute("tct_streetaddress").setValue(StreetAddress);
					formContext.getAttribute("tct_suburb").setValue(Suburb);
					formContext.getAttribute("tct_district").setValue(District);
					formContext.getAttribute("tct_city").setValue(City);
					formContext.getAttribute("tct_province").setValue(Province);
					formContext.getAttribute("tct_postalcode").setValue(PostalCode);
					formContext.getAttribute("tct_country").setValue(Country);
					formContext.getAttribute("tct_longitude").setValue(longitude);
					formContext.getAttribute("tct_latitude").setValue(latitude);
					formContext.getAttribute("tct_sector").setValue([{ id: territoryId, name: territoryName, entityType: territoryEntityName }]);


				},
				function (error) {
					Xrm.Utility.alertDialog(error.message);
				}
			);

	}
	else {
		formContext.getAttribute("tct_propertytype").setValue(null);

	}

}

function SetNewSignatureTextField(executionContext) {
	var formContext = executionContext.getFormContext();

	var newSignature;
	var signature = formContext.getAttribute("tct_customersignature").getValue();

	if (signature != null) {
		newSignature = signature.split(',')[1];
		//alert(newSignature);
		Xrm.Page.data.entity.attributes.get("tct_customersignaturetext").setValue(newSignature);
	}
	else {
		formContext.getAttribute("tct_customersignaturetext").setValue(null);
	}

}