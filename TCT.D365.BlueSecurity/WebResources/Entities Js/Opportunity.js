﻿function OnLoad(executionContext) {
    var formContext = executionContext.getFormContext();
	formContext.getAttribute("customerid").addOnChange(SetAddress)

}

function SetAddress(executionContext) {
	var formContext = executionContext.getFormContext();
	var CustomerField = formContext.getAttribute("customerid").getValue();
	//var CustomerField = Xrm.Page.getAttribute("customerid").getValue(); 
	if (CustomerField != null) {

		var EntityType = CustomerField[0].entityType;

		var CustomerGUID = CustomerField[0].id;
		CustomerGUID = CustomerGUID.replace("{", "");
		CustomerGUID = CustomerGUID.replace("}", "");

		Xrm.WebApi.online.retrieveRecord(EntityType, CustomerGUID,
			"?$select=address1_postofficebox,address1_name,address1_line1,address1_line2, address1_line3,address1_city,address1_stateorprovince,address1_country,address1_postalcode,address1_latitude,address1_longitude,_tct_territory_value").then(
            function success(result) {
					var BuildingNumber = result["address1_postofficebox"];
					var BuildingName = result["address1_name"];
					var StreetAddress = result["address1_line1"];
					var Suburb = result["address1_line2"];
					var District = result["address1_line3"];
					var City = result["address1_city"];
					var Province = result["address1_stateorprovince"];
					var Country = result["address1_country"];
					var Postalcode = result["address1_postalcode"];
					var Latitude = result["address1_latitude"];
					var Longitude = result["address1_longitude"];
					var territoryId = result["_tct_territory_value"];
					var territoryName = result["_tct_territory_value@OData.Community.Display.V1.FormattedValue"];
					var territoryEntityName = result["_tct_territory_value@Microsoft.Dynamics.CRM.lookuplogicalname"];

					formContext.getAttribute("tct_buildingunitnumber").setValue(BuildingNumber);
					formContext.getAttribute("tct_buildingunitname").setValue(BuildingName);
					formContext.getAttribute("tct_streetaddress").setValue(StreetAddress);
					formContext.getAttribute("tct_suburb").setValue(Suburb);
					formContext.getAttribute("tct_district").setValue(District);
					formContext.getAttribute("tct_city").setValue(City);
					formContext.getAttribute("tct_province").setValue(Province);
					formContext.getAttribute("tct_country").setValue(Country);
					formContext.getAttribute("tct_postalcode").setValue(Postalcode);
					formContext.getAttribute("tct_latitude").setValue(Latitude);
					formContext.getAttribute("tct_longitude").setValue(Longitude);
					formContext.getAttribute("tct_sector").setValue([{ id: territoryId, name: territoryName, entityType: territoryEntityName }]);
			},
			function (error) {
				Xrm.Utility.alertDialog(error.message);
			}
		);

	}
	else {
		formContext.getAttribute("tct_buildingunitnumber").setValue(null);
		formContext.getAttribute("tct_buildingunitname").setValue(null);
		formContext.getAttribute("tct_streetaddress").setValue(null);
		formContext.getAttribute("tct_suburb").setValue(null);
		formContext.getAttribute("tct_district").setValue(null);
		formContext.getAttribute("tct_city").setValue(null);
		formContext.getAttribute("tct_province").setValue(null);
		formContext.getAttribute("tct_country").setValue(null);
		formContext.getAttribute("tct_postalcode").setValue(null);
		formContext.getAttribute("tct_latitude").setValue(null);
		formContext.getAttribute("tct_longitude").setValue(null);
		formContext.getAttribute("tct_sector").setValue(null);
	}

}