﻿function OnLoad(executionContext) {
	var formContext = executionContext.getFormContext();
	//formContext.getAttribute("tct_accountsubstatus").addOnChange(SetPropertyType)
	formContext.getAttribute("tct_bankaccountnumber").addOnChange(ClearLinkserMessage)
	formContext.getAttribute("tct_branchcode").addOnChange(ClearLinkserMessage)
	formContext.getAttribute("tct_bankaccount").addOnChange(SetBranchCode)
}


function ClearLinkserMessage(executionContext) {
	Xrm.Page.data.entity.attributes.get("tct_linkservmessage").setValue(null);
}


function SetBranchCode(executionContext) {
	var formContext = executionContext.getFormContext();
	var branchCode = "";
	var bankName = formContext.getAttribute("tct_bankaccount").getValue();

	if (bankName != null) {
		switch (bankName) {
			case 1:
				branchCode = "632005"; //ABSA
				break;
			case 2:
				branchCode = "410506"; //Bank of Athens  
				break;
			case 3:
				branchCode = "462005"; //Bidvest Bank   
				break;
			case 4:
				branchCode = "470010"; //Capitec Bank   
				break;
			case 5:
				branchCode = "254005"; //FNB   
				break;
			case 6:
				branchCode = "580105"; //Investec Private Bank 
				break;
			case 7:
				branchCode = "198765"; //Nedbank  
				break;
			case 8:
				branchCode = "460005"; // SA Post Bank (Post Office)  
				break;
			case 9:
				branchCode = "051001"; // Standard Bank
				break;
			default:
				branchCode = "";
		}

		formContext.getAttribute("tct_branchcode").setValue(branchCode);
	}
	else {
		formContext.getAttribute("tct_branchcode").setValue(null);
	}

}

function SetPropertyType(executionContext) {
	var formContext = executionContext.getFormContext();
	var AccSubstatus = formContext.getAttribute("tct_accountsubstatus").getValue();

	if (AccSubstatus != null) {
		if (AccSubstatus == 6) //cancelled
		{
			Xrm.Page.ui.tabs.get("tab_cancel").setVisible(true);
		}
		else {
			Xrm.Page.ui.tabs.get("tab_cancel").setVisible(false);
		}

		if (AccSubstatus == 7) //terminated
		{
			Xrm.Page.ui.tabs.get("tab_terminate").setVisible(true);
		}
		else {
			Xrm.Page.ui.tabs.get("tab_terminate").setVisible(false);
		}
	}
}