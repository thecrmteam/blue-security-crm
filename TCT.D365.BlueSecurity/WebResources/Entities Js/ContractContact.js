﻿function filterLookup() {
    //try {
    // validate customer field data
    if (Xrm.Page.getControl("tct_accountlookup") != null && Xrm.Page.getControl("tct_accountlookup") != undefined) {

        //alert(Xrm.Page.getControl("customerid"));
        //call filter method on pre search of lookup
        Xrm.Page.getControl("tct_contactlookup").addPreSearch(function () {
            //calling filter method
            addContactLookupFilter();
        });
    }
    //} catch (e) {
    //    //throw error
    //    throw new Error(e.Message);
    //}
}

// function to set filter

function addContactLookupFilter() {

    var functionName = "addContactLookupFilter";
    try {
        //get current record id
        var RecordId = Xrm.Page.data.entity.getId();

        //get customer field id
        var CustomerField = Xrm.Page.getAttribute("tct_accountlookup").getValue();

        //validate customer id
        if (CustomerField != null) {
            var customerId = CustomerField[0].id;
            //Prepare condition for filter
            fetchXml = "<filter type='and'><condition attribute='parentcustomerid' operator='eq' value='" + customerId + "'/></filter>";
            //Apply Filter to the lookup field
            Xrm.Page.getControl("tct_contactlookup").addCustomFilter(fetchXml);
        }

    } catch (e) {
        //throw error
        throw new Error(e.Message);
    }
}