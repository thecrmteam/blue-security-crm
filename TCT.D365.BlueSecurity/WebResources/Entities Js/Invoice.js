﻿function filterLookup() {
    //try {
        // validate customer field data
        if (Xrm.Page.getControl("customerid") != null && Xrm.Page.getControl("customerid") != undefined) {

            //alert(Xrm.Page.getControl("customerid"));
            //call filter method on pre search of lookup
            Xrm.Page.getControl("tct_workorder").addPreSearch(function () {
                //calling filter method
                addWorkOrderLookupFilter();
            });
        }
    //} catch (e) {
    //    //throw error
    //    throw new Error(e.Message);
    //}
}

// function to set filter

function addWorkOrderLookupFilter() {

    var functionName = "addWorkOrderLookupFilter";
    try {
        //get current record id
        var RecordId = Xrm.Page.data.entity.getId();

        //get customer field id
        var CustomerField = Xrm.Page.getAttribute("customerid").getValue(); 

        //validate customer id
        if (CustomerField != null) {
            var customerId = CustomerField[0].id;
            //Prepare condition for filter
            fetchXml = "<filter type='and'><condition attribute='tct_servicecustomer' operator='eq' value='" + customerId + "'/><condition attribute='msdyn_systemstatus' operator='eq' value='690970004' /></filter>";
            //Apply Filter to the lookup field
            Xrm.Page.getControl("tct_workorder").addCustomFilter(fetchXml);
        }

    } catch (e) {
        //throw error
        throw new Error(e.Message);
    }
}