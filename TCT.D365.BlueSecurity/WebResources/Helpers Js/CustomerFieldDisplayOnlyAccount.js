﻿var oContactFormCustomization = {

    setCustomerLookupToShowCompany: function (execContext) {

        var formContext = execContext.getFormContext();
        if (formContext.getControl("customerid")) {
            var company = formContext.getControl('customerid');

            if (company.getEntityTypes().length > 1) {
                company.setEntityTypes(['account']);
            }
        }
    }
};