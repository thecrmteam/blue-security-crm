﻿function OnLoad(executionContext) {
	var formContext = executionContext.getFormContext();
	formContext.getAttribute("customerid").addOnChange(SetAddress)

}

function SetAddress(executionContext) {
	var formContext = executionContext.getFormContext();
	var entity = formContext.data.entity.getEntityName();
	var CustomerField = formContext.getAttribute("customerid").getValue();
	//var CustomerField = Xrm.Page.getAttribute("customerid").getValue(); 
	if (CustomerField != null) {

		var EntityType = CustomerField[0].entityType;

		var CustomerGUID = CustomerField[0].id;
		CustomerGUID = CustomerGUID.replace("{", "");
		CustomerGUID = CustomerGUID.replace("}", "");

		Xrm.WebApi.online.retrieveRecord(EntityType, CustomerGUID,
			"?$select=tct_idnumber,tct_ownershiptype,tct_companyregistrationnumber,tct_vatno,address1_postofficebox,address1_name,address1_line1,address1_line2, address1_line3,address1_city,address1_stateorprovince,address1_country,address1_postalcode,address1_latitude,address1_longitude,_tct_territory_value").then(
				function success(result) {
					var IdNo = result["tct_idnumber"];
					var VatNo = result["tct_vatno"];
					var OwnershipType = result["tct_ownershiptype"];
					var CompanyRegNumber = result["tct_companyregistrationnumber"];

					var BuildingNumber = result["address1_postofficebox"];
					var BuildingName = result["address1_name"];
					var StreetAddress = result["address1_line1"];
					var Suburb = result["address1_line2"];
					var District = result["address1_line3"];
					var City = result["address1_city"];
					var Province = result["address1_stateorprovince"];
					var Country = result["address1_country"];
					var Postalcode = result["address1_postalcode"];
					var Latitude = result["address1_latitude"];
					var Longitude = result["address1_longitude"];
					var territoryId = result["_tct_territory_value"];
					var territoryName = result["_tct_territory_value@OData.Community.Display.V1.FormattedValue"];
					var territoryEntityName = result["_tct_territory_value@Microsoft.Dynamics.CRM.lookuplogicalname"];

					if (entity == "opportunity") {
						formContext.getAttribute("tct_buildingunitnumber").setValue(BuildingNumber);
						formContext.getAttribute("tct_buildingunitname").setValue(BuildingName);
						formContext.getAttribute("tct_streetaddress").setValue(StreetAddress);
						formContext.getAttribute("tct_suburb").setValue(Suburb);
						formContext.getAttribute("tct_district").setValue(District);
						formContext.getAttribute("tct_city").setValue(City);
						formContext.getAttribute("tct_province").setValue(Province);
						formContext.getAttribute("tct_country").setValue(Country);
						formContext.getAttribute("tct_postalcode").setValue(Postalcode);
						formContext.getAttribute("tct_latitude").setValue(Latitude);
						formContext.getAttribute("tct_longitude").setValue(Longitude);
						formContext.getAttribute("tct_sector").setValue([{ id: territoryId, name: territoryName, entityType: territoryEntityName }]);
					}

					else if (entity == "quote") {
						formContext.getAttribute("billto_fax").setValue(BuildingNumber);
						formContext.getAttribute("billto_name").setValue(BuildingName);
						formContext.getAttribute("billto_line1").setValue(StreetAddress);
						formContext.getAttribute("billto_line2").setValue(Suburb);
						formContext.getAttribute("billto_line3").setValue(District);
						formContext.getAttribute("billto_city").setValue(City);
						formContext.getAttribute("billto_stateorprovince").setValue(Province);
						formContext.getAttribute("billto_country").setValue(Country);
						formContext.getAttribute("billto_postalcode").setValue(Postalcode);
						formContext.getAttribute("tct_latitude").setValue(Latitude);
						formContext.getAttribute("tct_longitude").setValue(Longitude);
						formContext.getAttribute("tct_sector").setValue([{ id: territoryId, name: territoryName, entityType: territoryEntityName }]);
					}

					else if (entity == "salesorder") {
						formContext.getAttribute("tct_idnumber").setValue(IdNo);
						formContext.getAttribute("tct_ownershiptype").setValue(OwnershipType);
						formContext.getAttribute("tct_companyregistrationnumber").setValue(CompanyRegNumber);
						formContext.getAttribute("tct_vatno").setValue(VatNo);

						formContext.getAttribute("shipto_fax").setValue(BuildingNumber);
						formContext.getAttribute("shipto_name").setValue(BuildingName);
						formContext.getAttribute("shipto_line1").setValue(StreetAddress);
						formContext.getAttribute("shipto_line2").setValue(Suburb);
						formContext.getAttribute("shipto_line3").setValue(District);
						formContext.getAttribute("shipto_city").setValue(City);
						formContext.getAttribute("shipto_stateorprovince").setValue(Province);
						formContext.getAttribute("shipto_country").setValue(Country);
						formContext.getAttribute("shipto_postalcode").setValue(Postalcode);
						formContext.getAttribute("tct_latitude").setValue(Latitude);
						formContext.getAttribute("tct_longitude").setValue(Longitude);
						formContext.getAttribute("tct_territory").setValue([{ id: territoryId, name: territoryName, entityType: territoryEntityName }]);
					}


				},
				function (error) {
					Xrm.Utility.alertDialog(error.message);
				}
			);

	}
	else {

		if (entity == "opportunity") {
			formContext.getAttribute("tct_buildingunitnumber").setValue(null);
			formContext.getAttribute("tct_buildingunitname").setValue(null);
			formContext.getAttribute("tct_streetaddress").setValue(null);
			formContext.getAttribute("tct_suburb").setValue(null);
			formContext.getAttribute("tct_district").setValue(null);
			formContext.getAttribute("tct_city").setValue(null);
			formContext.getAttribute("tct_province").setValue(null);
			formContext.getAttribute("tct_country").setValue(null);
			formContext.getAttribute("tct_postalcode").setValue(null);
			formContext.getAttribute("tct_latitude").setValue(null);
			formContext.getAttribute("tct_longitude").setValue(null);
			formContext.getAttribute("tct_sector").setValue(null);
		}

		else if (entity == "quote") {
			formContext.getAttribute("billto_fax").setValue(null);
			formContext.getAttribute("billto_name").setValue(null);
			formContext.getAttribute("billto_line1").setValue(null);
			formContext.getAttribute("billto_line2").setValue(null);
			formContext.getAttribute("billto_line3").setValue(null);
			formContext.getAttribute("billto_city").setValue(null);
			formContext.getAttribute("billto_stateorprovince").setValue(null);
			formContext.getAttribute("billto_country").setValue(null);
			formContext.getAttribute("billto_postalcode").setValue(null);
			formContext.getAttribute("tct_latitude").setValue(null);
			formContext.getAttribute("tct_longitude").setValue(null);
			formContext.getAttribute("tct_sector").setValue(null);
		}

		else if (entity == "salesorder") {
			formContext.getAttribute("tct_idnumber").setValue(null);
			formContext.getAttribute("tct_ownershiptype").setValue(null);
			formContext.getAttribute("tct_companyregistrationnumber").setValue(null);
			formContext.getAttribute("tct_vatno").setValue(null);

			formContext.getAttribute("shipto_fax").setValue(null);
			formContext.getAttribute("shipto_name").setValue(null);
			formContext.getAttribute("shipto_line1").setValue(null);
			formContext.getAttribute("shipto_line2").setValue(null);
			formContext.getAttribute("shipto_line3").setValue(null);
			formContext.getAttribute("shipto_city").setValue(null);
			formContext.getAttribute("shipto_stateorprovince").setValue(null);
			formContext.getAttribute("shipto_country").setValue(null);
			formContext.getAttribute("shipto_postalcode").setValue(null);
			formContext.getAttribute("tct_latitude").setValue(null);
			formContext.getAttribute("tct_longitude").setValue(null);
			formContext.getAttribute("tct_territory").setValue(null);
		}

	}

}