﻿if (typeof (CrmJs) == "undefined") {
    CrmJs = { __namespace: true };
}
if (typeof (CrmJs.Sales) == "undefined") {
    CrmJs.Sales = { __namespace: true };
}

CrmJs.Sales.Map = {
    __namespace: true,
    polygons: [],
    polygonsSS: [],
    polygonsSS2: [],
    polygonsSS3: [],
    polygonsSS4: [],
    polygonsSS5: [],
    polygonsSS6: [],
    polygonsSS7: [],
    polygonsSS8: [],
    polygonsSS9: [],

    DrawBlueMap: function () {

        var map = new google.maps.Map(document.getElementById('map'), {
            center: { lat: -29.883333, lng: 31.049999 },
            zoom: 11
        });

        //var src = 'https://developers.google.com/maps/documentation/javascript/examples/kml/westcampus.kml';

        //var kmlLayer = new google.maps.KmlLayer(src, {
        //    suppressInfoWindows: true,
        //    preserveViewport: false,
        //    map: map
        //});

        //kmlLayer.addListener('click', function (event) {
        //    var content = event.featureData.infoWindowHtml;
        //    var testimonial = document.getElementById('capture');
        //    testimonial.innerHTML = content;
        //});

        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.RIGHT_TOP].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(-29, 31)
        });

        if (parent != null && parent.Xrm != null) {
            //var geocoder = new google.maps.Geocoder();
            //var address = parent.Xrm.Page.getAttribute("address1_line1").getValue();

            //if (address != null && address != "") {
            //    if (parent.Xrm.Page.getAttribute("address1_line2").getValue() != null)
            //        address += ", " + parent.Xrm.Page.getAttribute("address1_line2").getValue();

            //    address += ", " + parent.Xrm.Page.getAttribute("address1_line3").getValue();
            //    address += ", " + parent.Xrm.Page.getAttribute("address1_city").getValue();
            //    address += ", " + parent.Xrm.Page.getAttribute("address1_stateorprovince").getValue();
            //    address += ", " + parent.Xrm.Page.getAttribute("address1_postalcode").getValue();
            //    address += ", " + parent.Xrm.Page.getAttribute("address1_country").getValue();
            //    geocoder.geocode({ 'address': address }, function (results, status) {
            //        if (status == google.maps.GeocoderStatus.OK) {
            //            map.setCenter(results[0].geometry.location);
            //            map.setZoom(10);
            //            marker = new google.maps.Marker({
            //                map: map,
            //                position: results[0].geometry.location
            //            });
            //        } else {
            //            alert("Geocode was not successful for the following reason: " + status);
            //        }
            //    });
            //}

            CrmJs.Sales.Map.DrawSectors(map);
        }

        autocomplete.addListener('place_changed', function () {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            var streetNumber = place.address_components.filter(function (item) {
                return item.types.includes("street_number");
            });

            var streetName = place.address_components.filter(function (item) {
                return item.types.includes("route");
            });

            var premise = place.address_components.filter(function (item) {
                return item.types.includes("premise");
            });

            var address2 = place.address_components.filter(function (item) {
                return item.types.includes("locality");
            });

            var postalTown = place.address_components.filter(function (item) {
                return item.types.includes("postal_town");
            });

            var stateorprovince = place.address_components.filter(function (item) {
                return item.types.includes("administrative_area_level_1");
            });

            var city = place.address_components.filter(function (item) {
                return item.types.includes("administrative_area_level_2");
            });

            var suburb = place.address_components.filter(function (item) {
                return item.types.includes("sublocality");
            });

            var country = place.address_components.filter(function (item) {
                return item.types.includes("country");
            });

            var postalCode = place.address_components.filter(function (item) {
                return item.types.includes("postal_code");
            });

            //Set Sector 
            CrmJs.Sales.Map.SetSector(marker);

            //Set Address 
            CrmJs.Sales.Map.SetAddressFields(streetNumber, streetName, premise, address2, postalTown, stateorprovince, city, suburb, country, postalCode, place);

            infowindowContent.children['place-icon'].src = place.icon;
            infowindowContent.children['place-name'].textContent = place.name;
            infowindowContent.children['place-address'].textContent = address;
            infowindow.open(map, marker);

        });

        function showDetails(event) {
            var contentString =
                "<b>WINSTON PARK</b><br>" +
                "Clicked location: <br>" +
                event.latLng.lat() +
                "," +
                event.latLng.lng() +
                "<br>";
        }

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
            var radioButton = document.getElementById(id);
            radioButton.addEventListener('click', function () {
                autocomplete.setTypes(types);
            });
        }

        setupClickListener('changetype-all', []);
        setupClickListener('changetype-address', ['address']);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);

        document.getElementById('use-strict-bounds')
            .addEventListener('click', function () {
                console.log('Checkbox clicked! New state=' + this.checked);
                autocomplete.setOptions({ strictBounds: this.checked });
            });
    },

    DrawSectors: function (map) {

        //Blue Sectors
        var S1 = [
            { lat: -29.822998, lng: 30.809312 }, { lat: -29.817599, lng: 30.811801 }, { lat: -29.815476, lng: 30.81532 }, { lat: -29.811902, lng: 30.81811 }, { lat: -29.808402, lng: 30.823731 }, { lat: -29.805981, lng: 30.826607 }, { lat: -29.802965, lng: 30.831757 }, { lat: -29.801252, lng: 30.835619 }, { lat: -29.802071, lng: 30.845361 }, { lat: -29.803226, lng: 30.849094 }, { lat: -29.802667, lng: 30.85124 }, { lat: -29.800526, lng: 30.85154 }, { lat: -29.796112, lng: 30.850339 }, { lat: -29.794716, lng: 30.851862 }, { lat: -29.793692, lng: 30.852871 }, { lat: -29.793096, lng: 30.854287 }, { lat: -29.790768, lng: 30.854223 }, { lat: -29.78885, lng: 30.857377 }, { lat: -29.788515, lng: 30.861454 }, { lat: -29.781326, lng: 30.864415 }, { lat: -29.778235, lng: 30.867806 }, { lat: -29.776447, lng: 30.868063 }, { lat: -29.775627, lng: 30.871282 }, { lat: -29.777043, lng: 30.8739 }, { lat: -29.778272, lng: 30.876217 }, { lat: -29.779501, lng: 30.878277 }, { lat: -29.779576, lng: 30.881023 }, { lat: -29.779948, lng: 30.885057 }, { lat: -29.780507, lng: 30.888405 }, { lat: -29.77207, lng: 30.888276 }, { lat: -29.771902, lng: 30.890808 }, { lat: -29.769854, lng: 30.890808 }, { lat: -29.768224, lng: 30.890159 }, { lat: -29.768447, lng: 30.867972 }, { lat: -29.769751, lng: 30.859002 }, { lat: -29.769406, lng: 30.849052 }, { lat: -29.768829, lng: 30.848322 }, { lat: -29.767656, lng: 30.845361 }, { lat: -29.77153, lng: 30.842142 }, { lat: -29.775646, lng: 30.837593 }, { lat: -29.774175, lng: 30.835769 }, { lat: -29.774864, lng: 30.8324 }, { lat: -29.773169, lng: 30.829074 }, { lat: -29.771567, lng: 30.829053 }, { lat: -29.768587, lng: 30.826564 }, { lat: -29.76855, lng: 30.821071 }, { lat: -29.77045, lng: 30.814891 }, { lat: -29.772238, lng: 30.810943 }, { lat: -29.776, lng: 30.808496 }, { lat: -29.779464, lng: 30.808411 }, { lat: -29.78267, lng: 30.811801 }, { lat: -29.783114, lng: 30.81281 }, { lat: -29.785535, lng: 30.811973 }, { lat: -29.789222, lng: 30.807767 }, { lat: -29.790172, lng: 30.806265 }, { lat: -29.79129, lng: 30.805621 }, { lat: -29.792742, lng: 30.806243 }, { lat: -29.79399, lng: 30.797596 }, { lat: -29.793021, lng: 30.783176 }, { lat: -29.794362, lng: 30.780816 }, { lat: -29.797006, lng: 30.778542 }, { lat: -29.798924, lng: 30.778348 }, { lat: -29.800842, lng: 30.779743 }, { lat: -29.804566, lng: 30.781031 }, { lat: -29.807526, lng: 30.779389 }, { lat: -29.808923, lng: 30.777522 }, { lat: -29.812395, lng: 30.775996 }, { lat: -29.812772, lng: 30.774887 }, { lat: -29.816072, lng: 30.775409 }, { lat: -29.821062, lng: 30.778284 }, { lat: -29.824412, lng: 30.78515 }, { lat: -29.826348, lng: 30.792875 }, { lat: -29.822476, lng: 30.804033 }, { lat: -29.82203, lng: 30.807424 }
        ];
        var S1Map = new google.maps.Polygon({
            paths: S1,
            strokeColor: "#40E0D0",
            strokeOpacity: 0,
            strokeWeight: 0,
            fillColor: "",
            fillOpacity: 0,
            draggable: false,
            geodesic: false,
            name: 'S1',
            map: map
        });

        CrmJs.Sales.Map.polygons.push(S1Map);
        CrmJs.Sales.Map.DrawSubSector1(map);

        var S2 = [
            { lat: -29.813764, lng: 31.000156 }, { lat: -29.813428, lng: 30.995307 }, { lat: -29.809742, lng: 30.985222 }, { lat: -29.806451, lng: 30.977892 }, { lat: -29.802909, lng: 30.978742 }, { lat: -29.80263, lng: 30.972734 }, { lat: -29.8030559, lng: 30.968076 }, { lat: -29.806167, lng: 30.958014 }, { lat: -29.809332, lng: 30.958185 }, { lat: -29.810971, lng: 30.960674 }, { lat: -29.814397, lng: 30.959215 }, { lat: -29.819088, lng: 30.954494 }, { lat: -29.820466, lng: 30.955138 }, { lat: -29.820726, lng: 30.96179 }, { lat: -29.822141, lng: 30.967455 }, { lat: -29.826795, lng: 30.965781 }, { lat: -29.830071, lng: 30.961704 }, { lat: -29.84258, lng: 30.956554 }, { lat: -29.847158, lng: 30.955868 }, { lat: -29.848014, lng: 30.955181 }, { lat: -29.847902, lng: 30.957327 }, { lat: -29.847195, lng: 30.959558 }, { lat: -29.847568, lng: 30.960975 }, { lat: -29.846562, lng: 30.965395 }, { lat: -29.847828, lng: 30.969686 }, { lat: -29.850248, lng: 30.971532 }, { lat: -29.850229, lng: 30.972412 }, { lat: -29.850992, lng: 30.972948 }, { lat: -29.852183, lng: 30.970931 }, { lat: -29.854602, lng: 30.969772 }, { lat: -29.858473, lng: 30.969386 }, { lat: -29.861488, lng: 30.967584 }, { lat: -29.86279, lng: 30.965395 }, { lat: -29.863646, lng: 30.96518 }, { lat: -29.871908, lng: 30.974493 }, { lat: -29.872504, lng: 30.976424 }, { lat: -29.874253, lng: 30.974321 }, { lat: -29.875416, lng: 30.96989 }, { lat: -29.878281, lng: 30.968174 }, { lat: -29.878876, lng: 30.965685 }, { lat: -29.881779, lng: 30.962681 }, { lat: -29.89242, lng: 30.968903 }, { lat: -29.900261, lng: 30.977411 }, { lat: -29.903237, lng: 30.979557 }, { lat: -29.904167, lng: 30.981102 }, { lat: -29.907329, lng: 31.011829 }, { lat: -29.90147, lng: 31.016679 }, { lat: -29.896355, lng: 31.021786 }, { lat: -29.891667, lng: 31.027279 }, { lat: -29.885937, lng: 31.039638 }, { lat: -29.883518, lng: 31.046076 }, { lat: -29.876672, lng: 31.039724 }, { lat: -29.874476, lng: 31.027279 }, { lat: -29.873471, lng: 31.007967 }, { lat: -29.868829, lng: 31.012548 }, { lat: -29.868903, lng: 31.014501 }, { lat: -29.86641, lng: 31.017827 }, { lat: -29.865098, lng: 31.017644 }, { lat: -29.862642, lng: 31.022515 }, { lat: -29.862195, lng: 31.031227 }, { lat: -29.864837, lng: 31.033974 }, { lat: -29.870122, lng: 31.032386 }, { lat: -29.872094, lng: 31.034145 }, { lat: -29.868522, lng: 31.035476 }, { lat: -29.868559, lng: 31.035862 }, { lat: -29.874811, lng: 31.042728 }, { lat: -29.873285, lng: 31.044702 }, { lat: -29.874327, lng: 31.050754 }, { lat: -29.870345, lng: 31.054144 }, { lat: -29.868522, lng: 31.050925 }, { lat: -29.867666, lng: 31.048222 }, { lat: -29.865991, lng: 31.046033 }, { lat: -29.864875, lng: 31.045604 }, { lat: -29.860902, lng: 31.04305 }, { lat: -29.857431, lng: 31.041698 }, { lat: -29.854733, lng: 31.040819 }, { lat: -29.851066, lng: 31.039424 }, { lat: -29.843073, lng: 31.037386 }, { lat: -29.839937, lng: 31.037149 }, { lat: -29.834092, lng: 31.03672 }, { lat: -29.831086, lng: 31.037461 }, { lat: -29.827167, lng: 31.037793 }, { lat: -29.822784, lng: 31.038523 }, { lat: -29.822216, lng: 31.033888 }, { lat: -29.820205, lng: 31.033115 }, { lat: -29.817943, lng: 31.031399 }, { lat: -29.818837, lng: 31.027408 }, { lat: -29.814359, lng: 31.025476 }, { lat: -29.811902, lng: 31.022043 }, { lat: -29.811083, lng: 31.017151 }, { lat: -29.812423, lng: 31.014318 }, { lat: -29.813317, lng: 31.007194 }
        ];
        var S2Map = new google.maps.Polygon({
            paths: S2,
            strokeColor: "#006400",
            strokeOpacity: 0,
            strokeWeight: 0,
            fillColor: "",
            fillOpacity: 0,
            draggable: false,
            geodesic: false,
            name: 'S2',
            map: map
        });

        CrmJs.Sales.Map.polygons.push(S2Map);
        CrmJs.Sales.Map.DrawSubSector2(map);

        var S3 = [
            { lat: -29.943259, lng: 30.950804 }, { lat: -29.946457, lng: 30.954752 }, { lat: -29.939131, lng: 30.962133 }, { lat: -29.935077, lng: 30.965695 }, { lat: -29.929498, lng: 30.970159 }, { lat: -29.924217, lng: 30.974493 }, { lat: -29.915253, lng: 30.977755 }, { lat: -29.910677, lng: 30.979128 }, { lat: -29.903944, lng: 30.981188 }, { lat: -29.90346, lng: 30.979686 }, { lat: -29.900261, lng: 30.977411 }, { lat: -29.894457, lng: 30.971188 }, { lat: -29.89242, lng: 30.968903 }, { lat: -29.881779, lng: 30.962681 }, { lat: -29.879946, lng: 30.961018 }, { lat: -29.878867, lng: 30.954323 }, { lat: -29.879202, lng: 30.951834 }, { lat: -29.878718, lng: 30.948873 }, { lat: -29.873974, lng: 30.948379 }, { lat: -29.872076, lng: 30.946148 }, { lat: -29.873062, lng: 30.9372 }, { lat: -29.871201, lng: 30.934968 }, { lat: -29.869917, lng: 30.932436 }, { lat: -29.870178, lng: 30.928681 }, { lat: -29.868745, lng: 30.927114 }, { lat: -29.865358, lng: 30.918746 }, { lat: -29.864726, lng: 30.912223 }, { lat: -29.866103, lng: 30.909476 }, { lat: -29.865507, lng: 30.907588 }, { lat: -29.864614, lng: 30.907974 }, { lat: -29.861562, lng: 30.907631 }, { lat: -29.860074, lng: 30.907159 }, { lat: -29.859739, lng: 30.905142 }, { lat: -29.861078, lng: 30.90467 }, { lat: -29.862604, lng: 30.904627 }, { lat: -29.86227, lng: 30.901837 }, { lat: -29.859701, lng: 30.896473 }, { lat: -29.857431, lng: 30.898533 }, { lat: -29.854416, lng: 30.89819 }, { lat: -29.853449, lng: 30.895314 }, { lat: -29.849652, lng: 30.890594 }, { lat: -29.851699, lng: 30.87759 }, { lat: -29.854081, lng: 30.874844 }, { lat: -29.850657, lng: 30.870724 }, { lat: -29.853393, lng: 30.86714 }, { lat: -29.862604, lng: 30.868707 }, { lat: -29.860762, lng: 30.864286 }, { lat: -29.859967, lng: 30.860392 }, { lat: -29.861106, lng: 30.860102 }, { lat: -29.862067, lng: 30.860258 }, { lat: -29.862432, lng: 30.864018 }, { lat: -29.86454, lng: 30.869651 }, { lat: -29.865768, lng: 30.873256 }, { lat: -29.868968, lng: 30.871882 }, { lat: -29.870866, lng: 30.870037 }, { lat: -29.873322, lng: 30.868664 }, { lat: -29.875667, lng: 30.868535 }, { lat: -29.877192, lng: 30.867376 }, { lat: -29.878718, lng: 30.864329 }, { lat: -29.878793, lng: 30.862441 }, { lat: -29.879128, lng: 30.860982 }, { lat: -29.880132, lng: 30.85978 }, { lat: -29.880914, lng: 30.857892 }, { lat: -29.881211, lng: 30.855274 }, { lat: -29.882737, lng: 30.852957 }, { lat: -29.883183, lng: 30.852785 }, { lat: -29.884411, lng: 30.855017 }, { lat: -29.886011, lng: 30.85566 }, { lat: -29.888132, lng: 30.855789 }, { lat: -29.889174, lng: 30.855317 }, { lat: -29.893043, lng: 30.854073 }, { lat: -29.895945, lng: 30.853901 }, { lat: -29.898178, lng: 30.854845 }, { lat: -29.901005, lng: 30.855875 }, { lat: -29.90227, lng: 30.855789 }, { lat: -29.904279, lng: 30.855102 }, { lat: -29.908966, lng: 30.854845 }, { lat: -29.911979, lng: 30.856218 }, { lat: -29.915922, lng: 30.85712 }, { lat: -29.925072, lng: 30.8639 }, { lat: -29.928308, lng: 30.868921 }, { lat: -29.929275, lng: 30.878191 }, { lat: -29.929238, lng: 30.88819 }, { lat: -29.925668, lng: 30.890636 }, { lat: -29.929364, lng: 30.893506 }, { lat: -29.931795, lng: 30.897578 }, { lat: -29.934752, lng: 30.903136 }, { lat: -29.935588, lng: 30.90615 }, { lat: -29.936007, lng: 30.907245 }, { lat: -29.935784, lng: 30.913854 }, { lat: -29.933738, lng: 30.917544 }, { lat: -29.935003, lng: 30.921364 }, { lat: -29.939019, lng: 30.923681 }, { lat: -29.934482, lng: 30.928917 }, { lat: -29.930681, lng: 30.931218 }, { lat: -29.93225, lng: 30.932436 }, { lat: -29.93449, lng: 30.933789 }, { lat: -29.933166, lng: 30.937608 }, { lat: -29.935894, lng: 30.94597 }, { lat: -29.938777, lng: 30.947733 }, { lat: -29.940396, lng: 30.951298 }
        ];
        var S3Map = new google.maps.Polygon({
            paths: S3,
            strokeColor: "#FF8C00",
            strokeOpacity: 0,
            strokeWeight: 0,
            fillColor: "",
            fillOpacity: 0,
            draggable: false,
            geodesic: false,
            name: 'S3',
            map: map
        });
        CrmJs.Sales.Map.polygons.push(S3Map);
        CrmJs.Sales.Map.DrawSubSector3(map);

        var S4 = [
            { lat: -29.7969134, lng: 30.9490018 }, { lat: -29.7919974, lng: 30.9460828 }, { lat: -29.7863364, lng: 30.9407188 }, { lat: -29.7845854, lng: 30.9352688 }, { lat: -29.7820154, lng: 30.9299468 }, { lat: -29.7843244, lng: 30.9251408 }, { lat: -29.7887194, lng: 30.9196468 }, { lat: -29.7942314, lng: 30.9197328 }, { lat: -29.7991294, lng: 30.9235958 }, { lat: -29.7996874, lng: 30.9213208 }, { lat: -29.8044914, lng: 30.9096908 }, { lat: -29.8068564, lng: 30.9053138 }, { lat: -29.8041304, lng: 30.8932868 }, { lat: -29.8027344, lng: 30.8934148 }, { lat: -29.7997364, lng: 30.8903468 }, { lat: -29.7952004, lng: 30.8885338 }, { lat: -29.7934124, lng: 30.8870748 }, { lat: -29.7888684, lng: 30.8878468 }, { lat: -29.7829834, lng: 30.8868168 }, { lat: -29.7804794, lng: 30.8885068 }, { lat: -29.7794734, lng: 30.8845588 }, { lat: -29.7794824, lng: 30.8783628 }, { lat: -29.7755994, lng: 30.8713838 }, { lat: -29.7769682, lng: 30.8681302 }, { lat: -29.7782074, lng: 30.8679078 }, { lat: -29.7813074, lng: 30.8645008 }, { lat: -29.7884874, lng: 30.8615558 }, { lat: -29.7888224, lng: 30.8573928 }, { lat: -29.7904794, lng: 30.8539218 }, { lat: -29.7930214, lng: 30.8541258 }, { lat: -29.7934874, lng: 30.8526238 }, { lat: -29.7960934, lng: 30.8503388 }, { lat: -29.8001344, lng: 30.8510499 }, { lat: -29.8026484, lng: 30.8513258 }, { lat: -29.8032074, lng: 30.8491798 }, { lat: -29.8020524, lng: 30.8454468 }, { lat: -29.8012334, lng: 30.8357048 }, { lat: -29.8030954, lng: 30.8314988 }, { lat: -29.8059624, lng: 30.8266928 }, { lat: -29.8089604, lng: 30.8228298 }, { lat: -29.8118834, lng: 30.8181958 }, { lat: -29.8155324, lng: 30.8150628 }, { lat: -29.8175804, lng: 30.8118868 }, { lat: -29.8249894, lng: 30.8088828 }, { lat: -29.8291964, lng: 30.8134318 }, { lat: -29.8320634, lng: 30.8207278 }, { lat: -29.8352832, lng: 30.8238416 }, { lat: -29.8368474, lng: 30.8280658 }, { lat: -29.8390064, lng: 30.8306198 }, { lat: -29.8418914, lng: 30.8361978 }, { lat: -29.8490934, lng: 30.8440088 }, { lat: -29.8422264, lng: 30.8489008 }, { lat: -29.8426914, lng: 30.8561538 }, { lat: -29.8443734, lng: 30.8626358 }, { lat: -29.8531694, lng: 30.8744578 }, { lat: -29.8531064, lng: 30.8766478 }, { lat: -29.8516994, lng: 30.8809808 }, { lat: -29.8476184, lng: 30.8804178 }, { lat: -29.8465997, lng: 30.8870988 }, { lat: -29.8488144, lng: 30.8887908 }, { lat: -29.8496334, lng: 30.8906798 }, { lat: -29.8534304, lng: 30.8953998 }, { lat: -29.8543974, lng: 30.8982758 }, { lat: -29.8576734, lng: 30.8985758 }, { lat: -29.8596824, lng: 30.8965588 }, { lat: -29.8622514, lng: 30.9019228 }, { lat: -29.8625854, lng: 30.9047128 }, { lat: -29.8610594, lng: 30.9047558 }, { lat: -29.8599434, lng: 30.9052278 }, { lat: -29.8599804, lng: 30.9072018 }, { lat: -29.8615434, lng: 30.9077168 }, { lat: -29.864093, lng: 30.9080415 }, { lat: -29.8655634, lng: 30.9074158 }, { lat: -29.8663074, lng: 30.9095188 }, { lat: -29.8647074, lng: 30.9123088 }, { lat: -29.8652284, lng: 30.9185318 }, { lat: -29.8688754, lng: 30.9272858 }, { lat: -29.8700664, lng: 30.9287018 }, { lat: -29.8698054, lng: 30.9324578 }, { lat: -29.8709964, lng: 30.9345388 }, { lat: -29.8728574, lng: 30.9369418 }, { lat: -29.8524994, lng: 30.9325648 }, { lat: -29.8514754, lng: 30.9521768 }, { lat: -29.8471394, lng: 30.9559538 }, { lat: -29.841835, lng: 30.9565358 }, { lat: -29.8300524, lng: 30.9617898 }, { lat: -29.8267764, lng: 30.9658668 }, { lat: -29.8221224, lng: 30.9675408 }, { lat: -29.8214524, lng: 30.9634208 }, { lat: -29.8207074, lng: 30.9618758 }, { lat: -29.8204474, lng: 30.9552238 }, { lat: -29.8190694, lng: 30.9545798 }, { lat: -29.8143784, lng: 30.9593008 }, { lat: -29.8109524, lng: 30.9607598 }, { lat: -29.8093134, lng: 30.9582708 }, { lat: -29.8061484, lng: 30.9580998 }, { lat: -29.8029464, lng: 30.9688278 }, { lat: -29.8028904, lng: 30.9788278 }, { lat: -29.7982164, lng: 30.9721758 }, { lat: -29.7976766, lng: 30.9638314 }, { lat: -29.7981794, lng: 30.9565978 }
        ];
        var S4Map = new google.maps.Polygon({
            paths: S4,
            strokeColor: "#00BFFF",
            strokeOpacity: 0,
            strokeWeight: 0,
            fillColor: "",
            fillOpacity: 0,
            draggable: false,
            geodesic: false,
            name: 'S4',
            map: map
        });
        CrmJs.Sales.Map.polygons.push(S4Map);
        CrmJs.Sales.Map.DrawSubSector4(map);

        var S5 = [
            { lat: -29.712991, lng: 30.853043 }, { lat: -29.717203, lng: 30.842228 }, { lat: -29.721079, lng: 30.831242 }, { lat: -29.731663, lng: 30.810685 }, { lat: -29.73457, lng: 30.806522 }, { lat: -29.737402, lng: 30.798154 }, { lat: -29.739302, lng: 30.797081 }, { lat: -29.741017, lng: 30.793991 }, { lat: -29.743625, lng: 30.79236 }, { lat: -29.745264, lng: 30.787039 }, { lat: -29.752381, lng: 30.777898 }, { lat: -29.748432, lng: 30.77558 }, { lat: -29.750108, lng: 30.773435 }, { lat: -29.751002, lng: 30.769615 }, { lat: -29.752418, lng: 30.768414 }, { lat: -29.755175, lng: 30.760345 }, { lat: -29.754691, lng: 30.755796 }, { lat: -29.751151, lng: 30.757213 }, { lat: -29.748916, lng: 30.753779 }, { lat: -29.746643, lng: 30.748544 }, { lat: -29.746047, lng: 30.742879 }, { lat: -29.748208, lng: 30.734167 }, { lat: -29.74914, lng: 30.729318 }, { lat: -29.751226, lng: 30.72494 }, { lat: -29.753834, lng: 30.718031 }, { lat: -29.756256, lng: 30.714426 }, { lat: -29.75782, lng: 30.714812 }, { lat: -29.77857, lng: 30.706058 }, { lat: -29.782295, lng: 30.702238 }, { lat: -29.784716, lng: 30.715799 }, { lat: -29.788142, lng: 30.710735 }, { lat: -29.791271, lng: 30.704556 }, { lat: -29.792463, lng: 30.700693 }, { lat: -29.795889, lng: 30.699019 }, { lat: -29.80155, lng: 30.687861 }, { lat: -29.809221, lng: 30.688248 }, { lat: -29.826311, lng: 30.697088 }, { lat: -29.832268, lng: 30.702238 }, { lat: -29.836028, lng: 30.708675 }, { lat: -29.830258, lng: 30.716958 }, { lat: -29.826795, lng: 30.723438 }, { lat: -29.827577, lng: 30.731978 }, { lat: -29.821136, lng: 30.740604 }, { lat: -29.811567, lng: 30.749745 }, { lat: -29.80397, lng: 30.746656 }, { lat: -29.806354, lng: 30.75129 }, { lat: -29.809519, lng: 30.757685 }, { lat: -29.81328, lng: 30.770388 }, { lat: -29.816296, lng: 30.77395 }, { lat: -29.816072, lng: 30.775409 }, { lat: -29.81287, lng: 30.775151 }, { lat: -29.81246, lng: 30.776267 }, { lat: -29.808923, lng: 30.777522 }, { lat: -29.807657, lng: 30.779743 }, { lat: -29.804566, lng: 30.781031 }, { lat: -29.800842, lng: 30.779743 }, { lat: -29.79898, lng: 30.77867 }, { lat: -29.797006, lng: 30.778542 }, { lat: -29.794362, lng: 30.780816 }, { lat: -29.793021, lng: 30.783176 }, { lat: -29.79399, lng: 30.797596 }, { lat: -29.792835, lng: 30.806522 }, { lat: -29.79129, lng: 30.805621 }, { lat: -29.790172, lng: 30.806265 }, { lat: -29.789446, lng: 30.807617 }, { lat: -29.785535, lng: 30.811973 }, { lat: -29.783114, lng: 30.81281 }, { lat: -29.782556, lng: 30.812101 }, { lat: -29.779464, lng: 30.808411 }, { lat: -29.776, lng: 30.808496 }, { lat: -29.772238, lng: 30.810943 }, { lat: -29.770375, lng: 30.815406 }, { lat: -29.76855, lng: 30.821071 }, { lat: -29.768587, lng: 30.826564 }, { lat: -29.771567, lng: 30.829053 }, { lat: -29.773169, lng: 30.829074 }, { lat: -29.774864, lng: 30.8324 }, { lat: -29.774175, lng: 30.835769 }, { lat: -29.775646, lng: 30.837593 }, { lat: -29.77153, lng: 30.842142 }, { lat: -29.767656, lng: 30.845361 }, { lat: -29.75756, lng: 30.85609 }, { lat: -29.744016, lng: 30.848472 }, { lat: -29.736731, lng: 30.853364 }, { lat: -29.728906, lng: 30.854437 }, { lat: -29.725533, lng: 30.853922 }, { lat: -29.718042, lng: 30.859072 }, { lat: -29.716495, lng: 30.857806 }
        ];
        var S5Map = new google.maps.Polygon({
            paths: S5,
            strokeColor: "#8B0000",
            strokeOpacity: 0,
            strokeWeight: 0,
            fillColor: "",
            fillOpacity: 0,
            draggable: false,
            geodesic: false,
            name: 'S5',
            map: map
        });
        CrmJs.Sales.Map.polygons.push(S5Map);
        CrmJs.Sales.Map.DrawSubSector5(map);

        var S6 = [
            { lat: -29.713849, lng: 31.094871 }, { lat: -29.71096, lng: 31.088131 }, { lat: -29.705761, lng: 31.09036 }, { lat: -29.704234, lng: 31.090605 }, { lat: -29.703117, lng: 31.087209 }, { lat: -29.702059, lng: 31.085953 }, { lat: -29.703203, lng: 31.084513 }, { lat: -29.704188, lng: 31.084359 }, { lat: -29.705919, lng: 31.082532 }, { lat: -29.70645, lng: 31.083144 }, { lat: -29.706319, lng: 31.081352 }, { lat: -29.705201, lng: 31.075344 }, { lat: -29.703971, lng: 31.069475 }, { lat: -29.698938, lng: 31.071568 }, { lat: -29.695658, lng: 31.076031 }, { lat: -29.678844, lng: 31.058564 }, { lat: -29.672132, lng: 31.045947 }, { lat: -29.673362, lng: 31.043158 }, { lat: -29.67739, lng: 31.039166 }, { lat: -29.677501, lng: 31.036034 }, { lat: -29.676606, lng: 31.030369 }, { lat: -29.665905, lng: 31.022301 }, { lat: -29.666949, lng: 31.012688 }, { lat: -29.671871, lng: 31.004448 }, { lat: -29.678732, lng: 31.002731 }, { lat: -29.683952, lng: 30.992947 }, { lat: -29.688873, lng: 30.986252 }, { lat: -29.695136, lng: 30.984106 }, { lat: -29.697596, lng: 30.982304 }, { lat: -29.70125, lng: 30.980244 }, { lat: -29.706767, lng: 30.981274 }, { lat: -29.71452, lng: 30.979042 }, { lat: -29.718992, lng: 30.981617 }, { lat: -29.723315, lng: 30.986252 }, { lat: -29.724657, lng: 30.992947 }, { lat: -29.725552, lng: 30.996208 }, { lat: -29.728086, lng: 30.99947 }, { lat: -29.733303, lng: 31.002216 }, { lat: -29.73852, lng: 31.008224 }, { lat: -29.733899, lng: 31.008053 }, { lat: -29.729278, lng: 31.00668 }, { lat: -29.726744, lng: 31.006508 }, { lat: -29.724582, lng: 31.008117 }, { lat: -29.726017, lng: 31.011003 }, { lat: -29.729129, lng: 31.013374 }, { lat: -29.730992, lng: 31.017494 }, { lat: -29.731663, lng: 31.019039 }, { lat: -29.732856, lng: 31.023502 }, { lat: -29.737626, lng: 31.028051 }, { lat: -29.739638, lng: 31.025562 }, { lat: -29.74247, lng: 31.023331 }, { lat: -29.745153, lng: 31.019297 }, { lat: -29.745674, lng: 31.011057 }, { lat: -29.749996, lng: 31.012087 }, { lat: -29.754542, lng: 31.011829 }, { lat: -29.756852, lng: 31.015863 }, { lat: -29.757187, lng: 31.016593 }, { lat: -29.761621, lng: 31.010284 }, { lat: -29.768922, lng: 31.000371 }, { lat: -29.771735, lng: 30.998526 }, { lat: -29.775692, lng: 30.998213 }, { lat: -29.770407, lng: 30.993913 }, { lat: -29.77045, lng: 30.99226 }, { lat: -29.770903, lng: 30.990026 }, { lat: -29.773898, lng: 30.985614 }, { lat: -29.779911, lng: 30.981417 }, { lat: -29.785849, lng: 30.988292 }, { lat: -29.783785, lng: 30.991616 }, { lat: -29.781866, lng: 30.993934 }, { lat: -29.785274, lng: 30.993666 }, { lat: -29.787546, lng: 30.994899 }, { lat: -29.789074, lng: 30.995479 }, { lat: -29.797937, lng: 30.98917 }, { lat: -29.800246, lng: 30.98505 }, { lat: -29.802667, lng: 30.981574 }, { lat: -29.803896, lng: 30.980415 }, { lat: -29.809742, lng: 30.985222 }, { lat: -29.813354, lng: 30.994964 }, { lat: -29.813764, lng: 31.000414 }, { lat: -29.813354, lng: 31.007624 }, { lat: -29.812423, lng: 31.014318 }, { lat: -29.811083, lng: 31.017151 }, { lat: -29.811902, lng: 31.022043 }, { lat: -29.814359, lng: 31.025476 }, { lat: -29.818837, lng: 31.027408 }, { lat: -29.817943, lng: 31.031399 }, { lat: -29.820866, lng: 31.033169 }, { lat: -29.822467, lng: 31.033684 }, { lat: -29.823035, lng: 31.039166 }, { lat: -29.813131, lng: 31.041656 }, { lat: -29.805609, lng: 31.043887 }, { lat: -29.798012, lng: 31.045947 }, { lat: -29.779464, lng: 31.055431 }, { lat: -29.774398, lng: 31.058135 }, { lat: -29.769928, lng: 31.06101 }, { lat: -29.768885, lng: 31.060967 }, { lat: -29.758566, lng: 31.068392 }, { lat: -29.75402, lng: 31.071482 }, { lat: -29.747574, lng: 31.076374 }, { lat: -29.739526, lng: 31.081738 }, { lat: -29.727378, lng: 31.089678 }, { lat: -29.724545, lng: 31.091695 }, { lat: -29.721936, lng: 31.091609 }
        ];
        var S6Map = new google.maps.Polygon({
            paths: S6,
            strokeColor: "#483D8B",
            strokeOpacity: 0,
            strokeWeight: 0,
            fillColor: "",
            fillOpacity: 0,
            draggable: false,
            geodesic: false,
            name: 'S6',
            map: map
        });
        CrmJs.Sales.Map.polygons.push(S6Map);
        CrmJs.Sales.Map.DrawSubSector6(map);

        var S7 = [
            { lat: -29.889732, lng: 31.031699 }, { lat: -29.891778, lng: 31.030026 }, { lat: -29.891667, lng: 31.027279 }, { lat: -29.893267, lng: 31.024876 }, { lat: -29.900708, lng: 31.017322 }, { lat: -29.907329, lng: 31.011829 }, { lat: -29.90413, lng: 30.980802 }, { lat: -29.915253, lng: 30.977755 }, { lat: -29.928029, lng: 30.970802 }, { lat: -29.935077, lng: 30.965695 }, { lat: -29.940396, lng: 30.961254 }, { lat: -29.946457, lng: 30.954752 }, { lat: -29.943259, lng: 30.950804 }, { lat: -29.948018, lng: 30.948186 }, { lat: -29.955102, lng: 30.946609 }, { lat: -29.966739, lng: 30.937125 }, { lat: -29.969119, lng: 30.92631 }, { lat: -29.977334, lng: 30.918714 }, { lat: -29.982948, lng: 30.914895 }, { lat: -29.982948, lng: 30.913908 }, { lat: -29.985271, lng: 30.910378 }, { lat: -29.98713, lng: 30.908833 }, { lat: -29.99566, lng: 30.910217 }, { lat: -29.998187, lng: 30.907256 }, { lat: -30.001755, lng: 30.904681 }, { lat: -30.00391, lng: 30.90511 }, { lat: -30.009039, lng: 30.907986 }, { lat: -30.010656, lng: 30.901194 }, { lat: -30.011956, lng: 30.900142 }, { lat: -30.012867, lng: 30.898747 }, { lat: -30.012997, lng: 30.895636 }, { lat: -30.015654, lng: 30.893104 }, { lat: -30.0174, lng: 30.892224 }, { lat: -30.018812, lng: 30.890722 }, { lat: -30.019202, lng: 30.888662 }, { lat: -30.019481, lng: 30.887246 }, { lat: -30.020262, lng: 30.88628 }, { lat: -30.021952, lng: 30.885036 }, { lat: -30.022268, lng: 30.883834 }, { lat: -30.021674, lng: 30.882161 }, { lat: -30.021785, lng: 30.880873 }, { lat: -30.022417, lng: 30.880101 }, { lat: -30.024256, lng: 30.879028 }, { lat: -30.025668, lng: 30.876925 }, { lat: -30.03544, lng: 30.876818 }, { lat: -30.037706, lng: 30.871453 }, { lat: -30.040641, lng: 30.867891 }, { lat: -30.04547, lng: 30.86669 }, { lat: -30.049296, lng: 30.862656 }, { lat: -30.049891, lng: 30.861239 }, { lat: -30.054794, lng: 30.860166 }, { lat: -30.05888, lng: 30.856948 }, { lat: -30.066754, lng: 30.852227 }, { lat: -30.06746, lng: 30.851154 }, { lat: -30.068797, lng: 30.849094 }, { lat: -30.069762, lng: 30.847807 }, { lat: -30.073662, lng: 30.839224 }, { lat: -30.075482, lng: 30.836906 }, { lat: -30.077747, lng: 30.836005 }, { lat: -30.080161, lng: 30.838323 }, { lat: -30.081089, lng: 30.841713 }, { lat: -30.085397, lng: 30.828624 }, { lat: -30.087328, lng: 30.827122 }, { lat: -30.08885, lng: 30.825276 }, { lat: -30.09119, lng: 30.82253 }, { lat: -30.093269, lng: 30.820684 }, { lat: -30.095497, lng: 30.819869 }, { lat: -30.096574, lng: 30.821242 }, { lat: -30.097985, lng: 30.82519 }, { lat: -30.099581, lng: 30.828366 }, { lat: -30.100695, lng: 30.83343 }, { lat: -30.099098, lng: 30.837851 }, { lat: -30.094791, lng: 30.841713 }, { lat: -30.092749, lng: 30.844974 }, { lat: -30.095163, lng: 30.846348 }, { lat: -30.0952, lng: 30.849609 }, { lat: -30.096611, lng: 30.851884 }, { lat: -30.097873, lng: 30.852485 }, { lat: -30.101734, lng: 30.851111 }, { lat: -30.1089, lng: 30.846176 }, { lat: -30.116399, lng: 30.840855 }, { lat: -30.124937, lng: 30.834718 }, { lat: -30.128166, lng: 30.833816 }, { lat: -30.13481, lng: 30.845146 }, { lat: -30.132323, lng: 30.847979 }, { lat: -30.127832, lng: 30.847292 }, { lat: -30.121262, lng: 30.850081 }, { lat: -30.104296, lng: 30.85815 }, { lat: -30.095163, lng: 30.864286 }, { lat: -30.092489, lng: 30.863857 }, { lat: -30.07771, lng: 30.87287 }, { lat: -30.071861, lng: 30.875638 }, { lat: -30.070654, lng: 30.876732 }, { lat: -30.067627, lng: 30.879178 }, { lat: -30.063541, lng: 30.882311 }, { lat: -30.058954, lng: 30.883942 }, { lat: -30.052231, lng: 30.889349 }, { lat: -30.051711, lng: 30.891495 }, { lat: -30.049482, lng: 30.891409 }, { lat: -30.047625, lng: 30.893383 }, { lat: -30.04235, lng: 30.897589 }, { lat: -30.037, lng: 30.902824 }, { lat: -30.029049, lng: 30.912952 }, { lat: -30.019909, lng: 30.922995 }, { lat: -30.0161, lng: 30.927522 }, { lat: -30.012922, lng: 30.931835 }, { lat: -30.008667, lng: 30.936953 }, { lat: -30.005174, lng: 30.941545 }, { lat: -30.004654, lng: 30.943262 }, { lat: -30.003204, lng: 30.943348 }, { lat: -29.997184, lng: 30.949828 }, { lat: -29.988096, lng: 30.959558 }, { lat: -29.982297, lng: 30.965738 }, { lat: -29.960884, lng: 30.986681 }, { lat: -29.950175, lng: 30.996208 }, { lat: -29.941548, lng: 31.004598 }, { lat: -29.939726, lng: 31.007247 }, { lat: -29.937457, lng: 31.008654 }, { lat: -29.93478, lng: 31.011229 }, { lat: -29.93225, lng: 31.013889 }, { lat: -29.93067, lng: 31.014164 }, { lat: -29.928457, lng: 31.016378 }, { lat: -29.918786, lng: 31.024876 }, { lat: -29.910008, lng: 31.034489 }, { lat: -29.90346, lng: 31.039896 }, { lat: -29.899666, lng: 31.042557 }, { lat: -29.898401, lng: 31.041741 }, { lat: -29.897006, lng: 31.040175 }, { lat: -29.894457, lng: 31.038094 }, { lat: -29.89215, lng: 31.038008 }, { lat: -29.891332, lng: 31.033759 }
        ];
        var S7Map = new google.maps.Polygon({
            paths: S7,
            strokeColor: "#800080",
            strokeOpacity: 0,
            strokeWeight: 0,
            fillColor: "",
            fillOpacity: 0,
            draggable: false,
            geodesic: false,
            name: 'S7',
            map: map
        });
        CrmJs.Sales.Map.polygons.push(S7Map);
        CrmJs.Sales.Map.DrawSubSector7(map);

        var S8 = [
            { lat: -29.534955, lng: 31.1946703 }, { lat: -29.5356178, lng: 31.1963654 }, { lat: -29.5377367, lng: 31.200378 }, { lat: -29.5376993, lng: 31.2009788 }, { lat: -29.5421424, lng: 31.2049485 }, { lat: -29.5421984, lng: 31.2063003 }, { lat: -29.5410037, lng: 31.2081457 }, { lat: -29.5421798, lng: 31.2088323 }, { lat: -29.5432817, lng: 31.2082022 }, { lat: -29.5443826, lng: 31.2071157 }, { lat: -29.5470053, lng: 31.2106562 }, { lat: -29.5480927, lng: 31.2086338 }, { lat: -29.5489327, lng: 31.2078399 }, { lat: -29.5494134, lng: 31.2071157 }, { lat: -29.5500854, lng: 31.2082208 }, { lat: -29.5475467, lng: 31.2123943 }, { lat: -29.5467813, lng: 31.2125284 }, { lat: -29.5451106, lng: 31.2148458 }, { lat: -29.5444666, lng: 31.2164283 }, { lat: -29.541475, lng: 31.2178177 }, { lat: -29.5401683, lng: 31.2203497 }, { lat: -29.5366772, lng: 31.2233967 }, { lat: -29.5351837, lng: 31.2236757 }, { lat: -29.5074261, lng: 31.2360372 }, { lat: -29.4844577, lng: 31.2580556 }, { lat: -29.4825446, lng: 31.2477897 }, { lat: -29.4963425, lng: 31.2424358 }, { lat: -29.4953637, lng: 31.2369118 }, { lat: -29.4989143, lng: 31.2361557 }, { lat: -29.4991426, lng: 31.232884 }, { lat: -29.5001648, lng: 31.2286359 }, { lat: -29.5042868, lng: 31.2246454 }, { lat: -29.5078994, lng: 31.2323714 }, { lat: -29.5142203, lng: 31.2258164 }, { lat: -29.5154108, lng: 31.2276348 }, { lat: -29.5213441, lng: 31.2243892 }, { lat: -29.5230432, lng: 31.2242604 }, { lat: -29.5235474, lng: 31.2240029 }, { lat: -29.5220536, lng: 31.2215138 }, { lat: -29.5220095, lng: 31.2201785 }, { lat: -29.5242942, lng: 31.2179518 }, { lat: -29.5240328, lng: 31.2144757 }, { lat: -29.5257132, lng: 31.212287 }, { lat: -29.5256385, lng: 31.2114287 }, { lat: -29.5243061, lng: 31.210823 }, { lat: -29.5208213, lng: 31.2099267 }, { lat: -29.5191828, lng: 31.2091702 }, { lat: -29.5180298, lng: 31.2053239 }, { lat: -29.5143046, lng: 31.2041116 }, { lat: -29.5106259, lng: 31.2065793 }, { lat: -29.5102151, lng: 31.2049056 }, { lat: -29.5175968, lng: 31.1988307 }, { lat: -29.5227258, lng: 31.1954642 }, { lat: -29.5287752, lng: 31.193018 }, { lat: -29.5303435, lng: 31.1963869 }
        ];
        var S8Map = new google.maps.Polygon({
            paths: S8,
            strokeColor: "#FFA500",
            strokeOpacity: 0,
            strokeWeight: 0,
            fillColor: "",
            fillOpacity: 0,
            draggable: false,
            geodesic: false,
            name: 'S8',
            map: map
        });
        CrmJs.Sales.Map.polygons.push(S8Map);
        CrmJs.Sales.Map.DrawSubSector8(map);

        var S9 = [
            { lat: -29.829848, lng: 30.891924 }, { lat: -29.8313, lng: 30.895722 }, { lat: -29.83431, lng: 30.8987043 }, { lat: -29.8352779, lng: 30.8992944 }, { lat: -29.8357364, lng: 30.9001742 }, { lat: -29.836097, lng: 30.905528 }, { lat: -29.835916, lng: 30.908189 }, { lat: -29.834232, lng: 30.912083 }, { lat: -29.832817, lng: 30.912899 }, { lat: -29.831663, lng: 30.91277 }, { lat: -29.831756, lng: 30.910903 }, { lat: -29.831598, lng: 30.909584 }, { lat: -29.833059, lng: 30.907706 }, { lat: -29.832873, lng: 30.906848 }, { lat: -29.831886, lng: 30.906848 }, { lat: -29.829746, lng: 30.905904 }, { lat: -29.829299, lng: 30.904659 }, { lat: -29.829057, lng: 30.904509 }, { lat: -29.829317, lng: 30.902728 }, { lat: -29.828871, lng: 30.901762 }, { lat: -29.82753, lng: 30.901354 }, { lat: -29.825948, lng: 30.900324 }, { lat: -29.824757, lng: 30.90011 }, { lat: -29.824021, lng: 30.90062 }, { lat: -29.823004, lng: 30.900947 }, { lat: -29.823586, lng: 30.901364 }, { lat: -29.823963, lng: 30.90177 }, { lat: -29.824917, lng: 30.902608 }, { lat: -29.824354, lng: 30.904259 }, { lat: -29.8231, lng: 30.905356 }, { lat: -29.822085, lng: 30.906547 }, { lat: -29.821852, lng: 30.908511 }, { lat: -29.821567, lng: 30.908798 }, { lat: -29.821476, lng: 30.908769 }, { lat: -29.820317, lng: 30.910678 }, { lat: -29.819312, lng: 30.911236 }, { lat: -29.819236, lng: 30.910275 }, { lat: -29.818985, lng: 30.908687 }, { lat: -29.818902, lng: 30.907803 }, { lat: -29.8184615, lng: 30.9063754 }, { lat: -29.817608, lng: 30.902926 }, { lat: -29.817375, lng: 30.901451 }, { lat: -29.81771, lng: 30.89879 }, { lat: -29.818641, lng: 30.895271 }, { lat: -29.820726, lng: 30.892654 }, { lat: -29.821154, lng: 30.889059 }, { lat: -29.821623, lng: 30.888995 }, { lat: -29.822722, lng: 30.888652 }, { lat: -29.823094, lng: 30.888094 }, { lat: -29.8234972, lng: 30.887836 }, { lat: -29.8238416, lng: 30.8876751 }, { lat: -29.8244652, lng: 30.887321 }, { lat: -29.825198, lng: 30.887128 }, { lat: -29.8257937, lng: 30.8872781 }, { lat: -29.826287, lng: 30.887171 }, { lat: -29.8267617, lng: 30.8868919 }, { lat: -29.827283, lng: 30.886345 }, { lat: -29.8279345, lng: 30.8859799 }, { lat: -29.829787, lng: 30.885937 }, { lat: -29.829865, lng: 30.88724 }, { lat: -29.830368, lng: 30.88797 }, { lat: -29.83142, lng: 30.88812 }, { lat: -29.832515, lng: 30.889032 }, { lat: -29.83238, lng: 30.889521 }
        ];
        var S9Map = new google.maps.Polygon({
            paths: S9,
            strokeColor: "#000000",
            strokeOpacity: 0,
            strokeWeight: 0,
            fillColor: "",
            fillOpacity: 0,
            draggable: false,
            geodesic: false,
            name: 'S9',
            map: map
        });
        CrmJs.Sales.Map.polygons.push(S9Map);
        CrmJs.Sales.Map.DrawSubSector9(map);
    },

    DrawSubSector1: function (map) {
        var B1SP = [
            { lng: 30.779389, lat: -29.807526 },
            { lng: 30.777522, lat: -29.808923 },
            { lng: 30.775996, lat: -29.812395 },
            { lng: 30.774887, lat: -29.812772 },
            { lng: 30.775409, lat: -29.816072 },
            { lng: 30.779014, lat: -29.81894 },
            { lng: 30.783649, lat: -29.821769 },
            { lng: 30.789184, lat: -29.821955 },
            { lng: 30.79266, lat: -29.822886 },
            { lng: 30.797338, lat: -29.822067 },
            { lng: 30.801029, lat: -29.819051 },
            { lng: 30.803904, lat: -29.816929 },
            { lng: 30.799956, lat: -29.813801 },
            { lng: 30.794678, lat: -29.811157 },
            { lng: 30.788863, lat: -29.81125 },
            { lng: 30.789571, lat: -29.807768 },
            { lng: 30.785434, lat: -29.808214 },
            { lng: 30.783031, lat: -29.809368 },
            { lng: 30.780799, lat: -29.808624 },
            { lng: 30.779389, lat: -29.807526 },
        ];

        var B1SPMap = new google.maps.Polygon({
            paths: B1SP,
            strokeColor: "#009D57",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#009D57",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'B1(SP)',
            map: map,
        });

        CrmJs.Sales.Map.polygonsSS.push(B1SPMap);

        var B2SP = [
            { lng: 30.797553, lat: -29.768587 },
            { lng: 30.796952, lat: -29.774174 },
            { lng: 30.799666, lat: -29.776561 },
            { lng: 30.8007662, lat: -29.7772576 },
            { lng: 30.80181, lat: -29.777743 },
            { lng: 30.801409, lat: -29.778381 },
            { lng: 30.80178, lat: -29.778545 },
            { lng: 30.802006, lat: -29.778441 },
            { lng: 30.802388, lat: -29.778374 },
            { lng: 30.803443, lat: -29.779545 },
            { lng: 30.804039, lat: -29.778958 },
            { lng: 30.807553, lat: -29.780935 },
            { lng: 30.809891, lat: -29.781131 },
            { lng: 30.811801, lat: -29.782593 },
            { lng: 30.81281, lat: -29.78304 },
            { lng: 30.813518, lat: -29.783747 },
            { lng: 30.816822, lat: -29.78304 },
            { lng: 30.822165, lat: -29.779986 },
            { lng: 30.825877, lat: -29.776335 },
            { lng: 30.827808, lat: -29.775274 },
            { lng: 30.829074, lat: -29.773094 },
            { lng: 30.829053, lat: -29.771567 },
            { lng: 30.826564, lat: -29.768587 },
            { lng: 30.821071, lat: -29.76855 },
            { lng: 30.817251, lat: -29.766985 },
            { lng: 30.814462, lat: -29.766389 },
            { lng: 30.811543, lat: -29.765849 },
            { lng: 30.799184, lat: -29.764974 },
            { lng: 30.797553, lat: -29.768587 },
        ];

        var B2SPMap = new google.maps.Polygon({
            paths: B2SP,
            strokeColor: "#93D7E8",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#93D7E8",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'B2(SP)',
            map: map
        });

        CrmJs.Sales.Map.polygonsSS.push(B2SPMap);
        var B3 = [
            { lng: 30.812316, lat: -29.793692 },
            { lng: 30.818152, lat: -29.793468 },
            { lng: 30.823002, lat: -29.793003 },
            { lng: 30.828066, lat: -29.793356 },
            { lng: 30.829525, lat: -29.797211 },
            { lng: 30.831413, lat: -29.803114 },
            { lng: 30.835619, lat: -29.801252 },
            { lng: 30.845361, lat: -29.802071 },
            { lng: 30.849094, lat: -29.803226 },
            { lng: 30.85124, lat: -29.802667 },
            { lng: 30.851541, lat: -29.800842 },
            { lng: 30.850253, lat: -29.796112 },
            { lng: 30.84903, lat: -29.793305 },
            { lng: 30.84645, lat: -29.79264 },
            { lng: 30.844706, lat: -29.792298 },
            { lng: 30.844253, lat: -29.792616 },
            { lng: 30.844009, lat: -29.792803 },
            { lng: 30.843108, lat: -29.792295 },
            { lng: 30.842657, lat: -29.79203 },
            { lng: 30.841788, lat: -29.791667 },
            { lng: 30.83935, lat: -29.791061 },
            { lng: 30.838664, lat: -29.79057 },
            { lng: 30.8375392, lat: -29.7901937 },
            { lng: 30.8372621, lat: -29.7905717 },
            { lng: 30.8375616, lat: -29.7907806 },
            { lng: 30.8374266, lat: -29.7913153 },
            { lng: 30.8370171, lat: -29.7919845 },
            { lng: 30.8361337, lat: -29.7929783 },
            { lng: 30.8353219, lat: -29.7922471 },
            { lng: 30.836077, lat: -29.791011 },
            { lng: 30.834968, lat: -29.790886 },
            { lng: 30.834216, lat: -29.790412 },
            { lng: 30.834594, lat: -29.789865 },
            { lng: 30.834932, lat: -29.789523 },
            { lng: 30.835587, lat: -29.788627 },
            { lng: 30.836692, lat: -29.786951 },
            { lng: 30.834718, lat: -29.786951 },
            { lng: 30.833173, lat: -29.787137 },
            { lng: 30.832357, lat: -29.78844 },
            { lng: 30.832057, lat: -29.787844 },
            { lng: 30.831671, lat: -29.787621 },
            { lng: 30.829096, lat: -29.787286 },
            { lng: 30.826006, lat: -29.787323 },
            { lng: 30.825534, lat: -29.786541 },
            { lng: 30.822959, lat: -29.789036 },
            { lng: 30.821629, lat: -29.790675 },
            { lng: 30.817981, lat: -29.793021 },
            { lng: 30.813475, lat: -29.793021 },
            { lng: 30.8119084, lat: -29.7926489 },
            { lng: 30.812316, lat: -29.793692 },
        ];

        var B3Map = new google.maps.Polygon({
            paths: B3,
            strokeColor: "#B29189",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#B29189",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'B3',
            map: map
        });

        CrmJs.Sales.Map.polygonsSS.push(B3Map);

        var B4SP = [
            { lng: 30.81281, lat: -29.783114 },
            { lng: 30.811973, lat: -29.785535 },
            { lng: 30.807617, lat: -29.789446 },
            { lng: 30.806265, lat: -29.790172 },
            { lng: 30.805621, lat: -29.79129 },
            { lng: 30.806243, lat: -29.792742 },
            { lng: 30.809484, lat: -29.792239 },
            { lng: 30.813475, lat: -29.793021 },
            { lng: 30.817981, lat: -29.793021 },
            { lng: 30.818474, lat: -29.792742 },
            { lng: 30.820019, lat: -29.791755 },
            { lng: 30.821629, lat: -29.790675 },
            { lng: 30.821993, lat: -29.79021 },
            { lng: 30.823688, lat: -29.788422 },
            { lng: 30.825534, lat: -29.786541 },
            { lng: 30.826178, lat: -29.785628 },
            { lng: 30.825341, lat: -29.784232 },
            { lng: 30.823839, lat: -29.781624 },
            { lng: 30.822165, lat: -29.78006 },
            { lng: 30.81929, lat: -29.781699 },
            { lng: 30.816822, lat: -29.783114 },
            { lng: 30.813518, lat: -29.783822 },
            { lng: 30.81281, lat: -29.783114 },
        ];

        var B4SPMap = new google.maps.Polygon({
            paths: B4SP,
            strokeColor: "#A61B4A",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#A61B4A",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'B4(SP)',
            map: map
        });

        CrmJs.Sales.Map.polygonsSS.push(B4SPMap);

        var B5SP = [
            { lng: 30.822165, lat: -29.78006 },
            { lng: 30.823774, lat: -29.781513 },
            { lng: 30.826178, lat: -29.785628 },
            { lng: 30.825534, lat: -29.786541 },
            { lng: 30.826006, lat: -29.787323 },
            { lng: 30.82886, lat: -29.787286 },
            { lng: 30.831671, lat: -29.787621 },
            { lng: 30.832057, lat: -29.787844 },
            { lng: 30.832357, lat: -29.78844 },
            { lng: 30.833173, lat: -29.787137 },
            { lng: 30.834718, lat: -29.786951 },
            { lng: 30.836692, lat: -29.786951 },
            { lng: 30.837658, lat: -29.785088 },
            { lng: 30.836391, lat: -29.784436 },
            { lng: 30.83549, lat: -29.783598 },
            { lng: 30.834224, lat: -29.782704 },
            { lng: 30.832679, lat: -29.781643 },
            { lng: 30.831242, lat: -29.780786 },
            { lng: 30.83077, lat: -29.780898 },
            { lng: 30.831242, lat: -29.77965 },
            { lng: 30.831757, lat: -29.778924 },
            { lng: 30.83122, lat: -29.778067 },
            { lng: 30.832057, lat: -29.777955 },
            { lng: 30.833774, lat: -29.778067 },
            { lng: 30.835512, lat: -29.777806 },
            { lng: 30.837593, lat: -29.775646 },
            { lng: 30.835769, lat: -29.774175 },
            { lng: 30.834117, lat: -29.774622 },
            { lng: 30.8324, lat: -29.774864 },
            { lng: 30.829074, lat: -29.773169 },
            { lng: 30.828624, lat: -29.773895 },
            { lng: 30.827808, lat: -29.775348 },
            { lng: 30.825877, lat: -29.77641 },
            { lng: 30.824204, lat: -29.778123 },
            { lng: 30.822165, lat: -29.78006 },
        ];

        var B5SPMap = new google.maps.Polygon({
            paths: B5SP,
            strokeColor: "#7C3592",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#7C3592",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'B5(SP)',
            map: map
        });

        CrmJs.Sales.Map.polygonsSS.push(B5SPMap);

        var B6SP = [
            { lng: 30.837593, lat: -29.775646 },
            { lng: 30.835512, lat: -29.777806 },
            { lng: 30.833774, lat: -29.778067 },
            { lng: 30.832057, lat: -29.777955 },
            { lng: 30.83122, lat: -29.778067 },
            { lng: 30.831757, lat: -29.778924 },
            { lng: 30.831242, lat: -29.77965 },
            { lng: 30.83077, lat: -29.780898 },
            { lng: 30.831242, lat: -29.780786 },
            { lng: 30.833881, lat: -29.7825 },
            { lng: 30.835211, lat: -29.78345 },
            { lng: 30.836391, lat: -29.784436 },
            { lng: 30.837443, lat: -29.785032 },
            { lng: 30.837775, lat: -29.783792 },
            { lng: 30.838108, lat: -29.782923 },
            { lng: 30.840404, lat: -29.778589 },
            { lng: 30.840404, lat: -29.775385 },
            { lng: 30.841734, lat: -29.773951 },
            { lng: 30.842807, lat: -29.773132 },
            { lng: 30.844159, lat: -29.772647 },
            { lng: 30.845597, lat: -29.77166 },
            { lng: 30.847185, lat: -29.770654 },
            { lng: 30.849052, lat: -29.769406 },
            { lng: 30.848322, lat: -29.768829 },
            { lng: 30.845361, lat: -29.767656 },
            { lng: 30.842571, lat: -29.770468 },
            { lng: 30.841412, lat: -29.772182 },
            { lng: 30.837593, lat: -29.775646 },
        ];

        var B6SPMap = new google.maps.Polygon({
            paths: B6SP,
            strokeColor: "#A61B4A",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#A61B4A",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'B6(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS.push(B6SPMap);

        var B7 = [
            { lng: 30.849068, lat: -29.769397 },
            { lng: 30.844175, lat: -29.772638 },
            { lng: 30.842823, lat: -29.773122 },
            { lng: 30.84175, lat: -29.773942 },
            { lng: 30.84042, lat: -29.775376 },
            { lng: 30.84042, lat: -29.778579 },
            { lng: 30.838108, lat: -29.782923 },
            { lng: 30.836708, lat: -29.786941 },
            { lng: 30.835018, lat: -29.789423 },
            { lng: 30.834932, lat: -29.789523 },
            { lng: 30.83552, lat: -29.789394 },
            { lng: 30.839514, lat: -29.789185 },
            { lng: 30.841139, lat: -29.786899 },
            { lng: 30.840876, lat: -29.786448 },
            { lng: 30.840313, lat: -29.786322 },
            { lng: 30.839379, lat: -29.78555 },
            { lng: 30.83881, lat: -29.785444 },
            { lng: 30.838175, lat: -29.785953 },
            { lng: 30.83784, lat: -29.785872 },
            { lng: 30.837555, lat: -29.785394 },
            { lng: 30.837577, lat: -29.785006 },
            { lng: 30.837872, lat: -29.783919 },
            { lng: 30.838338, lat: -29.783058 },
            { lng: 30.841198, lat: -29.784354 },
            { lng: 30.841666, lat: -29.784127 },
            { lng: 30.841806, lat: -29.783776 },
            { lng: 30.843192, lat: -29.782989 },
            { lng: 30.842945, lat: -29.78247 },
            { lng: 30.842981, lat: -29.782081 },
            { lng: 30.843513, lat: -29.781518 },
            { lng: 30.844343, lat: -29.782292 },
            { lng: 30.845122, lat: -29.782742 },
            { lng: 30.846466, lat: -29.783026 },
            { lng: 30.847174, lat: -29.783529 },
            { lng: 30.847678, lat: -29.782153 },
            { lng: 30.847839, lat: -29.781298 },
            { lng: 30.847678, lat: -29.780991 },
            { lng: 30.84771, lat: -29.780377 },
            { lng: 30.848247, lat: -29.779678 },
            { lng: 30.848354, lat: -29.779129 },
            { lng: 30.848311, lat: -29.778831 },
            { lng: 30.848451, lat: -29.778449 },
            { lng: 30.848703, lat: -29.778128 },
            { lng: 30.849143, lat: -29.777881 },
            { lng: 30.849513, lat: -29.777536 },
            { lng: 30.849754, lat: -29.777406 },
            { lng: 30.850763, lat: -29.778039 },
            { lng: 30.851079, lat: -29.778351 },
            { lng: 30.85146, lat: -29.778826 },
            { lng: 30.851702, lat: -29.779348 },
            { lng: 30.852023, lat: -29.779739 },
            { lng: 30.852256, lat: -29.780319 },
            { lng: 30.852402, lat: -29.780937 },
            { lng: 30.85242, lat: -29.78135 },
            { lng: 30.852758, lat: -29.781564 },
            { lng: 30.852812, lat: -29.781727 },
            { lng: 30.852619, lat: -29.781974 },
            { lng: 30.85323, lat: -29.782379 },
            { lng: 30.853563, lat: -29.782658 },
            { lng: 30.853579, lat: -29.782919 },
            { lng: 30.854285, lat: -29.783608 },
            { lng: 30.855033, lat: -29.784073 },
            { lng: 30.855784, lat: -29.784162 },
            { lng: 30.857393, lat: -29.784148 },
            { lng: 30.858208, lat: -29.784199 },
            { lng: 30.85867, lat: -29.78446 },
            { lng: 30.858252, lat: -29.785149 },
            { lng: 30.857211, lat: -29.786355 },
            { lng: 30.856519, lat: -29.786825 },
            { lng: 30.85551, lat: -29.786923 },
            { lng: 30.85506, lat: -29.787453 },
            { lng: 30.854588, lat: -29.787779 },
            { lng: 30.854491, lat: -29.788729 },
            { lng: 30.85462, lat: -29.78952 },
            { lng: 30.853836, lat: -29.790498 },
            { lng: 30.854153, lat: -29.790759 },
            { lng: 30.857307, lat: -29.788841 },
            { lng: 30.86147, lat: -29.788506 },
            { lng: 30.864345, lat: -29.781615 },
            { lng: 30.867822, lat: -29.778226 },
            { lng: 30.868079, lat: -29.776438 },
            { lng: 30.871298, lat: -29.775618 },
            { lng: 30.873486, lat: -29.7764 },
            { lng: 30.876233, lat: -29.778263 },
            { lng: 30.879666, lat: -29.77938 },
            { lng: 30.88104, lat: -29.779566 },
            { lng: 30.884473, lat: -29.779492 },
            { lng: 30.888421, lat: -29.780498 },
            { lng: 30.888292, lat: -29.772061 },
            { lng: 30.890953, lat: -29.771893 },
            { lng: 30.890652, lat: -29.769416 },
            { lng: 30.890159, lat: -29.768205 },
            { lng: 30.886297, lat: -29.768019 },
            { lng: 30.867972, lat: -29.768429 },
            { lng: 30.859002, lat: -29.769732 },
            { lng: 30.85632, lat: -29.769528 },
            { lng: 30.849068, lat: -29.769397 },
        ];

        var B7Map = new google.maps.Polygon({
            paths: B7,
            strokeColor: "#DB4436",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#DB4436",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'B7',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS.push(B7Map);

        var B8 = [
            { lng: 30.812316, lat: -29.793692 },
            { lng: 30.8115, lat: -29.798943 },
            { lng: 30.814376, lat: -29.800023 },
            { lng: 30.815706, lat: -29.798012 },
            { lng: 30.817536, lat: -29.79864 },
            { lng: 30.817949, lat: -29.797947 },
            { lng: 30.819601, lat: -29.798552 },
            { lng: 30.820888, lat: -29.798766 },
            { lng: 30.821328, lat: -29.79965 },
            { lng: 30.82206, lat: -29.799671 },
            { lng: 30.822718, lat: -29.799469 },
            { lng: 30.82346, lat: -29.79955 },
            { lng: 30.823668, lat: -29.800544 },
            { lng: 30.823403, lat: -29.801537 },
            { lng: 30.826607, lat: -29.805981 },
            { lng: 30.831413, lat: -29.803114 },
            { lng: 30.828066, lat: -29.793356 },
            { lng: 30.823002, lat: -29.793003 },
            { lng: 30.818152, lat: -29.793468 },
            { lng: 30.812316, lat: -29.793692 },
        ];

        var B8Map = new google.maps.Polygon({
            paths: B8,
            strokeColor: "#4186F0",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#4186F0",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'B8(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS.push(B8Map);

        var B9 = [
            { lng: 30.8115, lat: -29.798943 },
            { lng: 30.810556, lat: -29.806391 },
            { lng: 30.811801, lat: -29.817599 },
            { lng: 30.814977, lat: -29.815551 },
            { lng: 30.81811, lat: -29.811902 },
            { lng: 30.820427, lat: -29.810329 },
            { lng: 30.822744, lat: -29.808979 },
            { lng: 30.826607, lat: -29.805981 },
            { lng: 30.823403, lat: -29.801537 },
            { lng: 30.823668, lat: -29.800544 },
            { lng: 30.82346, lat: -29.79955 },
            { lng: 30.822718, lat: -29.799469 },
            { lng: 30.82206, lat: -29.799671 },
            { lng: 30.821328, lat: -29.79965 },
            { lng: 30.820888, lat: -29.798766 },
            { lng: 30.819499, lat: -29.798566 },
            { lng: 30.817949, lat: -29.797947 },
            { lng: 30.817536, lat: -29.79864 },
            { lng: 30.815706, lat: -29.798012 },
            { lng: 30.814376, lat: -29.800023 },
            { lng: 30.8115, lat: -29.798943 },
        ];

        var B9Map = new google.maps.Polygon({
            paths: B9,
            strokeColor: "#A61B4A",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#A61B4A",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'B9(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS.push(B9Map);

        var B10 = [
            { lng: 30.85036, lat: -29.777746 },
            { lng: 30.849776, lat: -29.777411 },
            { lng: 30.849534, lat: -29.777541 },
            { lng: 30.849164, lat: -29.777886 },
            { lng: 30.848724, lat: -29.778132 },
            { lng: 30.848472, lat: -29.778454 },
            { lng: 30.848333, lat: -29.778835 },
            { lng: 30.848376, lat: -29.779133 },
            { lng: 30.848268, lat: -29.779683 },
            { lng: 30.847732, lat: -29.780381 },
            { lng: 30.8477, lat: -29.780996 },
            { lng: 30.84786, lat: -29.781303 },
            { lng: 30.84771, lat: -29.782225 },
            { lng: 30.847399, lat: -29.782942 },
            { lng: 30.847185, lat: -29.783529 },
            { lng: 30.846476, lat: -29.783026 },
            { lng: 30.845133, lat: -29.782742 },
            { lng: 30.844354, lat: -29.782292 },
            { lng: 30.843524, lat: -29.781518 },
            { lng: 30.842991, lat: -29.782081 },
            { lng: 30.842956, lat: -29.78247 },
            { lng: 30.84327, lat: -29.782952 },
            { lng: 30.842628, lat: -29.783315 },
            { lng: 30.842238, lat: -29.78352 },
            { lng: 30.841817, lat: -29.783776 },
            { lng: 30.841676, lat: -29.784127 },
            { lng: 30.841209, lat: -29.784354 },
            { lng: 30.839892, lat: -29.78375 },
            { lng: 30.839228, lat: -29.783453 },
            { lng: 30.838349, lat: -29.783058 },
            { lng: 30.83792, lat: -29.783838 },
            { lng: 30.837588, lat: -29.785006 },
            { lng: 30.837566, lat: -29.785394 },
            { lng: 30.83785, lat: -29.785872 },
            { lng: 30.838186, lat: -29.785953 },
            { lng: 30.838831, lat: -29.785448 },
            { lng: 30.839389, lat: -29.78555 },
            { lng: 30.839722, lat: -29.785857 },
            { lng: 30.840334, lat: -29.786327 },
            { lng: 30.840887, lat: -29.786448 },
            { lng: 30.84115, lat: -29.786899 },
            { lng: 30.839524, lat: -29.789185 },
            { lng: 30.835587, lat: -29.789385 },
            { lng: 30.834943, lat: -29.789523 },
            { lng: 30.834617, lat: -29.789893 },
            { lng: 30.834227, lat: -29.790412 },
            { lng: 30.834979, lat: -29.790886 },
            { lng: 30.836088, lat: -29.791011 },
            { lng: 30.8353219, lat: -29.7922471 },
            { lng: 30.8361337, lat: -29.7929783 },
            { lng: 30.8370171, lat: -29.7919845 },
            { lng: 30.8374266, lat: -29.7913153 },
            { lng: 30.8375616, lat: -29.7907806 },
            { lng: 30.8372621, lat: -29.7905717 },
            { lng: 30.837493, lat: -29.790162 },
            { lng: 30.838714, lat: -29.790568 },
            { lng: 30.839361, lat: -29.791061 },
            { lng: 30.841799, lat: -29.791667 },
            { lng: 30.842555, lat: -29.791983 },
            { lng: 30.84402, lat: -29.792803 },
            { lng: 30.844717, lat: -29.792298 },
            { lng: 30.845854, lat: -29.792532 },
            { lng: 30.84646, lat: -29.79264 },
            { lng: 30.849041, lat: -29.793305 },
            { lng: 30.85035, lat: -29.796112 },
            { lng: 30.851873, lat: -29.794716 },
            { lng: 30.852538, lat: -29.793506 },
            { lng: 30.85404, lat: -29.79304 },
            { lng: 30.854233, lat: -29.790768 },
            { lng: 30.853858, lat: -29.790503 },
            { lng: 30.854641, lat: -29.789525 },
            { lng: 30.854512, lat: -29.788734 },
            { lng: 30.854609, lat: -29.787784 },
            { lng: 30.855081, lat: -29.787458 },
            { lng: 30.855532, lat: -29.786927 },
            { lng: 30.856465, lat: -29.786825 },
            { lng: 30.857232, lat: -29.786359 },
            { lng: 30.857801, lat: -29.785684 },
            { lng: 30.858273, lat: -29.785153 },
            { lng: 30.858691, lat: -29.784464 },
            { lng: 30.85823, lat: -29.784204 },
            { lng: 30.857414, lat: -29.784152 },
            { lng: 30.856588, lat: -29.784194 },
            { lng: 30.855805, lat: -29.784166 },
            { lng: 30.855054, lat: -29.784078 },
            { lng: 30.854614, lat: -29.78385 },
            { lng: 30.85403, lat: -29.783394 },
            { lng: 30.853831, lat: -29.78317 },
            { lng: 30.8536, lat: -29.782923 },
            { lng: 30.853584, lat: -29.782663 },
            { lng: 30.853252, lat: -29.782383 },
            { lng: 30.852989, lat: -29.782225 },
            { lng: 30.85264, lat: -29.781978 },
            { lng: 30.852833, lat: -29.781731 },
            { lng: 30.85278, lat: -29.781568 },
            { lng: 30.852442, lat: -29.781354 },
            { lng: 30.852442, lat: -29.781042 },
            { lng: 30.852377, lat: -29.780637 },
            { lng: 30.852254, lat: -29.780232 },
            { lng: 30.852045, lat: -29.779743 },
            { lng: 30.851723, lat: -29.779352 },
            { lng: 30.851482, lat: -29.778831 },
            { lng: 30.851101, lat: -29.778356 },
            { lng: 30.85036, lat: -29.777746 },
        ];

        var B10Map = new google.maps.Polygon({
            paths: B10,
            strokeColor: "#009D57",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#009D57",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'B10(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS.push(B10Map);

        var B11 = [
            { lng: 30.800261, lat: -29.801567 },
            { lng: 30.800089, lat: -29.80341 },
            { lng: 30.800686, lat: -29.806874 },
            { lng: 30.802234, lat: -29.806618 },
            { lng: 30.801764, lat: -29.804892 },
            { lng: 30.802107, lat: -29.802313 },
            { lng: 30.803424, lat: -29.801731 },
            { lng: 30.80434, lat: -29.801566 },
            { lng: 30.804637, lat: -29.800427 },
            { lng: 30.806851, lat: -29.800132 },
            { lng: 30.807931, lat: -29.801246 },
            { lng: 30.80861, lat: -29.803036 },
            { lng: 30.810921, lat: -29.802816 },
            { lng: 30.812101, lat: -29.79965 },
            { lng: 30.812316, lat: -29.793692 },
            { lng: 30.811908, lat: -29.792649 },
            { lng: 30.809484, lat: -29.792239 },
            { lng: 30.806243, lat: -29.792742 },
            { lng: 30.797596, lat: -29.79399 },
            { lng: 30.798008, lat: -29.794305 },
            { lng: 30.796892, lat: -29.795552 },
            { lng: 30.794446, lat: -29.796039 },
            { lng: 30.794607, lat: -29.796404 },
            { lng: 30.794022, lat: -29.796618 },
            { lng: 30.794408, lat: -29.797531 },
            { lng: 30.79406, lat: -29.797624 },
            { lng: 30.793845, lat: -29.797936 },
            { lng: 30.795422, lat: -29.801074 },
            { lng: 30.795047, lat: -29.801344 },
            { lng: 30.794789, lat: -29.802144 },
            { lng: 30.800261, lat: -29.801567 },
        ];

        var B11Map = new google.maps.Polygon({
            paths: B11,
            strokeColor: "#7C3592",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#7C3592",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'B11(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS.push(B11Map);

        var B12 = [
            { lng: 30.779389, lat: -29.807526 },
            { lng: 30.780799, lat: -29.808624 },
            { lng: 30.783031, lat: -29.809368 },
            { lng: 30.785434, lat: -29.808214 },
            { lng: 30.789571, lat: -29.807768 },
            { lng: 30.790197, lat: -29.804453 },
            { lng: 30.7917, lat: -29.803596 },
            { lng: 30.792086, lat: -29.802256 },
            { lng: 30.794789, lat: -29.802144 },
            { lng: 30.795047, lat: -29.801344 },
            { lng: 30.795422, lat: -29.801074 },
            { lng: 30.794339, lat: -29.79882 },
            { lng: 30.793845, lat: -29.797936 },
            { lng: 30.79406, lat: -29.797624 },
            { lng: 30.794408, lat: -29.797531 },
            { lng: 30.794022, lat: -29.796618 },
            { lng: 30.794607, lat: -29.796404 },
            { lng: 30.794446, lat: -29.796039 },
            { lng: 30.796892, lat: -29.795552 },
            { lng: 30.798008, lat: -29.794305 },
            { lng: 30.797596, lat: -29.79399 },
            { lng: 30.791378, lat: -29.793523 },
            { lng: 30.783402, lat: -29.792304 },
            { lng: 30.780816, lat: -29.794362 },
            { lng: 30.77822, lat: -29.796242 },
            { lng: 30.777608, lat: -29.797146 },
            { lng: 30.777576, lat: -29.799036 },
            { lng: 30.77882, lat: -29.801065 },
            { lng: 30.781031, lat: -29.804566 },
            { lng: 30.779389, lat: -29.807526 },
        ];

        var B12Map = new google.maps.Polygon({
            paths: B12,
            strokeColor: "#F9F7A6",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F9F7A6",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'B12(SP)',
            map: map
        });

        CrmJs.Sales.Map.polygonsSS.push(B12Map);

        var NoGoZone = [
            { lng: 30.800261, lat: -29.801567 },
            { lng: 30.794789, lat: -29.802144 },
            { lng: 30.792086, lat: -29.802256 },
            { lng: 30.7917, lat: -29.803596 },
            { lng: 30.790197, lat: -29.804453 },
            { lng: 30.788863, lat: -29.81125 },
            { lng: 30.794678, lat: -29.811157 },
            { lng: 30.799956, lat: -29.813801 },
            { lng: 30.8023576, lat: -29.8157735 },
            { lng: 30.8037326, lat: -29.815234 },
            { lng: 30.8078097, lat: -29.8146386 },
            { lng: 30.811801, lat: -29.817599 },
            { lng: 30.810556, lat: -29.806391 },
            { lng: 30.810921, lat: -29.802816 },
            { lng: 30.80861, lat: -29.803036 },
            { lng: 30.807931, lat: -29.801246 },
            { lng: 30.806851, lat: -29.800132 },
            { lng: 30.804637, lat: -29.800427 },
            { lng: 30.80434, lat: -29.801566 },
            { lng: 30.802107, lat: -29.802313 },
            { lng: 30.801764, lat: -29.804892 },
            { lng: 30.802234, lat: -29.806618 },
            { lng: 30.800686, lat: -29.806874 },
            { lng: 30.800089, lat: -29.80341 },
            { lng: 30.800261, lat: -29.801567 },
        ];

        var NoGoZoneMap = new google.maps.Polygon({
            paths: NoGoZone,
            strokeColor: "#000000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#000000",
            fillOpacity: 0.45,
            draggable: false,
            geodesic: false,
            name: 'NoCoverage',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS.push(NoGoZoneMap);
    },

    DrawSubSector2: function (map) {
        var D1 = [
            { lng: 30.9701115, lat: -29.8765221 },
            { lng: 30.969022, lat: -29.875831 },
            { lng: 30.96525, lat: -29.878901 },
            { lng: 30.962681, lat: -29.881779 },
            { lng: 30.968785, lat: -29.892131 },
            { lng: 30.970851, lat: -29.8881336 },
            { lng: 30.9727719, lat: -29.8842163 },
            { lng: 30.9746872, lat: -29.8805181 },
            { lng: 30.9749931, lat: -29.8798339 },
            { lng: 30.9730297, lat: -29.8789874 },
            { lng: 30.9727186, lat: -29.8781129 },
            { lng: 30.9716799, lat: -29.8777676 },
            { lng: 30.9705514, lat: -29.8769966 },
            { lng: 30.9701115, lat: -29.8765221 },
        ];

        var D1Map = new google.maps.Polygon({
            paths: D1,
            strokeColor: "#CE93D8",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#CE93D8",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'D1(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS2.push(D1Map);

        var D2 = [
            { lng: 30.968785, lat: -29.892131 },
            { lng: 30.969668, lat: -29.893453 },
            { lng: 30.971216, lat: -29.894309 },
            { lng: 30.9734942, lat: -29.8962116 },
            { lng: 30.978125, lat: -29.9 },
            { lng: 30.9780119, lat: -29.9018413 },
            { lng: 30.9805439, lat: -29.900428 },
            { lng: 30.9827698, lat: -29.8967756 },
            { lng: 30.9856822, lat: -29.8974388 },
            { lng: 30.9875905, lat: -29.8894339 },
            { lng: 30.9914782, lat: -29.8850666 },
            { lng: 30.995027, lat: -29.884612 },
            { lng: 31.002955, lat: -29.883514 },
            { lng: 31.003578, lat: -29.883393 },
            { lng: 31.005788, lat: -29.878174 },
            { lng: 31.007719, lat: -29.87357 },
            { lng: 31.010852, lat: -29.870267 },
            { lng: 30.997538, lat: -29.859587 },
            { lng: 30.992087, lat: -29.864388 },
            { lng: 30.991465, lat: -29.867923 },
            { lng: 30.987111, lat: -29.877859 },
            { lng: 30.985437, lat: -29.876148 },
            { lng: 30.98165, lat: -29.879618 },
            { lng: 30.975191, lat: -29.87958 },
            { lng: 30.968785, lat: -29.892131 },
        ];

        var D2Map = new google.maps.Polygon({
            paths: D2,
            strokeColor: "#7CB342",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#7CB342",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'D2',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS2.push(D2Map);

        var D3 = [
            { lng: 30.983656, lat: -29.851364 },
            { lng: 30.975501, lat: -29.848684 },
            { lng: 30.973115, lat: -29.850877 },
            { lng: 30.970809, lat: -29.852207 },
            { lng: 30.969393, lat: -29.854952 },
            { lng: 30.969961, lat: -29.857865 },
            { lng: 30.968148, lat: -29.86061 },
            { lng: 30.967343, lat: -29.861931 },
            { lng: 30.96597, lat: -29.862377 },
            { lng: 30.965509, lat: -29.863038 },
            { lng: 30.965573, lat: -29.863717 },
            { lng: 30.965428, lat: -29.864006 },
            { lng: 30.97399, lat: -29.871644 },
            { lng: 30.97693, lat: -29.872705 },
            { lng: 30.97813, lat: -29.872503 },
            { lng: 30.980084, lat: -29.872146 },
            { lng: 30.983324, lat: -29.873895 },
            { lng: 30.987283, lat: -29.877914 },
            { lng: 30.991637, lat: -29.867978 },
            { lng: 30.992259, lat: -29.864443 },
            { lng: 30.997837, lat: -29.859828 },
            { lng: 30.99221, lat: -29.856745 },
            { lng: 30.984781, lat: -29.851614 },
            { lng: 30.983656, lat: -29.851364 },
        ];
        var D3Map = new google.maps.Polygon({
            paths: D3,
            strokeColor: "#A1C2FA",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#A1C2FA",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'D3',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS2.push(D3Map);

        var D4 = [
            { lng: 31.031952, lat: -29.829651 },
            { lng: 31.025572, lat: -29.828411 },
            { lng: 31.02387, lat: -29.837224 },
            { lng: 31.020093, lat: -29.847609 },
            { lng: 31.015823, lat: -29.84733 },
            { lng: 31.010582, lat: -29.847469 },
            { lng: 31.010963, lat: -29.852522 },
            { lng: 31.013527, lat: -29.869512 },
            { lng: 31.021187, lat: -29.863018 },
            { lng: 31.031227, lat: -29.862195 },
            { lng: 31.033974, lat: -29.864837 },
            { lng: 31.032386, lat: -29.870122 },
            { lng: 31.034145, lat: -29.872094 },
            { lng: 31.035476, lat: -29.868522 },
            { lng: 31.042728, lat: -29.874811 },
            { lng: 31.044702, lat: -29.873285 },
            { lng: 31.050754, lat: -29.874327 },
            { lng: 31.054144, lat: -29.870345 },
            { lng: 31.050625, lat: -29.868485 },
            { lng: 31.047344, lat: -29.867261 },
            { lng: 31.045604, lat: -29.864875 },
            { lng: 31.04025, lat: -29.853011 },
            { lng: 31.037388, lat: -29.843961 },
            { lng: 31.036913, lat: -29.833282 },
            { lng: 31.03745, lat: -29.830016 },
            { lng: 31.031952, lat: -29.829651 },
        ];

        var D4Map = new google.maps.Polygon({
            paths: D4,
            strokeColor: "#CE93D8",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#CE93D8",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'D4',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS2.push(D4Map);

        var D5 = [
            { lng: 30.9990847, lat: -29.8218948 },
            { lng: 30.9913997, lat: -29.8364468 },
            { lng: 30.9909127, lat: -29.8375088 },
            { lng: 30.9902837, lat: -29.8408958 },
            { lng: 31.0022857, lat: -29.8459748 },
            { lng: 31.0034047, lat: -29.8465588 },
            { lng: 31.0044267, lat: -29.8475398 },
            { lng: 31.0061777, lat: -29.8443518 },
            { lng: 31.0069827, lat: -29.8440348 },
            { lng: 31.0073797, lat: -29.8433468 },
            { lng: 31.0080017, lat: -29.8425088 },
            { lng: 31.0094607, lat: -29.8402758 },
            { lng: 31.0144987, lat: -29.8323388 },
            { lng: 31.0150248, lat: -29.8319954 },
            { lng: 31.0144377, lat: -29.8317382 },
            { lng: 31.0119945, lat: -29.8304199 },
            { lng: 31.0099106, lat: -29.8290546 },
            { lng: 31.0102312, lat: -29.8274784 },
            { lng: 31.0109601, lat: -29.8272581 },
            { lng: 31.0112709, lat: -29.828088 },
            { lng: 31.0127781, lat: -29.8290055 },
            { lng: 31.0132957, lat: -29.8294596 },
            { lng: 31.0134151, lat: -29.8297658 },
            { lng: 31.0152918, lat: -29.8311666 },
            { lng: 31.0158383, lat: -29.8314879 },
            { lng: 31.0187057, lat: -29.8297368 },
            { lng: 31.0189327, lat: -29.8253908 },
            { lng: 31.0149597, lat: -29.8235528 },
            { lng: 31.0150347, lat: -29.8228828 },
            { lng: 31.0153117, lat: -29.8227658 },
            { lng: 31.0141207, lat: -29.8222258 },
            { lng: 31.0129127, lat: -29.8222528 },
            { lng: 31.0124197, lat: -29.8219188 },
            { lng: 30.9990847, lat: -29.8218948 },
        ];

        var D5Map = new google.maps.Polygon({
            paths: D5,
            strokeColor: "#0288D1",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#0288D1",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'D5',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS2.push(D5Map);

        var D6 = [
            { lng: 31.004416, lat: -29.847577 },
            { lng: 31.010582, lat: -29.847469 },
            { lng: 31.020093, lat: -29.847609 },
            { lng: 31.02387, lat: -29.837224 },
            { lng: 31.025572, lat: -29.828411 },
            { lng: 31.032579, lat: -29.829823 },
            { lng: 31.03745, lat: -29.830016 },
            { lng: 31.038523, lat: -29.822784 },
            { lng: 31.033684, lat: -29.822467 },
            { lng: 31.033115, lat: -29.820205 },
            { lng: 31.031399, lat: -29.817943 },
            { lng: 31.027408, lat: -29.818837 },
            { lng: 31.021928, lat: -29.820439 },
            { lng: 31.018594, lat: -29.8221814 },
            { lng: 31.017551, lat: -29.822896 },
            { lng: 31.01861, lat: -29.824064 },
            { lng: 31.017811, lat: -29.824562 },
            { lng: 31.019085, lat: -29.825179 },
            { lng: 31.018964, lat: -29.825391 },
            { lng: 31.018695, lat: -29.829774 },
            { lng: 31.0158276, lat: -29.8315251 },
            { lng: 31.0152454, lat: -29.8311727 },
            { lng: 31.0142633, lat: -29.8304493 },
            { lng: 31.0134044, lat: -29.829803 },
            { lng: 31.013285, lat: -29.8294968 },
            { lng: 31.0127674, lat: -29.8290427 },
            { lng: 31.0112602, lat: -29.8281252 },
            { lng: 31.0109494, lat: -29.8272953 },
            { lng: 31.0102205, lat: -29.8275156 },
            { lng: 31.0098999, lat: -29.8290918 },
            { lng: 31.0119838, lat: -29.8304571 },
            { lng: 31.0142848, lat: -29.8316986 },
            { lng: 31.0150141, lat: -29.8320326 },
            { lng: 31.014488, lat: -29.832376 },
            { lng: 31.006972, lat: -29.844072 },
            { lng: 31.006167, lat: -29.844389 },
            { lng: 31.004416, lat: -29.847577 },
        ];

        var D6Map = new google.maps.Polygon({
            paths: D6,
            strokeColor: "#558B2F",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#558B2F",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'D6',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS2.push(D6Map);

        var D7 = [
            { lng: 31.027022, lat: -29.818577 },
            { lng: 31.026989, lat: -29.818074 },
            { lng: 31.026764, lat: -29.817487 },
            { lng: 31.026506, lat: -29.81684 },
            { lng: 31.026335, lat: -29.81623 },
            { lng: 31.026164, lat: -29.81556 },
            { lng: 31.025176, lat: -29.814359 },
            { lng: 31.021614, lat: -29.811753 },
            { lng: 31.017151, lat: -29.811083 },
            { lng: 31.014318, lat: -29.812423 },
            { lng: 31.000156, lat: -29.813764 },
            { lng: 30.999117, lat: -29.821895 },
            { lng: 31.012452, lat: -29.821918 },
            { lng: 31.012192, lat: -29.820827 },
            { lng: 31.012471, lat: -29.819978 },
            { lng: 31.01361, lat: -29.819218 },
            { lng: 31.014201, lat: -29.819671 },
            { lng: 31.015808, lat: -29.820187 },
            { lng: 31.017975, lat: -29.820218 },
            { lng: 31.019069, lat: -29.821895 },
            { lng: 31.021928, lat: -29.820439 },
            { lng: 31.024275, lat: -29.819633 },
            { lng: 31.024919, lat: -29.819544 },
            { lng: 31.025573, lat: -29.819359 },
            { lng: 31.025991, lat: -29.819238 },
            { lng: 31.026635, lat: -29.819107 },
            { lng: 31.027022, lat: -29.818577 },
        ];
        var D7Map = new google.maps.Polygon({
            paths: D7,
            strokeColor: "#FFD600",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#FFD600",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'D7',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS2.push(D7Map);

        var D8 = [
            { lng: 31.015067, lat: -29.822882 },
            { lng: 31.014992, lat: -29.823553 },
            { lng: 31.018964, lat: -29.825391 },
            { lng: 31.019085, lat: -29.825179 },
            { lng: 31.017811, lat: -29.824562 },
            { lng: 31.01669, lat: -29.824008 },
            { lng: 31.015344, lat: -29.822766 },
            { lng: 31.015067, lat: -29.822882 },
        ];

        var D8Map = new google.maps.Polygon({
            paths: D8,
            strokeColor: "#E65100",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#E65100",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'D8(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS2.push(D8Map);

        var D9 = [
            { lng: 30.990273, lat: -29.840933 },
            { lng: 30.990029, lat: -29.841984 },
            { lng: 30.98534, lat: -29.847009 },
            { lng: 30.984729, lat: -29.847865 },
            { lng: 30.97857, lat: -29.845855 },
            { lng: 30.975501, lat: -29.848684 },
            { lng: 30.984781, lat: -29.851614 },
            { lng: 30.99221, lat: -29.856745 },
            { lng: 30.997538, lat: -29.859587 },
            { lng: 31.010852, lat: -29.870267 },
            { lng: 31.013613, lat: -29.869586 },
            { lng: 31.010963, lat: -29.852522 },
            { lng: 31.010582, lat: -29.847469 },
            { lng: 31.004416, lat: -29.847577 },
            { lng: 31.003394, lat: -29.846596 },
            { lng: 31.001251, lat: -29.845543 },
            { lng: 30.994817, lat: -29.842778 },
            { lng: 30.990273, lat: -29.840933 },
        ];

        var D9Map = new google.maps.Polygon({
            paths: D9,
            strokeColor: "#FADA80",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#FADA80",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'D9',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS2.push(D9Map);

        var D10 = [
            { lng: 31.017551, lat: -29.822896 },
            { lng: 31.019069, lat: -29.821895 },
            { lng: 31.017975, lat: -29.820218 },
            { lng: 31.015808, lat: -29.820187 },
            { lng: 31.014201, lat: -29.819671 },
            { lng: 31.01361, lat: -29.819218 },
            { lng: 31.012471, lat: -29.819978 },
            { lng: 31.012192, lat: -29.820827 },
            { lng: 31.012452, lat: -29.821918 },
            { lng: 31.012945, lat: -29.822253 },
            { lng: 31.014153, lat: -29.822226 },
            { lng: 31.015344, lat: -29.822766 },
            { lng: 31.01669, lat: -29.824008 },
            { lng: 31.017811, lat: -29.824562 },
            { lng: 31.01861, lat: -29.824064 },
            { lng: 31.017551, lat: -29.822896 },
        ];

        var D10Map = new google.maps.Polygon({
            paths: D10,
            strokeColor: "#CE93D8",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#CE93D8",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'D10(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS2.push(D10Map);

        var D11 = [
            { lng: 30.969686, lat: -29.847828 },
            { lng: 30.971532, lat: -29.850248 },
            { lng: 30.972412, lat: -29.850229 },
            { lng: 30.972583, lat: -29.850731 },
            { lng: 30.972884, lat: -29.850918 },
            { lng: 30.973291, lat: -29.850992 },
            { lng: 30.975501, lat: -29.848684 },
            { lng: 30.97563, lat: -29.848461 },
            { lng: 30.97857, lat: -29.845855 },
            { lng: 30.981982, lat: -29.846991 },
            { lng: 30.984686, lat: -29.847866 },
            { lng: 30.985565, lat: -29.846786 },
            { lng: 30.989943, lat: -29.84191 },
            { lng: 30.990902, lat: -29.837546 },
            { lng: 30.99485, lat: -29.830228 },
            { lng: 30.999117, lat: -29.821895 },
            { lng: 31.000156, lat: -29.813764 },
            { lng: 30.995307, lat: -29.813428 },
            { lng: 30.991968, lat: -29.812163 },
            { lng: 30.987718, lat: -29.810338 },
            { lng: 30.981172, lat: -29.807955 },
            { lng: 30.977892, lat: -29.806451 },
            { lng: 30.978742, lat: -29.802909 },
            { lng: 30.972734, lat: -29.80263 },
            { lng: 30.968742, lat: -29.802965 },
            { lng: 30.964751, lat: -29.804008 },
            { lng: 30.958014, lat: -29.806167 },
            { lng: 30.958185, lat: -29.809332 },
            { lng: 30.960674, lat: -29.810971 },
            { lng: 30.959215, lat: -29.814397 },
            { lng: 30.954494, lat: -29.819088 },
            { lng: 30.955138, lat: -29.820466 },
            { lng: 30.959129, lat: -29.820466 },
            { lng: 30.96179, lat: -29.820726 },
            { lng: 30.963335, lat: -29.821471 },
            { lng: 30.967455, lat: -29.822141 },
            { lng: 30.965781, lat: -29.826795 },
            { lng: 30.961704, lat: -29.830071 },
            { lng: 30.958486, lat: -29.838299 },
            { lng: 30.956554, lat: -29.84258 },
            { lng: 30.955868, lat: -29.847158 },
            { lng: 30.955181, lat: -29.848014 },
            { lng: 30.957327, lat: -29.847902 },
            { lng: 30.959558, lat: -29.847195 },
            { lng: 30.960975, lat: -29.847568 },
            { lng: 30.962992, lat: -29.847121 },
            { lng: 30.965395, lat: -29.846562 },
            { lng: 30.968442, lat: -29.84753 },
            { lng: 30.969686, lat: -29.847828 },
        ];

        var D11Map = new google.maps.Polygon({
            paths: D11,
            strokeColor: "#DB4436",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#DB4436",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'D11',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS2.push(D11Map);

        var D12 = [
            { lng: 30.9701115, lat: -29.8765221 },
            { lng: 30.9705514, lat: -29.8769966 },
            { lng: 30.9710664, lat: -29.8773687 },
            { lng: 30.9717637, lat: -29.8777966 },
            { lng: 30.9727186, lat: -29.8781129 },
            { lng: 30.9730297, lat: -29.8789874 },
            { lng: 30.9751433, lat: -29.8799083 },
            { lng: 30.975362, lat: -29.879599 },
            { lng: 30.981821, lat: -29.879636 },
            { lng: 30.985608, lat: -29.876166 },
            { lng: 30.983324, lat: -29.873859 },
            { lng: 30.980084, lat: -29.87211 },
            { lng: 30.976929, lat: -29.872668 },
            { lng: 30.976274, lat: -29.872445 },
            { lng: 30.969022, lat: -29.875831 },
            { lng: 30.9701115, lat: -29.8765221 },
        ];

        var D12Map = new google.maps.Polygon({
            paths: D12,
            strokeColor: "#F4B400",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F4B400",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'D12(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS2.push(D12Map);

        var NoGoZoneS2 = [
            { lng: 30.955181, lat: -29.848014 },
            { lng: 30.9537, lat: -29.849522 },
            { lng: 30.952091, lat: -29.851494 },
            { lng: 30.932479, lat: -29.852518 },
            { lng: 30.936921, lat: -29.872969 },
            { lng: 30.946148, lat: -29.872076 },
            { lng: 30.948379, lat: -29.873974 },
            { lng: 30.948873, lat: -29.878718 },
            { lng: 30.951576, lat: -29.879128 },
            { lng: 30.954323, lat: -29.878867 },
            { lng: 30.959794, lat: -29.879435 },
            { lng: 30.961554, lat: -29.877193 },
            { lng: 30.965438, lat: -29.878746 },
            { lng: 30.967219, lat: -29.877341 },
            { lng: 30.969022, lat: -29.875831 },
            { lng: 30.973516, lat: -29.873694 },
            { lng: 30.976274, lat: -29.872445 },
            { lng: 30.974493, lat: -29.871908 },
            { lng: 30.96518, lat: -29.863646 },
            { lng: 30.965395, lat: -29.86279 },
            { lng: 30.967584, lat: -29.861488 },
            { lng: 30.968678, lat: -29.859553 },
            { lng: 30.969386, lat: -29.858473 },
            { lng: 30.969558, lat: -29.856798 },
            { lng: 30.969772, lat: -29.854602 },
            { lng: 30.970931, lat: -29.852183 },
            { lng: 30.972948, lat: -29.850992 },
            { lng: 30.972583, lat: -29.850731 },
            { lng: 30.972412, lat: -29.850229 },
            { lng: 30.971532, lat: -29.850248 },
            { lng: 30.969686, lat: -29.847828 },
            { lng: 30.965395, lat: -29.846562 },
            { lng: 30.960975, lat: -29.847568 },
            { lng: 30.959558, lat: -29.847195 },
            { lng: 30.957327, lat: -29.847902 },
            { lng: 30.955181, lat: -29.848014 },
        ];

        var NoGoZoneS2Map = new google.maps.Polygon({
            paths: NoGoZoneS2,
            strokeColor: "#000000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#000000",
            fillOpacity: 0.45,
            draggable: false,
            geodesic: false,
            name: 'NoCoverage',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS2.push(NoGoZoneS2Map);
    },

    DrawSubSector3: function (map) {
        var F1OT = [
            { lng: 30.907812, lat: -29.93125 },
            { lng: 30.915625, lat: -29.93125 },
            { lng: 30.915625, lat: -29.939062 },
            { lng: 30.923438, lat: -29.939062 },
            { lng: 30.93125, lat: -29.939062 },
            { lng: 30.93125, lat: -29.93125 },
            { lng: 30.939062, lat: -29.93125 },
            { lng: 30.946875, lat: -29.9312872 },
            { lng: 30.9404782, lat: -29.9277539 },
            { lng: 30.9362022, lat: -29.9266981 },
            { lng: 30.929609, lat: -29.9236335 },
            { lng: 30.9260988, lat: -29.9209459 },
            { lng: 30.9234809, lat: -29.9180075 },
            { lng: 30.923438, lat: -29.915625 },
            { lng: 30.92215, lat: -29.916388 },
            { lng: 30.920262, lat: -29.914993 },
            { lng: 30.919896, lat: -29.9126 },
            { lng: 30.919068, lat: -29.911105 },
            { lng: 30.918231, lat: -29.911124 },
            { lng: 30.917169, lat: -29.910073 },
            { lng: 30.915206, lat: -29.910059 },
            { lng: 30.916504, lat: -29.907915 },
            { lng: 30.916166, lat: -29.90686 },
            { lng: 30.915632, lat: -29.906369 },
            { lng: 30.913446, lat: -29.902568 },
            { lng: 30.914261, lat: -29.901935 },
            { lng: 30.914595, lat: -29.90093 },
            { lng: 30.915625, lat: -29.9 },
            { lng: 30.907812, lat: -29.9 },
            { lng: 30.9, lat: -29.9 },
            { lng: 30.892188, lat: -29.9 },
            { lng: 30.892188, lat: -29.907812 },
            { lng: 30.892188, lat: -29.915625 },
            { lng: 30.884375, lat: -29.915625 },
            { lng: 30.876562, lat: -29.915625 },
            { lng: 30.876562, lat: -29.923438 },
            { lng: 30.884375, lat: -29.923438 },
            { lng: 30.884375, lat: -29.93125 },
            { lng: 30.892188, lat: -29.93125 },
            { lng: 30.892188, lat: -29.939062 },
            { lng: 30.9, lat: -29.939062 },
            { lng: 30.907812, lat: -29.939062 },
            { lng: 30.907614, lat: -29.937018 },
            { lng: 30.908876, lat: -29.935198 },
            { lng: 30.907812, lat: -29.93125 },
        ];

        var F1IN = [
            { lng: 30.900506, lat: -29.933438 },
            { lng: 30.903136, lat: -29.934752 },
            { lng: 30.906203, lat: -29.935595 },
            { lng: 30.907571, lat: -29.935477 },
            { lng: 30.908876, lat: -29.935198 },
            { lng: 30.908253, lat: -29.934603 },
            { lng: 30.906612, lat: -29.933794 },
            { lng: 30.905988, lat: -29.932942 },
            { lng: 30.906537, lat: -29.931748 },
            { lng: 30.906882, lat: -29.930503 },
            { lng: 30.908269, lat: -29.926464 },
            { lng: 30.908854, lat: -29.925509 },
            { lng: 30.908972, lat: -29.924022 },
            { lng: 30.908135, lat: -29.923575 },
            { lng: 30.908071, lat: -29.922841 },
            { lng: 30.908478, lat: -29.922106 },
            { lng: 30.908347, lat: -29.921616 },
            { lng: 30.908701, lat: -29.92024 },
            { lng: 30.907159, lat: -29.919093 },
            { lng: 30.906762, lat: -29.91914 },
            { lng: 30.905925, lat: -29.919656 },
            { lng: 30.90453, lat: -29.920172 },
            { lng: 30.90393, lat: -29.920032 },
            { lng: 30.903532, lat: -29.919716 },
            { lng: 30.903007, lat: -29.919577 },
            { lng: 30.902985, lat: -29.9194 },
            { lng: 30.903329, lat: -29.91887 },
            { lng: 30.903522, lat: -29.918387 },
            { lng: 30.902792, lat: -29.918024 },
            { lng: 30.90204, lat: -29.918109 },
            { lng: 30.901514, lat: -29.918375 },
            { lng: 30.900593, lat: -29.918359 },
            { lng: 30.899906, lat: -29.917698 },
            { lng: 30.899148, lat: -29.917984 },
            { lng: 30.898308, lat: -29.918461 },
            { lng: 30.897996, lat: -29.91954 },
            { lng: 30.898002, lat: -29.920762 },
            { lng: 30.896996, lat: -29.921183 },
            { lng: 30.895926, lat: -29.921372 },
            { lng: 30.895046, lat: -29.922934 },
            { lng: 30.8951, lat: -29.923408 },
            { lng: 30.895652, lat: -29.924064 },
            { lng: 30.896277, lat: -29.925656 },
            { lng: 30.896516, lat: -29.926551 },
            { lng: 30.896945, lat: -29.927481 },
            { lng: 30.897621, lat: -29.927369 },
            { lng: 30.898007, lat: -29.927769 },
            { lng: 30.897578, lat: -29.931795 },
            { lng: 30.900506, lat: -29.933438 },
        ];

        var F1Map = new google.maps.Polygon({
            paths: [F1OT, F1IN],
            strokeColor: "#3F5BA9",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#3F5BA9",
            fillOpacity: 0.20,
            draggable: false,
            geodesic: false,
            name: 'F1',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS3.push(F1Map);

        var F2 = [
            { lng: 30.86875, lat: -29.93125 },
            { lng: 30.876562, lat: -29.93125 },
            { lng: 30.884375, lat: -29.93125 },
            { lng: 30.884375, lat: -29.923438 },
            { lng: 30.876562, lat: -29.923438 },
            { lng: 30.876562, lat: -29.915625 },
            { lng: 30.884375, lat: -29.915625 },
            { lng: 30.892188, lat: -29.915625 },
            { lng: 30.892188, lat: -29.907812 },
            { lng: 30.892188, lat: -29.9 },
            { lng: 30.884375, lat: -29.9 },
            { lng: 30.884375, lat: -29.907812 },
            { lng: 30.876562, lat: -29.907812 },
            { lng: 30.876562, lat: -29.9 },
            { lng: 30.876562, lat: -29.892188 },
            { lng: 30.884375, lat: -29.892188 },
            { lng: 30.884375, lat: -29.884375 },
            { lng: 30.884375, lat: -29.876562 },
            { lng: 30.876562, lat: -29.876562 },
            { lng: 30.874867, lat: -29.881891 },
            { lng: 30.86875, lat: -29.884375 },
            { lng: 30.860938, lat: -29.884375 },
            { lng: 30.860938, lat: -29.876562 },
            { lng: 30.853125, lat: -29.876562 },
            { lng: 30.853125, lat: -29.884375 },
            { lng: 30.853125, lat: -29.892188 },
            { lng: 30.853125, lat: -29.9 },
            { lng: 30.853125, lat: -29.907812 },
            { lng: 30.853125, lat: -29.915625 },
            { lng: 30.853125, lat: -29.923438 },
            { lng: 30.860938, lat: -29.923438 },
            { lng: 30.860938, lat: -29.93125 },
            { lng: 30.86875, lat: -29.93125 },
        ];
        var F2Map = new google.maps.Polygon({
            paths: F2,
            strokeColor: "#F4EB37",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F4EB37",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'F2',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS3.push(F2Map);

        var F3 = [
            { lng: 30.897503, lat: -29.865172 },
            { lng: 30.894678, lat: -29.863844 },
            { lng: 30.890986, lat: -29.862355 },
            { lng: 30.88656, lat: -29.860855 },
            { lng: 30.881968, lat: -29.858771 },
            { lng: 30.880766, lat: -29.856184 },
            { lng: 30.880895, lat: -29.851718 },
            { lng: 30.876562, lat: -29.853125 },
            { lng: 30.86875, lat: -29.853125 },
            { lng: 30.860938, lat: -29.853125 },
            { lng: 30.860938, lat: -29.860938 },
            { lng: 30.860938, lat: -29.86875 },
            { lng: 30.86875, lat: -29.86875 },
            { lng: 30.86875, lat: -29.876562 },
            { lng: 30.860938, lat: -29.876562 },
            { lng: 30.860938, lat: -29.884375 },
            { lng: 30.86875, lat: -29.884375 },
            { lng: 30.874867, lat: -29.881891 },
            { lng: 30.876562, lat: -29.876562 },
            { lng: 30.884375, lat: -29.876562 },
            { lng: 30.884375, lat: -29.884375 },
            { lng: 30.884375, lat: -29.892188 },
            { lng: 30.892188, lat: -29.892188 },
            { lng: 30.892188, lat: -29.884375 },
            { lng: 30.892188, lat: -29.876562 },
            { lng: 30.892188, lat: -29.86875 },
            { lng: 30.9, lat: -29.86875 },
            { lng: 30.897503, lat: -29.865172 },
        ];
        var F3Map = new google.maps.Polygon({
            paths: F3,
            strokeColor: "#009D57",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#009D57",
            fillOpacity: 0.20,
            draggable: false,
            geodesic: false,
            name: 'F3',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS3.push(F3Map);

        var F4 = [
            { lng: 30.907812, lat: -29.884375 },
            { lng: 30.915625, lat: -29.884375 },
            { lng: 30.915625, lat: -29.876562 },
            { lng: 30.914514, lat: -29.874164 },
            { lng: 30.912716, lat: -29.87318 },
            { lng: 30.911063, lat: -29.874974 },
            { lng: 30.909687, lat: -29.874764 },
            { lng: 30.90698, lat: -29.874718 },
            { lng: 30.905384, lat: -29.87403 },
            { lng: 30.904209, lat: -29.872988 },
            { lng: 30.904584, lat: -29.872038 },
            { lng: 30.9057, lat: -29.870829 },
            { lng: 30.9055666, lat: -29.8706192 },
            { lng: 30.9040046, lat: -29.8700565 },
            { lng: 30.902395, lat: -29.86815 },
            { lng: 30.898876, lat: -29.866698 },
            { lng: 30.9, lat: -29.86875 },
            { lng: 30.892188, lat: -29.86875 },
            { lng: 30.892188, lat: -29.876562 },
            { lng: 30.892188, lat: -29.884375 },
            { lng: 30.9, lat: -29.884375 },
            { lng: 30.907812, lat: -29.884375 },
        ];
        var F4Map = new google.maps.Polygon({
            paths: F4,
            strokeColor: "#0BA9CC",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#0BA9CC",
            fillOpacity: 0.20,
            draggable: false,
            geodesic: false,
            name: 'F4',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS3.push(F4Map);

        var F5 = [
            { lng: 30.915625, lat: -29.876562 },
            { lng: 30.915625, lat: -29.884375 },
            { lng: 30.939062, lat: -29.884375 },
            { lng: 30.939062, lat: -29.892188 },
            { lng: 30.942324, lat: -29.899554 },
            { lng: 30.946875, lat: -29.9 },
            { lng: 30.946875, lat: -29.892188 },
            { lng: 30.954688, lat: -29.892188 },
            { lng: 30.954688, lat: -29.884375 },
            { lng: 30.9625, lat: -29.884375 },
            { lng: 30.9625, lat: -29.9 },
            { lng: 30.954688, lat: -29.9 },
            { lng: 30.954666, lat: -29.906957 },
            { lng: 30.946598, lat: -29.906957 },
            { lng: 30.946875, lat: -29.915625 },
            { lng: 30.954688, lat: -29.915625 },
            { lng: 30.954688, lat: -29.907812 },
            { lng: 30.978125, lat: -29.907812 },
            { lng: 30.978125, lat: -29.9 },
            { lng: 30.971216, lat: -29.894309 },
            { lng: 30.969668, lat: -29.893453 },
            { lng: 30.962681, lat: -29.881779 },
            { lng: 30.9654382, lat: -29.8787458 },
            { lng: 30.9615545, lat: -29.8771928 },
            { lng: 30.9597943, lat: -29.8794348 },
            { lng: 30.954323, lat: -29.878867 },
            { lng: 30.952434, lat: -29.879053 },
            { lng: 30.948873, lat: -29.878718 },
            { lng: 30.948379, lat: -29.873974 },
            { lng: 30.946148, lat: -29.872076 },
            { lng: 30.936856, lat: -29.872876 },
            { lng: 30.932372, lat: -29.869824 },
            { lng: 30.928616, lat: -29.870085 },
            { lng: 30.9272, lat: -29.868894 },
            { lng: 30.925291, lat: -29.868187 },
            { lng: 30.925162, lat: -29.869378 },
            { lng: 30.922265, lat: -29.874067 },
            { lng: 30.915625, lat: -29.876562 },
        ];
        var F5Map = new google.maps.Polygon({
            paths: F5,
            strokeColor: "#DB4436",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#DB4436",
            fillOpacity: 0.20,
            draggable: false,
            geodesic: false,
            name: 'F5',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS3.push(F5Map);

        var F6 = [
            { lng: 30.923438, lat: -29.9156622 },
            { lng: 30.9234809, lat: -29.9180075 },
            { lng: 30.9259068, lat: -29.9205928 },
            { lng: 30.929609, lat: -29.9236335 },
            { lng: 30.9364871, lat: -29.9265623 },
            { lng: 30.9404782, lat: -29.9277539 },
            { lng: 30.946875, lat: -29.9312872 },
            { lng: 30.946598, lat: -29.9069942 },
            { lng: 30.950782, lat: -29.9070312 },
            { lng: 30.954666, lat: -29.9069942 },
            { lng: 30.954688, lat: -29.9000372 },
            { lng: 30.9625, lat: -29.9000372 },
            { lng: 30.9625, lat: -29.8844122 },
            { lng: 30.954688, lat: -29.8844122 },
            { lng: 30.954688, lat: -29.8922252 },
            { lng: 30.946875, lat: -29.8922252 },
            { lng: 30.946875, lat: -29.9000372 },
            { lng: 30.942324, lat: -29.8995912 },
            { lng: 30.941985, lat: -29.9008002 },
            { lng: 30.942049, lat: -29.9014892 },
            { lng: 30.938873, lat: -29.9027532 },
            { lng: 30.93544, lat: -29.9024562 },
            { lng: 30.923438, lat: -29.9156622 },
        ];
        var F6Map = new google.maps.Polygon({
            paths: F6,
            strokeColor: "#F8971B",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F8971B",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'F6',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS3.push(F6Map);

        var F7 = [
            { lng: 30.954688, lat: -29.946875 },
            { lng: 30.954752, lat: -29.946457 },
            { lng: 30.9625, lat: -29.939062 },
            { lng: 30.965463, lat: -29.935455 },
            { lng: 30.969668, lat: -29.930655 },
            { lng: 30.974493, lat: -29.924217 },
            { lng: 30.97555, lat: -29.921355 },
            { lng: 30.978125, lat: -29.915625 },
            { lng: 30.978125, lat: -29.907812 },
            { lng: 30.970312, lat: -29.907812 },
            { lng: 30.9625, lat: -29.907812 },
            { lng: 30.954688, lat: -29.907812 },
            { lng: 30.954688, lat: -29.915625 },
            { lng: 30.946875, lat: -29.915625 },
            { lng: 30.946875, lat: -29.923438 },
            { lng: 30.946875, lat: -29.93125 },
            { lng: 30.939062, lat: -29.93125 },
            { lng: 30.939062, lat: -29.939062 },
            { lng: 30.946875, lat: -29.939062 },
            { lng: 30.945986, lat: -29.945883 },
            { lng: 30.948186, lat: -29.948018 },
            { lng: 30.950804, lat: -29.943259 },
            { lng: 30.954688, lat: -29.946875 }
        ];
        var F7Map = new google.maps.Polygon({
            paths: F7,
            strokeColor: "#795046",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#795046",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'F7',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS3.push(F7Map);

        var F8 = [
            { lng: 30.884375, lat: -29.9 },
            { lng: 30.892188, lat: -29.9 },
            { lng: 30.9, lat: -29.9 },
            { lng: 30.907812, lat: -29.9 },
            { lng: 30.915882, lat: -29.90026 },
            { lng: 30.918188, lat: -29.898568 },
            { lng: 30.919937, lat: -29.896782 },
            { lng: 30.920441, lat: -29.893918 },
            { lng: 30.923102, lat: -29.895108 },
            { lng: 30.926063, lat: -29.898085 },
            { lng: 30.929754, lat: -29.90054 },
            { lng: 30.935161, lat: -29.90121 },
            { lng: 30.941985, lat: -29.900763 },
            { lng: 30.942324, lat: -29.899554 },
            { lng: 30.939062, lat: -29.892188 },
            { lng: 30.939062, lat: -29.884375 },
            { lng: 30.93125, lat: -29.884375 },
            { lng: 30.923438, lat: -29.884375 },
            { lng: 30.915625, lat: -29.884375 },
            { lng: 30.907812, lat: -29.884375 },
            { lng: 30.9, lat: -29.884375 },
            { lng: 30.892188, lat: -29.884375 },
            { lng: 30.892188, lat: -29.892188 },
            { lng: 30.884375, lat: -29.892188 },
            { lng: 30.876562, lat: -29.892188 },
            { lng: 30.876562, lat: -29.9 },
            { lng: 30.876562, lat: -29.907812 },
            { lng: 30.884375, lat: -29.907812 },
            { lng: 30.884375, lat: -29.9 },
        ];
        var F8Map = new google.maps.Polygon({
            paths: F8,
            strokeColor: "#A61B4A",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#A61B4A",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'F8',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS3.push(F8Map);

        var F9 = [
            { lng: 30.899906, lat: -29.917698 },
            { lng: 30.899026, lat: -29.918024 },
            { lng: 30.898308, lat: -29.918461 },
            { lng: 30.897996, lat: -29.91954 },
            { lng: 30.897996, lat: -29.92073 },
            { lng: 30.897031, lat: -29.921176 },
            { lng: 30.895926, lat: -29.921372 },
            { lng: 30.895389, lat: -29.922329 },
            { lng: 30.895046, lat: -29.922934 },
            { lng: 30.8951, lat: -29.923408 },
            { lng: 30.8957, lat: -29.924087 },
            { lng: 30.896022, lat: -29.924914 },
            { lng: 30.896215, lat: -29.925444 },
            { lng: 30.896516, lat: -29.926551 },
            { lng: 30.896945, lat: -29.927481 },
            { lng: 30.897621, lat: -29.927369 },
            { lng: 30.898007, lat: -29.927769 },
            { lng: 30.897857, lat: -29.929638 },
            { lng: 30.897578, lat: -29.931795 },
            { lng: 30.898962, lat: -29.932511 },
            { lng: 30.9004, lat: -29.933385 },
            { lng: 30.903136, lat: -29.934752 },
            { lng: 30.904659, lat: -29.935124 },
            { lng: 30.90615, lat: -29.935588 },
            { lng: 30.907642, lat: -29.935486 },
            { lng: 30.908876, lat: -29.935198 },
            { lng: 30.908253, lat: -29.934603 },
            { lng: 30.906612, lat: -29.933794 },
            { lng: 30.905979, lat: -29.932808 },
            { lng: 30.906537, lat: -29.931748 },
            { lng: 30.906869, lat: -29.930651 },
            { lng: 30.907191, lat: -29.929378 },
            { lng: 30.908253, lat: -29.926393 },
            { lng: 30.908854, lat: -29.925509 },
            { lng: 30.908972, lat: -29.924022 },
            { lng: 30.908135, lat: -29.923575 },
            { lng: 30.908071, lat: -29.922841 },
            { lng: 30.908478, lat: -29.922106 },
            { lng: 30.908414, lat: -29.921409 },
            { lng: 30.908543, lat: -29.920293 },
            { lng: 30.907749, lat: -29.919605 },
            { lng: 30.907159, lat: -29.919093 },
            { lng: 30.906762, lat: -29.91914 },
            { lng: 30.906075, lat: -29.919605 },
            { lng: 30.905378, lat: -29.919893 },
            { lng: 30.90453, lat: -29.920172 },
            { lng: 30.90393, lat: -29.920032 },
            { lng: 30.903532, lat: -29.919716 },
            { lng: 30.903007, lat: -29.919577 },
            { lng: 30.902985, lat: -29.9194 },
            { lng: 30.903329, lat: -29.91887 },
            { lng: 30.903522, lat: -29.918387 },
            { lng: 30.902792, lat: -29.918024 },
            { lng: 30.901988, lat: -29.918052 },
            { lng: 30.901408, lat: -29.918377 },
            { lng: 30.900593, lat: -29.918359 },
            { lng: 30.899906, lat: -29.917698 },
        ];
        var F9Map = new google.maps.Polygon({
            paths: F9,
            strokeColor: "#F4EB37",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F4EB37",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'F9(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS3.push(F9Map);

        var F10 = [
            { lng: 30.915882, lat: -29.90026 },
            { lng: 30.914476, lat: -29.901191 },
            { lng: 30.914261, lat: -29.901935 },
            { lng: 30.913446, lat: -29.902568 },
            { lng: 30.915632, lat: -29.906369 },
            { lng: 30.916166, lat: -29.90686 },
            { lng: 30.916504, lat: -29.907915 },
            { lng: 30.915077, lat: -29.910324 },
            { lng: 30.917169, lat: -29.910073 },
            { lng: 30.918231, lat: -29.911124 },
            { lng: 30.919068, lat: -29.911105 },
            { lng: 30.919561, lat: -29.912314 },
            { lng: 30.919733, lat: -29.913951 },
            { lng: 30.920291, lat: -29.91529 },
            { lng: 30.922222, lat: -29.916629 },
            { lng: 30.930474, lat: -29.907776 },
            { lng: 30.925989, lat: -29.907488 },
            { lng: 30.925764, lat: -29.90627 },
            { lng: 30.926086, lat: -29.905126 },
            { lng: 30.92555, lat: -29.905107 },
            { lng: 30.924219, lat: -29.905442 },
            { lng: 30.92276, lat: -29.905126 },
            { lng: 30.920722, lat: -29.903415 },
            { lng: 30.918984, lat: -29.902671 },
            { lng: 30.916666, lat: -29.901815 },
            { lng: 30.915882, lat: -29.90026 },
        ];
        var F10Map = new google.maps.Polygon({
            paths: F10,
            strokeColor: "#009D57",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#009D57",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'F10(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS3.push(F10Map);

        var F11 = [
            { lng: 30.912223, lat: -29.864726 },
            { lng: 30.909433, lat: -29.866326 },
            { lng: 30.910335, lat: -29.867554 },
            { lng: 30.907352, lat: -29.868968 },
            { lng: 30.906618, lat: -29.870796 },
            { lng: 30.9057, lat: -29.870829 },
            { lng: 30.904584, lat: -29.872038 },
            { lng: 30.904209, lat: -29.872988 },
            { lng: 30.905384, lat: -29.87403 },
            { lng: 30.90698, lat: -29.874718 },
            { lng: 30.909476, lat: -29.874811 },
            { lng: 30.911063, lat: -29.874974 },
            { lng: 30.912716, lat: -29.87318 },
            { lng: 30.914514, lat: -29.874164 },
            { lng: 30.915625, lat: -29.876562 },
            { lng: 30.922265, lat: -29.874067 },
            { lng: 30.925162, lat: -29.869378 },
            { lng: 30.925291, lat: -29.868187 },
            { lng: 30.92361, lat: -29.867485 },
            { lng: 30.918446, lat: -29.865247 },
            { lng: 30.912223, lat: -29.864726 },
        ];
        var F11Map = new google.maps.Polygon({
            paths: F11,
            strokeColor: "#F4EB37",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F4EB37",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'F11(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS3.push(F11Map);

        var F12 = [
            { lng: 30.8803213, lat: -29.847637 },
            { lng: 30.8808843, lat: -29.851718 },
            { lng: 30.8807553, lat: -29.856184 },
            { lng: 30.8819573, lat: -29.858771 },
            { lng: 30.8865493, lat: -29.860855 },
            { lng: 30.8905352, lat: -29.8620315 },
            { lng: 30.8907846, lat: -29.861557 },
            { lng: 30.89079, lat: -29.8612197 },
            { lng: 30.890916, lat: -29.8608475 },
            { lng: 30.8912996, lat: -29.8602683 },
            { lng: 30.891482, lat: -29.8603916 },
            { lng: 30.8914096, lat: -29.8604846 },
            { lng: 30.8915034, lat: -29.8606382 },
            { lng: 30.8922115, lat: -29.8600694 },
            { lng: 30.8924516, lat: -29.8599275 },
            { lng: 30.8928124, lat: -29.8602741 },
            { lng: 30.8930055, lat: -29.8601346 },
            { lng: 30.8931557, lat: -29.8597717 },
            { lng: 30.8930354, lat: -29.8585682 },
            { lng: 30.8934431, lat: -29.8582332 },
            { lng: 30.894516, lat: -29.8581216 },
            { lng: 30.8944516, lat: -29.8562048 },
            { lng: 30.8939796, lat: -29.8545857 },
            { lng: 30.8953033, lat: -29.853449 },
            { lng: 30.8905833, lat: -29.849652 },
            { lng: 30.8886943, lat: -29.848833 },
            { lng: 30.8863983, lat: -29.84673 },
            { lng: 30.8813883, lat: -29.847316 },
            { lng: 30.8803213, lat: -29.847637 },
        ];
        var F12Map = new google.maps.Polygon({
            paths: F12,
            strokeColor: "#7C3592",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#7C3592",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'F12(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS3.push(F12Map);

        var F13 = [
            { lng: 30.916439, lat: -29.899759 },
            { lng: 30.915882, lat: -29.900279 },
            { lng: 30.916666, lat: -29.901834 },
            { lng: 30.917136, lat: -29.902065 },
            { lng: 30.917974, lat: -29.902345 },
            { lng: 30.918746, lat: -29.902586 },
            { lng: 30.920722, lat: -29.903433 },
            { lng: 30.921316, lat: -29.903902 },
            { lng: 30.921632, lat: -29.904232 },
            { lng: 30.922158, lat: -29.904651 },
            { lng: 30.92276, lat: -29.905144 },
            { lng: 30.923145, lat: -29.905246 },
            { lng: 30.923402, lat: -29.90532 },
            { lng: 30.923681, lat: -29.905376 },
            { lng: 30.924219, lat: -29.905461 },
            { lng: 30.92555, lat: -29.905126 },
            { lng: 30.926086, lat: -29.905144 },
            { lng: 30.925764, lat: -29.906288 },
            { lng: 30.925989, lat: -29.907507 },
            { lng: 30.930474, lat: -29.907795 },
            { lng: 30.93544, lat: -29.902438 },
            { lng: 30.935365, lat: -29.902223 },
            { lng: 30.935236, lat: -29.901843 },
            { lng: 30.935226, lat: -29.901637 },
            { lng: 30.935193, lat: -29.901489 },
            { lng: 30.935161, lat: -29.901229 },
            { lng: 30.930467, lat: -29.90148 },
            { lng: 30.929134, lat: -29.900419 },
            { lng: 30.927114, lat: -29.899006 },
            { lng: 30.926063, lat: -29.898104 },
            { lng: 30.923102, lat: -29.895127 },
            { lng: 30.920441, lat: -29.893937 },
            { lng: 30.919937, lat: -29.896801 },
            { lng: 30.918188, lat: -29.898587 },
            { lng: 30.916439, lat: -29.899759 },
        ];
        var F13Map = new google.maps.Polygon({
            paths: F13,
            strokeColor: "#795046",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#795046",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'F13(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS3.push(F13Map);

        var F14 = [
            { lng: 30.8953033, lat: -29.853449 },
            { lng: 30.8939796, lat: -29.8545857 },
            { lng: 30.8944516, lat: -29.8562048 },
            { lng: 30.894516, lat: -29.8581216 },
            { lng: 30.8934431, lat: -29.8582332 },
            { lng: 30.8930354, lat: -29.8585682 },
            { lng: 30.8931342, lat: -29.8592925 },
            { lng: 30.8931557, lat: -29.8597717 },
            { lng: 30.8930055, lat: -29.8601346 },
            { lng: 30.8928124, lat: -29.8602741 },
            { lng: 30.8924516, lat: -29.8599275 },
            { lng: 30.8922115, lat: -29.8600694 },
            { lng: 30.8919648, lat: -29.8602858 },
            { lng: 30.8917931, lat: -29.8604207 },
            { lng: 30.8916188, lat: -29.860573 },
            { lng: 30.8915034, lat: -29.8606382 },
            { lng: 30.8914096, lat: -29.8604846 },
            { lng: 30.891482, lat: -29.8603916 },
            { lng: 30.8912996, lat: -29.8602683 },
            { lng: 30.890916, lat: -29.8608475 },
            { lng: 30.89079, lat: -29.8612197 },
            { lng: 30.8907846, lat: -29.861557 },
            { lng: 30.8905352, lat: -29.8620315 },
            { lng: 30.8915703, lat: -29.862344 },
            { lng: 30.8936465, lat: -29.8633294 },
            { lng: 30.8947435, lat: -29.8638714 },
            { lng: 30.897503, lat: -29.865172 },
            { lng: 30.898876, lat: -29.866698 },
            { lng: 30.902395, lat: -29.86815 },
            { lng: 30.9040046, lat: -29.8700565 },
            { lng: 30.9055666, lat: -29.8706192 },
            { lng: 30.9057, lat: -29.870829 },
            { lng: 30.906618, lat: -29.870796 },
            { lng: 30.907352, lat: -29.868968 },
            { lng: 30.910335, lat: -29.867554 },
            { lng: 30.909433, lat: -29.866326 },
            { lng: 30.9073193, lat: -29.865582 },
            { lng: 30.9076633, lat: -29.864651 },
            { lng: 30.9077493, lat: -29.863051 },
            { lng: 30.9076203, lat: -29.861562 },
            { lng: 30.9071053, lat: -29.859999 },
            { lng: 30.9051313, lat: -29.859962 },
            { lng: 30.9046593, lat: -29.861078 },
            { lng: 30.9046163, lat: -29.862604 },
            { lng: 30.9018263, lat: -29.86227 },
            { lng: 30.8964623, lat: -29.859701 },
            { lng: 30.898533, lat: -29.857431 },
            { lng: 30.8981793, lat: -29.854416 },
            { lng: 30.8953033, lat: -29.853449 },
        ];
        var F14Map = new google.maps.Polygon({
            paths: F14,
            strokeColor: "#B29189",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#B29189",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'F14(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS3.push(F14Map);
    },

    DrawSubSector4: function (map) {
        var C1 = [
            { lng: 30.8908297, lat: -29.7952375 },
            { lng: 30.888448, lat: -29.795219 },
            { lng: 30.886989, lat: -29.793431 },
            { lng: 30.887761, lat: -29.788887 },
            { lng: 30.887203, lat: -29.785014 },
            { lng: 30.886731, lat: -29.783002 },
            { lng: 30.888405, lat: -29.780507 },
            { lng: 30.886441, lat: -29.779985 },
            { lng: 30.884457, lat: -29.779501 },
            { lng: 30.881023, lat: -29.779576 },
            { lng: 30.87965, lat: -29.77939 },
            { lng: 30.876217, lat: -29.778272 },
            { lng: 30.87347, lat: -29.77641 },
            { lng: 30.871282, lat: -29.775627 },
            { lng: 30.868063, lat: -29.776447 },
            { lng: 30.867806, lat: -29.778235 },
            { lng: 30.864415, lat: -29.781326 },
            { lng: 30.863299, lat: -29.784083 },
            { lng: 30.861454, lat: -29.788515 },
            { lng: 30.857291, lat: -29.78885 },
            { lng: 30.858042, lat: -29.791159 },
            { lng: 30.862184, lat: -29.794027 },
            { lng: 30.866818, lat: -29.798347 },
            { lng: 30.874972, lat: -29.800209 },
            { lng: 30.87611, lat: -29.799241 },
            { lng: 30.877676, lat: -29.798477 },
            { lng: 30.882225, lat: -29.798049 },
            { lng: 30.886173, lat: -29.797993 },
            { lng: 30.888019, lat: -29.7981882 },
            { lng: 30.8909852, lat: -29.7974766 },
            { lng: 30.8908297, lat: -29.7952375 },
        ];
        var C1Map = new google.maps.Polygon({
            paths: C1,
            strokeColor: "#4186F0",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#4186F0",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'C1',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS4.push(C1Map);

        var C2 = [
            { lng: 30.82798, lat: -29.836866 },
            { lng: 30.83004, lat: -29.833273 },
            { lng: 30.832164, lat: -29.83076 },
            { lng: 30.833902, lat: -29.8278 },
            { lng: 30.839181, lat: -29.820801 },
            { lng: 30.844266, lat: -29.814964 },
            { lng: 30.84858, lat: -29.811245 },
            { lng: 30.850511, lat: -29.808625 },
            { lng: 30.852613, lat: -29.807694 },
            { lng: 30.852699, lat: -29.805832 },
            { lng: 30.85124, lat: -29.802667 },
            { lng: 30.849094, lat: -29.803226 },
            { lng: 30.845361, lat: -29.802071 },
            { lng: 30.835619, lat: -29.801252 },
            { lng: 30.831757, lat: -29.802965 },
            { lng: 30.827208, lat: -29.805646 },
            { lng: 30.824461, lat: -29.807806 },
            { lng: 30.81811, lat: -29.811902 },
            { lng: 30.814977, lat: -29.815551 },
            { lng: 30.811801, lat: -29.817599 },
            { lng: 30.80781, lat: -29.814639 },
            { lng: 30.803733, lat: -29.815234 },
            { lng: 30.803904, lat: -29.816929 },
            { lng: 30.797338, lat: -29.822067 },
            { lng: 30.79266, lat: -29.822886 },
            { lng: 30.789184, lat: -29.821955 },
            { lng: 30.783649, lat: -29.821769 },
            { lng: 30.778455, lat: -29.822385 },
            { lng: 30.776073, lat: -29.824517 },
            { lng: 30.77574, lat: -29.826514 },
            { lng: 30.778449, lat: -29.82904 },
            { lng: 30.78637, lat: -29.825685 },
            { lng: 30.792476, lat: -29.8268 },
            { lng: 30.801709, lat: -29.824192 },
            { lng: 30.80697, lat: -29.823781 },
            { lng: 30.820642, lat: -29.832082 },
            { lng: 30.824332, lat: -29.835097 },
            { lng: 30.82798, lat: -29.836866 },
        ];
        var C2Map = new google.maps.Polygon({
            paths: C2,
            strokeColor: "#3F5BA9",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#3F5BA9",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'C2',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS4.push(C2Map);

        var C3 = [
            { lng: 30.86255, lat: -29.844392 },
            { lng: 30.874372, lat: -29.853188 },
            { lng: 30.876562, lat: -29.853125 },
            { lng: 30.880895, lat: -29.851718 },
            { lng: 30.880332, lat: -29.847637 },
            { lng: 30.881399, lat: -29.847316 },
            { lng: 30.886409, lat: -29.84673 },
            { lng: 30.888705, lat: -29.848833 },
            { lng: 30.890594, lat: -29.849652 },
            { lng: 30.897117, lat: -29.842617 },
            { lng: 30.895743, lat: -29.839267 },
            { lng: 30.893555, lat: -29.837442 },
            { lng: 30.898254, lat: -29.833906 },
            { lng: 30.895722, lat: -29.8313 },
            { lng: 30.891924, lat: -29.829848 },
            { lng: 30.889521, lat: -29.83238 },
            { lng: 30.887032, lat: -29.833106 },
            { lng: 30.883276, lat: -29.832044 },
            { lng: 30.878958, lat: -29.827377 },
            { lng: 30.868514, lat: -29.824282 },
            { lng: 30.864651, lat: -29.822178 },
            { lng: 30.865767, lat: -29.823277 },
            { lng: 30.866303, lat: -29.828694 },
            { lng: 30.86684, lat: -29.830016 },
            { lng: 30.868921, lat: -29.83117 },
            { lng: 30.869672, lat: -29.831114 },
            { lng: 30.870702, lat: -29.830518 },
            { lng: 30.87185, lat: -29.831486 },
            { lng: 30.872612, lat: -29.832882 },
            { lng: 30.872784, lat: -29.834073 },
            { lng: 30.874136, lat: -29.834744 },
            { lng: 30.87317, lat: -29.835265 },
            { lng: 30.870509, lat: -29.834706 },
            { lng: 30.869694, lat: -29.834799 },
            { lng: 30.868192, lat: -29.835339 },
            { lng: 30.865123, lat: -29.835693 },
            { lng: 30.86405, lat: -29.836251 },
            { lng: 30.862248, lat: -29.836549 },
            { lng: 30.861368, lat: -29.836177 },
            { lng: 30.861454, lat: -29.832901 },
            { lng: 30.860424, lat: -29.837107 },
            { lng: 30.86255, lat: -29.844392 },
        ];
        var C3Map = new google.maps.Polygon({
            paths: C3,
            strokeColor: "#F4B400",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F4B400",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'C3',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS4.push(C3Map);

        var C4 = [
            { lng: 30.86255, lat: -29.844355 },
            { lng: 30.862572, lat: -29.844344 },
            { lng: 30.860424, lat: -29.83707 },
            { lng: 30.861025, lat: -29.833375 },
            { lng: 30.857506, lat: -29.831319 },
            { lng: 30.861669, lat: -29.830071 },
            { lng: 30.863171, lat: -29.827391 },
            { lng: 30.864265, lat: -29.823724 },
            { lng: 30.861787, lat: -29.822197 },
            { lng: 30.860875, lat: -29.821564 },
            { lng: 30.859544, lat: -29.821415 },
            { lng: 30.860467, lat: -29.820615 },
            { lng: 30.858192, lat: -29.81974 },
            { lng: 30.855424, lat: -29.818064 },
            { lng: 30.850103, lat: -29.813261 },
            { lng: 30.84858, lat: -29.811208 },
            { lng: 30.844266, lat: -29.814927 },
            { lng: 30.83976, lat: -29.820056 },
            { lng: 30.833774, lat: -29.828005 },
            { lng: 30.832164, lat: -29.830723 },
            { lng: 30.83004, lat: -29.833236 },
            { lng: 30.82798, lat: -29.836828 },
            { lng: 30.830534, lat: -29.839025 },
            { lng: 30.836112, lat: -29.84191 },
            { lng: 30.843923, lat: -29.849112 },
            { lng: 30.848815, lat: -29.842245 },
            { lng: 30.856068, lat: -29.84271 },
            { lng: 30.86255, lat: -29.844355 },
        ];
        var C4Map = new google.maps.Polygon({
            paths: C4,
            strokeColor: "#0BA9CC",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#0BA9CC",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'C4',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS4.push(C4Map);

        var C5 = [
            { lng: 30.864737, lat: -29.822178 },
            { lng: 30.866046, lat: -29.818939 },
            { lng: 30.86729, lat: -29.816594 },
            { lng: 30.868664, lat: -29.814918 },
            { lng: 30.871882, lat: -29.813428 },
            { lng: 30.871453, lat: -29.808774 },
            { lng: 30.871968, lat: -29.805907 },
            { lng: 30.872998, lat: -29.801773 },
            { lng: 30.875058, lat: -29.800209 },
            { lng: 30.871024, lat: -29.799204 },
            { lng: 30.869265, lat: -29.798906 },
            { lng: 30.866454, lat: -29.798124 },
            { lng: 30.862184, lat: -29.794027 },
            { lng: 30.859373, lat: -29.792034 },
            { lng: 30.858128, lat: -29.791159 },
            { lng: 30.857377, lat: -29.78885 },
            { lng: 30.854223, lat: -29.790768 },
            { lng: 30.85403, lat: -29.79304 },
            { lng: 30.852528, lat: -29.793506 },
            { lng: 30.851862, lat: -29.794716 },
            { lng: 30.850339, lat: -29.796112 },
            { lng: 30.850983, lat: -29.79844 },
            { lng: 30.85154, lat: -29.800526 },
            { lng: 30.851326, lat: -29.802667 },
            { lng: 30.852785, lat: -29.805832 },
            { lng: 30.852699, lat: -29.807694 },
            { lng: 30.850596, lat: -29.808625 },
            { lng: 30.848665, lat: -29.811245 },
            { lng: 30.850189, lat: -29.813298 },
            { lng: 30.852699, lat: -29.81557 },
            { lng: 30.85551, lat: -29.818101 },
            { lng: 30.85815, lat: -29.819702 },
            { lng: 30.864737, lat: -29.822178 },
        ];
        var C5Map = new google.maps.Polygon({
            paths: C5,
            strokeColor: "#F4EB37",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F4EB37",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'C5',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS4.push(C5Map);

        var C6 = [
            { lng: 30.864651, lat: -29.822178 },
            { lng: 30.860467, lat: -29.820652 },
            { lng: 30.859652, lat: -29.821006 },
            { lng: 30.859544, lat: -29.821452 },
            { lng: 30.860875, lat: -29.821564 },
            { lng: 30.862505, lat: -29.822514 },
            { lng: 30.864265, lat: -29.823761 },
            { lng: 30.86345, lat: -29.826386 },
            { lng: 30.863171, lat: -29.827707 },
            { lng: 30.862226, lat: -29.828899 },
            { lng: 30.861669, lat: -29.830071 },
            { lng: 30.860124, lat: -29.830313 },
            { lng: 30.857506, lat: -29.831356 },
            { lng: 30.861411, lat: -29.833608 },
            { lng: 30.861368, lat: -29.836177 },
            { lng: 30.862248, lat: -29.836549 },
            { lng: 30.86405, lat: -29.836251 },
            { lng: 30.865123, lat: -29.835693 },
            { lng: 30.866518, lat: -29.835525 },
            { lng: 30.868471, lat: -29.835302 },
            { lng: 30.869694, lat: -29.834799 },
            { lng: 30.870509, lat: -29.834706 },
            { lng: 30.87199, lat: -29.835004 },
            { lng: 30.87317, lat: -29.835265 },
            { lng: 30.874136, lat: -29.834744 },
            { lng: 30.872784, lat: -29.834073 },
            { lng: 30.872612, lat: -29.832882 },
            { lng: 30.871625, lat: -29.831207 },
            { lng: 30.870702, lat: -29.830518 },
            { lng: 30.869672, lat: -29.831114 },
            { lng: 30.868921, lat: -29.83117 },
            { lng: 30.86684, lat: -29.830016 },
            { lng: 30.866303, lat: -29.828694 },
            { lng: 30.866132, lat: -29.827056 },
            { lng: 30.865917, lat: -29.825455 },
            { lng: 30.865853, lat: -29.824636 },
            { lng: 30.865767, lat: -29.823277 },
            { lng: 30.864651, lat: -29.822178 },
        ];
        var C6Map = new google.maps.Polygon({
            paths: C6,
            strokeColor: "#7C3592",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#7C3592",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'C6(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS4.push(C6Map);

        var C7 = [
            { lng: 30.8647696, lat: -29.8222649 },
            { lng: 30.8661916, lat: -29.8230609 },
            { lng: 30.8668556, lat: -29.8234049 },
            { lng: 30.8686316, lat: -29.8243679 },
            { lng: 30.8693836, lat: -29.8245959 },
            { lng: 30.8700156, lat: -29.8247779 },
            { lng: 30.8715656, lat: -29.8252389 },
            { lng: 30.8740496, lat: -29.8259829 },
            { lng: 30.8788936, lat: -29.8272839 },
            { lng: 30.8832116, lat: -29.8319509 },
            { lng: 30.8869676, lat: -29.8330129 },
            { lng: 30.8889677, lat: -29.8324223 },
            { lng: 30.8880828, lat: -29.8313317 },
            { lng: 30.8879326, lat: -29.83028 },
            { lng: 30.887203, lat: -29.8297774 },
            { lng: 30.8859477, lat: -29.8297308 },
            { lng: 30.8859799, lat: -29.8279345 },
            { lng: 30.8863554, lat: -29.8272271 },
            { lng: 30.8868919, lat: -29.8267617 },
            { lng: 30.8871815, lat: -29.8262312 },
            { lng: 30.8872781, lat: -29.8257937 },
            { lng: 30.8871386, lat: -29.8251422 },
            { lng: 30.8876536, lat: -29.8237646 },
            { lng: 30.8881686, lat: -29.82302 },
            { lng: 30.8887265, lat: -29.8226477 },
            { lng: 30.8891126, lat: -29.8210799 },
            { lng: 30.8823326, lat: -29.8228299 },
            { lng: 30.8799726, lat: -29.8215639 },
            { lng: 30.8793496, lat: -29.8211169 },
            { lng: 30.8805076, lat: -29.8195909 },
            { lng: 30.8811416, lat: -29.8173499 },
            { lng: 30.8808836, lat: -29.8149669 },
            { lng: 30.8816456, lat: -29.8121369 },
            { lng: 30.8832016, lat: -29.8109449 },
            { lng: 30.8855196, lat: -29.8095299 },
            { lng: 30.8868496, lat: -29.8084509 },
            { lng: 30.8897236, lat: -29.8077429 },
            { lng: 30.8911416, lat: -29.8088969 },
            { lng: 30.8921166, lat: -29.8094259 },
            { lng: 30.8926966, lat: -29.8078429 },
            { lng: 30.8929756, lat: -29.8038769 },
            { lng: 30.8931036, lat: -29.8024809 },
            { lng: 30.8928887, lat: -29.7992711 },
            { lng: 30.8927818, lat: -29.7970457 },
            { lng: 30.8895206, lat: -29.7978999 },
            { lng: 30.8879546, lat: -29.7981789 },
            { lng: 30.8862916, lat: -29.7980789 },
            { lng: 30.8836736, lat: -29.7980609 },
            { lng: 30.8797036, lat: -29.7983399 },
            { lng: 30.8777936, lat: -29.7985629 },
            { lng: 30.8762276, lat: -29.7993269 },
            { lng: 30.8750896, lat: -29.8002949 },
            { lng: 30.8730296, lat: -29.8018589 },
            { lng: 30.8715276, lat: -29.8081899 },
            { lng: 30.8716996, lat: -29.8096979 },
            { lng: 30.8716136, lat: -29.8104989 },
            { lng: 30.8719156, lat: -29.8135149 },
            { lng: 30.8686956, lat: -29.8150039 },
            { lng: 30.8682016, lat: -29.8157119 },
            { lng: 30.8670876, lat: -29.8171449 },
            { lng: 30.8660136, lat: -29.8192119 },
            { lng: 30.8647696, lat: -29.8222649 },
        ];
        var C7Map = new google.maps.Polygon({
            paths: C7,
            strokeColor: "#D698AD",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#D698AD",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'C7',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS4.push(C7Map);

        var NoGoZoneC = [
            { lng: 30.88834, lat: -29.78019 },
            { lng: 30.888405, lat: -29.780507 },
            { lng: 30.886731, lat: -29.783002 },
            { lng: 30.887761, lat: -29.788887 },
            { lng: 30.886989, lat: -29.793431 },
            { lng: 30.888448, lat: -29.795219 },
            { lng: 30.8908297, lat: -29.7952375 },
            { lng: 30.8906338, lat: -29.795966 },
            { lng: 30.8909852, lat: -29.7974766 },
            { lng: 30.892868, lat: -29.7970808 },
            { lng: 30.893168, lat: -29.802574 },
            { lng: 30.89304, lat: -29.80397 },
            { lng: 30.905228, lat: -29.806875 },
            { lng: 30.92351, lat: -29.799148 },
            { lng: 30.919647, lat: -29.79425 },
            { lng: 30.919561, lat: -29.788738 },
            { lng: 30.925055, lat: -29.784343 },
            { lng: 30.929861, lat: -29.782034 },
            { lng: 30.926685, lat: -29.779948 },
            { lng: 30.925655, lat: -29.774063 },
            { lng: 30.921879, lat: -29.771008 },
            { lng: 30.91218, lat: -29.76624 },
            { lng: 30.899734, lat: -29.765272 },
            { lng: 30.890808, lat: -29.771902 },
            { lng: 30.888276, lat: -29.77207 },
            { lng: 30.888298, lat: -29.773243 },
            { lng: 30.888378, lat: -29.773667 },
            { lng: 30.88833, lat: -29.775302 },
            { lng: 30.888249, lat: -29.775697 },
            { lng: 30.888319, lat: -29.776242 },
            { lng: 30.888319, lat: -29.778235 },
            { lng: 30.88834, lat: -29.778887 },
            { lng: 30.888426, lat: -29.779296 },
            { lng: 30.888448, lat: -29.779818 },
            { lng: 30.88834, lat: -29.78019 },
        ];
        var NoGoZoneCMap = new google.maps.Polygon({
            paths: NoGoZoneC,
            strokeColor: "#000000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#000000",
            fillOpacity: 0.45,
            draggable: false,
            geodesic: false,
            name: 'NoCoverage',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS4.push(NoGoZoneCMap);

        var C8 = [
            { lng: 30.883083, lat: -29.810859 },
            { lng: 30.881538, lat: -29.812162 },
            { lng: 30.880766, lat: -29.814881 },
            { lng: 30.881023, lat: -29.817264 },
            { lng: 30.880551, lat: -29.819684 },
            { lng: 30.879393, lat: -29.82121 },
            { lng: 30.882397, lat: -29.822923 },
            { lng: 30.885873, lat: -29.822067 },
            { lng: 30.889177, lat: -29.821173 },
            { lng: 30.892654, lat: -29.820726 },
            { lng: 30.895271, lat: -29.818641 },
            { lng: 30.89879, lat: -29.81771 },
            { lng: 30.901451, lat: -29.817375 },
            { lng: 30.90364, lat: -29.817785 },
            { lng: 30.906043, lat: -29.818343 },
            { lng: 30.907803, lat: -29.818902 },
            { lng: 30.911236, lat: -29.819312 },
            { lng: 30.910678, lat: -29.820317 },
            { lng: 30.909176, lat: -29.82121 },
            { lng: 30.909004, lat: -29.822216 },
            { lng: 30.909605, lat: -29.823295 },
            { lng: 30.910442, lat: -29.823193 },
            { lng: 30.910925, lat: -29.821904 },
            { lng: 30.911762, lat: -29.820959 },
            { lng: 30.912185, lat: -29.820117 },
            { lng: 30.912309, lat: -29.819716 },
            { lng: 30.912314, lat: -29.819344 },
            { lng: 30.912842, lat: -29.818686 },
            { lng: 30.913343, lat: -29.818044 },
            { lng: 30.913518, lat: -29.817745 },
            { lng: 30.913712, lat: -29.817453 },
            { lng: 30.913877, lat: -29.817468 },
            { lng: 30.914288, lat: -29.816654 },
            { lng: 30.91453, lat: -29.816002 },
            { lng: 30.914291, lat: -29.81448 },
            { lng: 30.914524, lat: -29.813219 },
            { lng: 30.913848, lat: -29.811348 },
            { lng: 30.91247, lat: -29.808802 },
            { lng: 30.907502, lat: -29.807843 },
            { lng: 30.902395, lat: -29.807768 },
            { lng: 30.899949, lat: -29.807247 },
            { lng: 30.898147, lat: -29.807247 },
            { lng: 30.896602, lat: -29.807582 },
            { lng: 30.895572, lat: -29.808848 },
            { lng: 30.893898, lat: -29.809779 },
            { lng: 30.892181, lat: -29.809519 },
            { lng: 30.891023, lat: -29.808811 },
            { lng: 30.889606, lat: -29.807657 },
            { lng: 30.886731, lat: -29.808364 },
            { lng: 30.885401, lat: -29.809444 },
            { lng: 30.883083, lat: -29.810859 },
        ];
        var C8Map = new google.maps.Polygon({
            paths: C8,
            strokeColor: "#F4B400",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F4B400",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'C8(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS4.push(C8Map);

        var C9 = [
            { lng: 30.915999, lat: -29.834483 },
            { lng: 30.915688, lat: -29.832929 },
            { lng: 30.915838, lat: -29.831951 },
            { lng: 30.915731, lat: -29.830909 },
            { lng: 30.91719, lat: -29.828266 },
            { lng: 30.923896, lat: -29.826292 },
            { lng: 30.924368, lat: -29.825957 },
            { lng: 30.924915, lat: -29.825446 },
            { lng: 30.925376, lat: -29.82417 },
            { lng: 30.925848, lat: -29.823668 },
            { lng: 30.926439, lat: -29.823686 },
            { lng: 30.927372, lat: -29.824561 },
            { lng: 30.928016, lat: -29.824822 },
            { lng: 30.928799, lat: -29.824468 },
            { lng: 30.92896, lat: -29.823426 },
            { lng: 30.92852, lat: -29.82242 },
            { lng: 30.93559, lat: -29.82067 },
            { lng: 30.9378, lat: -29.821062 },
            { lng: 30.94207, lat: -29.81947 },
            { lng: 30.944838, lat: -29.822318 },
            { lng: 30.949774, lat: -29.824757 },
            { lng: 30.951823, lat: -29.824263 },
            { lng: 30.951823, lat: -29.8235 },
            { lng: 30.955138, lat: -29.820466 },
            { lng: 30.954494, lat: -29.819088 },
            { lng: 30.948808, lat: -29.815663 },
            { lng: 30.947306, lat: -29.814583 },
            { lng: 30.941212, lat: -29.813186 },
            { lng: 30.938144, lat: -29.814173 },
            { lng: 30.93499, lat: -29.81328 },
            { lng: 30.931428, lat: -29.813391 },
            { lng: 30.929604, lat: -29.809165 },
            { lng: 30.928273, lat: -29.805702 },
            { lng: 30.927522, lat: -29.804305 },
            { lng: 30.926278, lat: -29.800321 },
            { lng: 30.92351, lat: -29.799148 },
            { lng: 30.921235, lat: -29.799706 },
            { lng: 30.914218, lat: -29.80317 },
            { lng: 30.909691, lat: -29.805218 },
            { lng: 30.905228, lat: -29.806875 },
            { lng: 30.901794, lat: -29.806074 },
            { lng: 30.89304, lat: -29.80397 },
            { lng: 30.892761, lat: -29.807936 },
            { lng: 30.892181, lat: -29.809519 },
            { lng: 30.893898, lat: -29.809779 },
            { lng: 30.895572, lat: -29.808848 },
            { lng: 30.896602, lat: -29.807582 },
            { lng: 30.898147, lat: -29.807247 },
            { lng: 30.899949, lat: -29.807247 },
            { lng: 30.902685, lat: -29.807796 },
            { lng: 30.905363, lat: -29.807829 },
            { lng: 30.907502, lat: -29.807843 },
            { lng: 30.91247, lat: -29.808802 },
            { lng: 30.913848, lat: -29.811348 },
            { lng: 30.914524, lat: -29.813219 },
            { lng: 30.914291, lat: -29.81448 },
            { lng: 30.91453, lat: -29.816002 },
            { lng: 30.914289, lat: -29.816716 },
            { lng: 30.913877, lat: -29.817468 },
            { lng: 30.913712, lat: -29.817453 },
            { lng: 30.913343, lat: -29.818044 },
            { lng: 30.912314, lat: -29.819344 },
            { lng: 30.912309, lat: -29.819716 },
            { lng: 30.912185, lat: -29.820117 },
            { lng: 30.911762, lat: -29.820959 },
            { lng: 30.910925, lat: -29.821904 },
            { lng: 30.910442, lat: -29.823193 },
            { lng: 30.909605, lat: -29.823295 },
            { lng: 30.909004, lat: -29.822216 },
            { lng: 30.909176, lat: -29.82121 },
            { lng: 30.908511, lat: -29.821852 },
            { lng: 30.906547, lat: -29.822085 },
            { lng: 30.905152, lat: -29.8233601 },
            { lng: 30.904259, lat: -29.824354 },
            { lng: 30.902608, lat: -29.824917 },
            { lng: 30.901364, lat: -29.823586 },
            { lng: 30.900947, lat: -29.823004 },
            { lng: 30.90062, lat: -29.824021 },
            { lng: 30.90011, lat: -29.824757 },
            { lng: 30.900324, lat: -29.825948 },
            { lng: 30.901354, lat: -29.82753 },
            { lng: 30.901762, lat: -29.828871 },
            { lng: 30.902728, lat: -29.829317 },
            { lng: 30.904509, lat: -29.829057 },
            { lng: 30.904659, lat: -29.829299 },
            { lng: 30.905904, lat: -29.829746 },
            { lng: 30.906848, lat: -29.831886 },
            { lng: 30.906848, lat: -29.832873 },
            { lng: 30.907706, lat: -29.833059 },
            { lng: 30.909584, lat: -29.831598 },
            { lng: 30.910903, lat: -29.831756 },
            { lng: 30.91277, lat: -29.831663 },
            { lng: 30.912899, lat: -29.832817 },
            { lng: 30.912083, lat: -29.834232 },
            { lng: 30.911751, lat: -29.834353 },
            { lng: 30.915999, lat: -29.834483 },
        ];
        var C9Map = new google.maps.Polygon({
            paths: C9,
            strokeColor: "#DB4436",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#DB4436",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'C9',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS4.push(C9Map);

        var C10 = [
            { lng: 30.936856, lat: -29.872876 },
            { lng: 30.932479, lat: -29.852518 },
            { lng: 30.93264, lat: -29.843929 },
            { lng: 30.930247, lat: -29.843585 },
            { lng: 30.928788, lat: -29.84284 },
            { lng: 30.929217, lat: -29.84124 },
            { lng: 30.92823, lat: -29.839192 },
            { lng: 30.92514, lat: -29.838112 },
            { lng: 30.922823, lat: -29.83264 },
            { lng: 30.921374, lat: -29.833487 },
            { lng: 30.91962, lat: -29.834311 },
            { lng: 30.918102, lat: -29.834613 },
            { lng: 30.915012, lat: -29.834427 },
            { lng: 30.911751, lat: -29.834353 },
            { lng: 30.908189, lat: -29.835916 },
            { lng: 30.9048841, lat: -29.8361205 },
            { lng: 30.9003029, lat: -29.8357618 },
            { lng: 30.8992944, lat: -29.8352779 },
            { lng: 30.8987043, lat: -29.83431 },
            { lng: 30.898254, lat: -29.833906 },
            { lng: 30.8968591, lat: -29.8350602 },
            { lng: 30.893555, lat: -29.837442 },
            { lng: 30.895743, lat: -29.839267 },
            { lng: 30.897117, lat: -29.842617 },
            { lng: 30.890594, lat: -29.849652 },
            { lng: 30.895314, lat: -29.853449 },
            { lng: 30.89819, lat: -29.854416 },
            { lng: 30.898533, lat: -29.857431 },
            { lng: 30.896473, lat: -29.859701 },
            { lng: 30.901837, lat: -29.86227 },
            { lng: 30.904627, lat: -29.862604 },
            { lng: 30.90467, lat: -29.861078 },
            { lng: 30.905142, lat: -29.859962 },
            { lng: 30.906858, lat: -29.859962 },
            { lng: 30.907631, lat: -29.861562 },
            { lng: 30.90776, lat: -29.863051 },
            { lng: 30.907674, lat: -29.864651 },
            { lng: 30.90733, lat: -29.865582 },
            { lng: 30.909433, lat: -29.866326 },
            { lng: 30.912223, lat: -29.864726 },
            { lng: 30.918446, lat: -29.865247 },
            { lng: 30.9272, lat: -29.868894 },
            { lng: 30.928616, lat: -29.870085 },
            { lng: 30.932372, lat: -29.869824 },
            { lng: 30.934453, lat: -29.871015 },
            { lng: 30.936856, lat: -29.872876 },
        ];
        var C10Map = new google.maps.Polygon({
            paths: C10,
            strokeColor: "#009D57",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#009D57",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'B9',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS4.push(C10Map);

        var C11 = [
            { lng: 30.932479, lat: -29.8525 },
            { lng: 30.952091, lat: -29.851494 },
            { lng: 30.953808, lat: -29.849336 },
            { lng: 30.955181, lat: -29.847996 },
            { lng: 30.955868, lat: -29.84714 },
            { lng: 30.956554, lat: -29.842561 },
            { lng: 30.958432, lat: -29.838429 },
            { lng: 30.961704, lat: -29.830053 },
            { lng: 30.965781, lat: -29.826776 },
            { lng: 30.967455, lat: -29.822123 },
            { lng: 30.963335, lat: -29.821452 },
            { lng: 30.96179, lat: -29.820708 },
            { lng: 30.959129, lat: -29.820447 },
            { lng: 30.955138, lat: -29.820447 },
            { lng: 30.951823, lat: -29.823482 },
            { lng: 30.951823, lat: -29.824245 },
            { lng: 30.949774, lat: -29.824738 },
            { lng: 30.947242, lat: -29.823705 },
            { lng: 30.944838, lat: -29.8223 },
            { lng: 30.94207, lat: -29.819451 },
            { lng: 30.9378, lat: -29.821043 },
            { lng: 30.93559, lat: -29.820652 },
            { lng: 30.92852, lat: -29.822402 },
            { lng: 30.92896, lat: -29.823407 },
            { lng: 30.928799, lat: -29.82445 },
            { lng: 30.928016, lat: -29.824803 },
            { lng: 30.927243, lat: -29.824394 },
            { lng: 30.926439, lat: -29.823668 },
            { lng: 30.925848, lat: -29.823649 },
            { lng: 30.925301, lat: -29.824301 },
            { lng: 30.924915, lat: -29.825427 },
            { lng: 30.924068, lat: -29.826283 },
            { lng: 30.91719, lat: -29.828247 },
            { lng: 30.915731, lat: -29.83089 },
            { lng: 30.915838, lat: -29.831933 },
            { lng: 30.915688, lat: -29.83291 },
            { lng: 30.915999, lat: -29.834464 },
            { lng: 30.918102, lat: -29.834595 },
            { lng: 30.918671, lat: -29.834464 },
            { lng: 30.919132, lat: -29.834492 },
            { lng: 30.91962, lat: -29.834292 },
            { lng: 30.921675, lat: -29.833292 },
            { lng: 30.922823, lat: -29.832622 },
            { lng: 30.923316, lat: -29.833711 },
            { lng: 30.924539, lat: -29.833794 },
            { lng: 30.928123, lat: -29.83385 },
            { lng: 30.929925, lat: -29.835004 },
            { lng: 30.931428, lat: -29.832547 },
            { lng: 30.932136, lat: -29.832677 },
            { lng: 30.931546, lat: -29.834651 },
            { lng: 30.930848, lat: -29.835227 },
            { lng: 30.931025, lat: -29.835525 },
            { lng: 30.93139, lat: -29.835805 },
            { lng: 30.93235, lat: -29.83707 },
            { lng: 30.933552, lat: -29.83761 },
            { lng: 30.934453, lat: -29.837852 },
            { lng: 30.935333, lat: -29.837815 },
            { lng: 30.935397, lat: -29.8372 },
            { lng: 30.935236, lat: -29.836028 },
            { lng: 30.935376, lat: -29.835656 },
            { lng: 30.935376, lat: -29.835097 },
            { lng: 30.935612, lat: -29.834297 },
            { lng: 30.935891, lat: -29.83372 },
            { lng: 30.936363, lat: -29.833869 },
            { lng: 30.936883, lat: -29.836405 },
            { lng: 30.937205, lat: -29.83713 },
            { lng: 30.937929, lat: -29.837331 },
            { lng: 30.940462, lat: -29.838857 },
            { lng: 30.940869, lat: -29.838503 },
            { lng: 30.941169, lat: -29.837871 },
            { lng: 30.941834, lat: -29.838262 },
            { lng: 30.940743, lat: -29.840488 },
            { lng: 30.941463, lat: -29.840834 },
            { lng: 30.941867, lat: -29.840151 },
            { lng: 30.942215, lat: -29.840374 },
            { lng: 30.941703, lat: -29.841495 },
            { lng: 30.94089, lat: -29.840979 },
            { lng: 30.940869, lat: -29.844162 },
            { lng: 30.935655, lat: -29.845762 },
            { lng: 30.933015, lat: -29.843901 },
            { lng: 30.932629, lat: -29.843417 },
            { lng: 30.932779, lat: -29.842394 },
            { lng: 30.931878, lat: -29.841388 },
            { lng: 30.929947, lat: -29.841016 },
            { lng: 30.929432, lat: -29.83975 },
            { lng: 30.92823, lat: -29.838112 },
            { lng: 30.928102, lat: -29.83681 },
            { lng: 30.925913, lat: -29.835469 },
            { lng: 30.924132, lat: -29.835656 },
            { lng: 30.92514, lat: -29.838112 },
            { lng: 30.92823, lat: -29.839192 },
            { lng: 30.929217, lat: -29.84124 },
            { lng: 30.928788, lat: -29.84284 },
            { lng: 30.930247, lat: -29.843585 },
            { lng: 30.932737, lat: -29.844161 },
            { lng: 30.932479, lat: -29.8525 },
        ];
        var C11Map = new google.maps.Polygon({
            paths: C11,
            strokeColor: "#4186F0",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#4186F0",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'C11',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS4.push(C11Map);

        var C12 = [
            { lng: 30.923316, lat: -29.833711 },
            { lng: 30.923896, lat: -29.835079 },
            { lng: 30.924132, lat: -29.835656 },
            { lng: 30.925913, lat: -29.835469 },
            { lng: 30.928102, lat: -29.83681 },
            { lng: 30.92823, lat: -29.838112 },
            { lng: 30.929432, lat: -29.83975 },
            { lng: 30.929947, lat: -29.841016 },
            { lng: 30.931878, lat: -29.841388 },
            { lng: 30.932779, lat: -29.842394 },
            { lng: 30.932629, lat: -29.843417 },
            { lng: 30.933015, lat: -29.843901 },
            { lng: 30.934024, lat: -29.844515 },
            { lng: 30.935655, lat: -29.845762 },
            { lng: 30.936985, lat: -29.845371 },
            { lng: 30.939345, lat: -29.844627 },
            { lng: 30.940869, lat: -29.844162 },
            { lng: 30.940869, lat: -29.842412 },
            { lng: 30.94089, lat: -29.840979 },
            { lng: 30.941703, lat: -29.841495 },
            { lng: 30.942215, lat: -29.840374 },
            { lng: 30.941867, lat: -29.840151 },
            { lng: 30.941463, lat: -29.840834 },
            { lng: 30.940743, lat: -29.840488 },
            { lng: 30.941255, lat: -29.839174 },
            { lng: 30.941834, lat: -29.838262 },
            { lng: 30.941169, lat: -29.837871 },
            { lng: 30.940869, lat: -29.838503 },
            { lng: 30.940462, lat: -29.838857 },
            { lng: 30.937929, lat: -29.837331 },
            { lng: 30.937205, lat: -29.83713 },
            { lng: 30.936883, lat: -29.836405 },
            { lng: 30.93647, lat: -29.834502 },
            { lng: 30.936363, lat: -29.833869 },
            { lng: 30.935891, lat: -29.83372 },
            { lng: 30.935612, lat: -29.834297 },
            { lng: 30.935376, lat: -29.835097 },
            { lng: 30.935376, lat: -29.835656 },
            { lng: 30.935236, lat: -29.836028 },
            { lng: 30.93529, lat: -29.836382 },
            { lng: 30.935397, lat: -29.8372 },
            { lng: 30.935333, lat: -29.837815 },
            { lng: 30.934453, lat: -29.837852 },
            { lng: 30.933552, lat: -29.83761 },
            { lng: 30.93235, lat: -29.83707 },
            { lng: 30.93139, lat: -29.835805 },
            { lng: 30.931025, lat: -29.835525 },
            { lng: 30.930848, lat: -29.835227 },
            { lng: 30.931546, lat: -29.834651 },
            { lng: 30.9319, lat: -29.833664 },
            { lng: 30.932136, lat: -29.832677 },
            { lng: 30.931428, lat: -29.832547 },
            { lng: 30.931041, lat: -29.833292 },
            { lng: 30.930355, lat: -29.834427 },
            { lng: 30.929925, lat: -29.835004 },
            { lng: 30.928123, lat: -29.83385 },
            { lng: 30.926535, lat: -29.833831 },
            { lng: 30.925012, lat: -29.833831 },
            { lng: 30.923316, lat: -29.833711 },
        ];
        var C12Map = new google.maps.Polygon({
            paths: C12,
            strokeColor: "#DB4436",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#DB4436",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'C12',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS4.push(C12Map);

        var C13 = [
            { lng: 30.978742, lat: -29.802909 },
            { lng: 30.975566, lat: -29.800321 },
            { lng: 30.97209, lat: -29.798235 },
            { lng: 30.96488, lat: -29.797975 },
            { lng: 30.956512, lat: -29.798198 },
            { lng: 30.951619, lat: -29.797342 },
            { lng: 30.948916, lat: -29.796932 },
            { lng: 30.945997, lat: -29.792016 },
            { lng: 30.940633, lat: -29.786355 },
            { lng: 30.935183, lat: -29.784604 },
            { lng: 30.929861, lat: -29.782034 },
            { lng: 30.925055, lat: -29.784343 },
            { lng: 30.919561, lat: -29.788738 },
            { lng: 30.919647, lat: -29.79425 },
            { lng: 30.92351, lat: -29.799148 },
            { lng: 30.926278, lat: -29.800321 },
            { lng: 30.927522, lat: -29.804305 },
            { lng: 30.928273, lat: -29.805702 },
            { lng: 30.931428, lat: -29.813391 },
            { lng: 30.93499, lat: -29.81328 },
            { lng: 30.938144, lat: -29.814173 },
            { lng: 30.94162, lat: -29.813317 },
            { lng: 30.947306, lat: -29.814583 },
            { lng: 30.954494, lat: -29.819088 },
            { lng: 30.959215, lat: -29.814397 },
            { lng: 30.960674, lat: -29.810971 },
            { lng: 30.958185, lat: -29.809332 },
            { lng: 30.958014, lat: -29.806167 },
            { lng: 30.968742, lat: -29.802965 },
            { lng: 30.972734, lat: -29.80263 },
            { lng: 30.978742, lat: -29.802909 },
        ];
        var C13Map = new google.maps.Polygon({
            paths: C13,
            strokeColor: "#4186F0",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#4186F0",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'C13',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS4.push(C13Map);
    },

    DrawSubSector5: function (map) {
        var A1 = [
            { lng: 30.708675, lat: -29.836028 },
            { lng: 30.716958, lat: -29.830258 },
            { lng: 30.723438, lat: -29.826795 },
            { lng: 30.731978, lat: -29.827577 },
            { lng: 30.727558, lat: -29.823221 },
            { lng: 30.724812, lat: -29.81704 },
            { lng: 30.721378, lat: -29.812796 },
            { lng: 30.717173, lat: -29.81071 },
            { lng: 30.72361, lat: -29.802071 },
            { lng: 30.727301, lat: -29.794623 },
            { lng: 30.73245, lat: -29.795591 },
            { lng: 30.741377, lat: -29.801177 },
            { lng: 30.741098, lat: -29.799352 },
            { lng: 30.740476, lat: -29.795852 },
            { lng: 30.740132, lat: -29.794474 },
            { lng: 30.739918, lat: -29.793506 },
            { lng: 30.736656, lat: -29.790601 },
            { lng: 30.733137, lat: -29.787174 },
            { lng: 30.729103, lat: -29.787081 },
            { lng: 30.725176, lat: -29.787006 },
            { lng: 30.721528, lat: -29.786895 },
            { lng: 30.717945, lat: -29.786839 },
            { lng: 30.715799, lat: -29.784716 },
            { lng: 30.710735, lat: -29.788142 },
            { lng: 30.704556, lat: -29.791271 },
            { lng: 30.700693, lat: -29.792463 },
            { lng: 30.699019, lat: -29.795889 },
            { lng: 30.687861, lat: -29.80155 },
            { lng: 30.688248, lat: -29.809221 },
            { lng: 30.692968, lat: -29.817338 },
            { lng: 30.697088, lat: -29.826311 },
            { lng: 30.702238, lat: -29.832268 },
            { lng: 30.708675, lat: -29.836028 },
        ];
        var A1Map = new google.maps.Polygon({
            paths: A1,
            strokeColor: "#DB4436",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#DB4436",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'A1(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS5.push(A1Map);

        var A2 = [
            { lng: 30.714812, lat: -29.75782 },
            { lng: 30.706058, lat: -29.77857 },
            { lng: 30.702238, lat: -29.782295 },
            { lng: 30.715799, lat: -29.784716 },
            { lng: 30.717945, lat: -29.786839 },
            { lng: 30.733137, lat: -29.787174 },
            { lng: 30.739918, lat: -29.793506 },
            { lng: 30.741634, lat: -29.801438 },
            { lng: 30.743115, lat: -29.798086 },
            { lng: 30.742192, lat: -29.79641 },
            { lng: 30.742149, lat: -29.795666 },
            { lng: 30.742106, lat: -29.794474 },
            { lng: 30.740615, lat: -29.789818 },
            { lng: 30.743823, lat: -29.78628 },
            { lng: 30.74657, lat: -29.782854 },
            { lng: 30.749016, lat: -29.78114 },
            { lng: 30.749531, lat: -29.780619 },
            { lng: 30.750389, lat: -29.778235 },
            { lng: 30.748973, lat: -29.776596 },
            { lng: 30.744424, lat: -29.773541 },
            { lng: 30.741849, lat: -29.769481 },
            { lng: 30.739016, lat: -29.766538 },
            { lng: 30.732365, lat: -29.764824 },
            { lng: 30.727258, lat: -29.759944 },
            { lng: 30.72494, lat: -29.751226 },
            { lng: 30.717602, lat: -29.753946 },
            { lng: 30.714426, lat: -29.756256 },
            { lng: 30.714812, lat: -29.75782 },
        ];
        var A2Map = new google.maps.Polygon({
            paths: A2,
            strokeColor: "#009D57",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#009D57",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'A2',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS5.push(A2Map);

        var A3 = [
            { lng: 30.783402, lat: -29.792304 },
            { lng: 30.782495, lat: -29.7919178 },
            { lng: 30.781116, lat: -29.790675 },
            { lng: 30.781288, lat: -29.787902 },
            { lng: 30.776331, lat: -29.787434 },
            { lng: 30.7749583, lat: -29.7871926 },
            { lng: 30.770296, lat: -29.783584 },
            { lng: 30.768725, lat: -29.783281 },
            { lng: 30.767126, lat: -29.783319 },
            { lng: 30.765871, lat: -29.783203 },
            { lng: 30.764712, lat: -29.782321 },
            { lng: 30.764723, lat: -29.781597 },
            { lng: 30.765056, lat: -29.780321 },
            { lng: 30.765839, lat: -29.779474 },
            { lng: 30.767051, lat: -29.778831 },
            { lng: 30.76785, lat: -29.777988 },
            { lng: 30.76733, lat: -29.777857 },
            { lng: 30.768306, lat: -29.776117 },
            { lng: 30.769251, lat: -29.775471 },
            { lng: 30.7698405, lat: -29.7748828 },
            { lng: 30.7697335, lat: -29.7737272 },
            { lng: 30.769664, lat: -29.772948 },
            { lng: 30.7690629, lat: -29.7705423 },
            { lng: 30.7688, lat: -29.770561 },
            { lng: 30.767856, lat: -29.769108 },
            { lng: 30.766311, lat: -29.768736 },
            { lng: 30.764337, lat: -29.769481 },
            { lng: 30.76144, lat: -29.769965 },
            { lng: 30.759401, lat: -29.769816 },
            { lng: 30.756998, lat: -29.771493 },
            { lng: 30.753736, lat: -29.773132 },
            { lng: 30.751033, lat: -29.776782 },
            { lng: 30.749531, lat: -29.780619 },
            { lng: 30.74657, lat: -29.782854 },
            { lng: 30.743973, lat: -29.785908 },
            { lng: 30.740615, lat: -29.789818 },
            { lng: 30.741892, lat: -29.793729 },
            { lng: 30.742192, lat: -29.79641 },
            { lng: 30.743523, lat: -29.798943 },
            { lng: 30.746656, lat: -29.80397 },
            { lng: 30.752664, lat: -29.806949 },
            { lng: 30.757685, lat: -29.809519 },
            { lng: 30.764809, lat: -29.811604 },
            { lng: 30.770988, lat: -29.813093 },
            { lng: 30.77395, lat: -29.816296 },
            { lng: 30.775409, lat: -29.816072 },
            { lng: 30.774887, lat: -29.812772 },
            { lng: 30.775996, lat: -29.812395 },
            { lng: 30.777522, lat: -29.808923 },
            { lng: 30.779389, lat: -29.807526 },
            { lng: 30.781031, lat: -29.804566 },
            { lng: 30.7790564, lat: -29.8014751 },
            { lng: 30.777576, lat: -29.799036 },
            { lng: 30.777608, lat: -29.797146 },
            { lng: 30.77822, lat: -29.796242 },
            { lng: 30.780816, lat: -29.794362 },
            { lng: 30.783402, lat: -29.792304 },
        ];
        var A3Map = new google.maps.Polygon({
            paths: A3,
            strokeColor: "#F8971B",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F8971B",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'A3',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS5.push(A3Map);

        var A4 = [
            { lng: 30.750346, lat: -29.778198 },
            { lng: 30.75099, lat: -29.776745 },
            { lng: 30.751762, lat: -29.775776 },
            { lng: 30.753694, lat: -29.773094 },
            { lng: 30.756955, lat: -29.771455 },
            { lng: 30.759401, lat: -29.769816 },
            { lng: 30.76144, lat: -29.769965 },
            { lng: 30.764337, lat: -29.769481 },
            { lng: 30.766311, lat: -29.768736 },
            { lng: 30.767856, lat: -29.769108 },
            { lng: 30.768843, lat: -29.770785 },
            { lng: 30.770988, lat: -29.768103 },
            { lng: 30.773199, lat: -29.765961 },
            { lng: 30.77425, lat: -29.764862 },
            { lng: 30.77837, lat: -29.762328 },
            { lng: 30.781975, lat: -29.761062 },
            { lng: 30.786052, lat: -29.759981 },
            { lng: 30.778241, lat: -29.752418 },
            { lng: 30.777855, lat: -29.752344 },
            { lng: 30.77558, lat: -29.748432 },
            { lng: 30.773435, lat: -29.750108 },
            { lng: 30.769615, lat: -29.751002 },
            { lng: 30.768414, lat: -29.752418 },
            { lng: 30.766611, lat: -29.752977 },
            { lng: 30.764208, lat: -29.75402 },
            { lng: 30.760345, lat: -29.755175 },
            { lng: 30.755796, lat: -29.754691 },
            { lng: 30.756397, lat: -29.75335 },
            { lng: 30.757213, lat: -29.751151 },
            { lng: 30.755968, lat: -29.750145 },
            { lng: 30.75232, lat: -29.748133 },
            { lng: 30.748029, lat: -29.746308 },
            { lng: 30.742879, lat: -29.746047 },
            { lng: 30.735025, lat: -29.74791 },
            { lng: 30.72979, lat: -29.748953 },
            { lng: 30.724897, lat: -29.751189 },
            { lng: 30.727215, lat: -29.759907 },
            { lng: 30.732322, lat: -29.764787 },
            { lng: 30.738974, lat: -29.766501 },
            { lng: 30.741806, lat: -29.769444 },
            { lng: 30.744381, lat: -29.773504 },
            { lng: 30.74893, lat: -29.776558 },
            { lng: 30.750346, lat: -29.778198 },
        ];
        var A4Map = new google.maps.Polygon({
            paths: A4,
            strokeColor: "#93D7E8",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#93D7E8",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'A4',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS5.push(A4Map);

        var A5 = [
            { lng: 30.77868, lat: -29.785178 },
            { lng: 30.782801, lat: -29.782812 },
            { lng: 30.786728, lat: -29.782301 },
            { lng: 30.789966, lat: -29.77849 },
            { lng: 30.787029, lat: -29.779798 },
            { lng: 30.78576, lat: -29.780224 },
            { lng: 30.78477, lat: -29.780409 },
            { lng: 30.784065, lat: -29.780421 },
            { lng: 30.783826, lat: -29.780184 },
            { lng: 30.783488, lat: -29.779986 },
            { lng: 30.781041, lat: -29.778501 },
            { lng: 30.779539, lat: -29.777506 },
            { lng: 30.7775, lat: -29.771539 },
            { lng: 30.774433, lat: -29.769243 },
            { lng: 30.773657, lat: -29.769113 },
            { lng: 30.77271, lat: -29.76887 },
            { lng: 30.771946, lat: -29.768385 },
            { lng: 30.771712, lat: -29.768182 },
            { lng: 30.771413, lat: -29.768054 },
            { lng: 30.771224, lat: -29.768151 },
            { lng: 30.76983, lat: -29.769452 },
            { lng: 30.7690629, lat: -29.7705423 },
            { lng: 30.769371, lat: -29.771894 },
            { lng: 30.769664, lat: -29.772948 },
            { lng: 30.769862, lat: -29.774822 },
            { lng: 30.769251, lat: -29.775471 },
            { lng: 30.768306, lat: -29.776117 },
            { lng: 30.76733, lat: -29.777857 },
            { lng: 30.76785, lat: -29.777988 },
            { lng: 30.767051, lat: -29.778831 },
            { lng: 30.765839, lat: -29.779474 },
            { lng: 30.765056, lat: -29.780321 },
            { lng: 30.764712, lat: -29.78154 },
            { lng: 30.764712, lat: -29.782321 },
            { lng: 30.765871, lat: -29.783203 },
            { lng: 30.767126, lat: -29.783319 },
            { lng: 30.768725, lat: -29.783281 },
            { lng: 30.770296, lat: -29.783584 },
            { lng: 30.77174, lat: -29.7848 },
            { lng: 30.774347, lat: -29.786858 },
            { lng: 30.775092, lat: -29.787267 },
            { lng: 30.775334, lat: -29.787295 },
            { lng: 30.776331, lat: -29.787434 },
            { lng: 30.776642, lat: -29.787153 },
            { lng: 30.77868, lat: -29.785178 },
        ];
        var A5Map = new google.maps.Polygon({
            paths: A5,
            strokeColor: "#4186F0",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#4186F0",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'A5(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS5.push(A5Map);

        var A6 = [
            { lng: 30.777898, lat: -29.752381 },
            { lng: 30.778284, lat: -29.752456 },
            { lng: 30.786095, lat: -29.760019 },
            { lng: 30.788538, lat: -29.762871 },
            { lng: 30.789482, lat: -29.768459 },
            { lng: 30.792314, lat: -29.771551 },
            { lng: 30.793282, lat: -29.773998 },
            { lng: 30.796952, lat: -29.774174 },
            { lng: 30.7972097, lat: -29.7720326 },
            { lng: 30.797553, lat: -29.768587 },
            { lng: 30.799184, lat: -29.764974 },
            { lng: 30.811543, lat: -29.765849 },
            { lng: 30.814462, lat: -29.766389 },
            { lng: 30.817251, lat: -29.766985 },
            { lng: 30.82092, lat: -29.768643 },
            { lng: 30.826564, lat: -29.768587 },
            { lng: 30.829053, lat: -29.771567 },
            { lng: 30.829074, lat: -29.773169 },
            { lng: 30.8324, lat: -29.774864 },
            { lng: 30.835769, lat: -29.774175 },
            { lng: 30.834675, lat: -29.768624 },
            { lng: 30.83313, lat: -29.768662 },
            { lng: 30.832486, lat: -29.767432 },
            { lng: 30.828366, lat: -29.767134 },
            { lng: 30.826993, lat: -29.765458 },
            { lng: 30.820534, lat: -29.766911 },
            { lng: 30.818624, lat: -29.767004 },
            { lng: 30.814633, lat: -29.765812 },
            { lng: 30.813303, lat: -29.764526 },
            { lng: 30.813088, lat: -29.761024 },
            { lng: 30.817594, lat: -29.761322 },
            { lng: 30.820642, lat: -29.76013 },
            { lng: 30.824075, lat: -29.757672 },
            { lng: 30.829396, lat: -29.757597 },
            { lng: 30.829289, lat: -29.755529 },
            { lng: 30.828495, lat: -29.753834 },
            { lng: 30.828645, lat: -29.753145 },
            { lng: 30.824075, lat: -29.750667 },
            { lng: 30.821156, lat: -29.750108 },
            { lng: 30.819569, lat: -29.7494 },
            { lng: 30.816736, lat: -29.749624 },
            { lng: 30.815363, lat: -29.7494 },
            { lng: 30.805943, lat: -29.744985 },
            { lng: 30.800557, lat: -29.742284 },
            { lng: 30.797231, lat: -29.739228 },
            { lng: 30.793991, lat: -29.741017 },
            { lng: 30.79236, lat: -29.743625 },
            { lng: 30.78721, lat: -29.745264 },
            { lng: 30.786352, lat: -29.745749 },
            { lng: 30.779743, lat: -29.749996 },
            { lng: 30.777898, lat: -29.752381 },
        ];
        var A6Map = new google.maps.Polygon({
            paths: A6,
            strokeColor: "#F4EB37",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F4EB37",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'A6',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS5.push(A6Map);

        var A7 = [
            { lng: 30.857806, lat: -29.716495 },
            { lng: 30.853043, lat: -29.712991 },
            { lng: 30.831885, lat: -29.721042 },
            { lng: 30.816865, lat: -29.728496 },
            { lng: 30.810771, lat: -29.731663 },
            { lng: 30.813432, lat: -29.73444 },
            { lng: 30.820513, lat: -29.735092 },
            { lng: 30.82341, lat: -29.734048 },
            { lng: 30.826156, lat: -29.734961 },
            { lng: 30.828581, lat: -29.733042 },
            { lng: 30.831649, lat: -29.736079 },
            { lng: 30.835265, lat: -29.733247 },
            { lng: 30.836847, lat: -29.732083 },
            { lng: 30.843003, lat: -29.734594 },
            { lng: 30.844235, lat: -29.736296 },
            { lng: 30.847871, lat: -29.736321 },
            { lng: 30.856497, lat: -29.732949 },
            { lng: 30.85463, lat: -29.736456 },
            { lng: 30.8555, lat: -29.737763 },
            { lng: 30.857672, lat: -29.736721 },
            { lng: 30.859252, lat: -29.736647 },
            { lng: 30.860321, lat: -29.735725 },
            { lng: 30.861252, lat: -29.734593 },
            { lng: 30.86146, lat: -29.73344 },
            { lng: 30.860435, lat: -29.733061 },
            { lng: 30.858713, lat: -29.732434 },
            { lng: 30.857055, lat: -29.732078 },
            { lng: 30.854437, lat: -29.728906 },
            { lng: 30.853922, lat: -29.725533 },
            { lng: 30.859072, lat: -29.718042 },
            { lng: 30.857806, lat: -29.716495 },
        ];
        var A7Map = new google.maps.Polygon({
            paths: A7,
            strokeColor: "#DB4436",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#DB4436",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'A7(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS5.push(A7Map);

        var A8 = [
            { lng: 30.829396, lat: -29.757597 },
            { lng: 30.824075, lat: -29.757672 },
            { lng: 30.820642, lat: -29.76013 },
            { lng: 30.817594, lat: -29.761322 },
            { lng: 30.813088, lat: -29.761024 },
            { lng: 30.813303, lat: -29.764526 },
            { lng: 30.814462, lat: -29.766389 },
            { lng: 30.817251, lat: -29.766985 },
            { lng: 30.820384, lat: -29.766911 },
            { lng: 30.826993, lat: -29.765458 },
            { lng: 30.828366, lat: -29.767134 },
            { lng: 30.832486, lat: -29.767432 },
            { lng: 30.832357, lat: -29.765607 },
            { lng: 30.8321, lat: -29.763446 },
            { lng: 30.831585, lat: -29.762179 },
            { lng: 30.830255, lat: -29.76177 },
            { lng: 30.829954, lat: -29.760726 },
            { lng: 30.829353, lat: -29.760577 },
            { lng: 30.83004, lat: -29.758975 },
            { lng: 30.829396, lat: -29.757597 },
        ];
        var A8Map = new google.maps.Polygon({
            paths: A8,
            strokeColor: "#7C3592",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#7C3592",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'A8(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS5.push(A8Map);

        var A9 = [
            { lng: 30.848472, lat: -29.744016 },
            { lng: 30.845726, lat: -29.744165 },
            { lng: 30.843472, lat: -29.745656 },
            { lng: 30.842292, lat: -29.746885 },
            { lng: 30.837378, lat: -29.747072 },
            { lng: 30.828731, lat: -29.747183 },
            { lng: 30.821414, lat: -29.747388 },
            { lng: 30.819826, lat: -29.747537 },
            { lng: 30.81811, lat: -29.748022 },
            { lng: 30.815363, lat: -29.7494 },
            { lng: 30.816736, lat: -29.749624 },
            { lng: 30.819569, lat: -29.7494 },
            { lng: 30.82135, lat: -29.750108 },
            { lng: 30.824075, lat: -29.750667 },
            { lng: 30.826242, lat: -29.751971 },
            { lng: 30.827615, lat: -29.75266 },
            { lng: 30.828645, lat: -29.753145 },
            { lng: 30.828495, lat: -29.753834 },
            { lng: 30.829289, lat: -29.755529 },
            { lng: 30.829396, lat: -29.757597 },
            { lng: 30.83004, lat: -29.758975 },
            { lng: 30.829353, lat: -29.760577 },
            { lng: 30.830255, lat: -29.76177 },
            { lng: 30.831585, lat: -29.762179 },
            { lng: 30.8321, lat: -29.763446 },
            { lng: 30.832486, lat: -29.767432 },
            { lng: 30.83313, lat: -29.768662 },
            { lng: 30.834675, lat: -29.768624 },
            { lng: 30.835769, lat: -29.774175 },
            { lng: 30.837593, lat: -29.775646 },
            { lng: 30.841412, lat: -29.772182 },
            { lng: 30.845361, lat: -29.767656 },
            { lng: 30.85609, lat: -29.75756 },
            { lng: 30.848472, lat: -29.744016 },
        ];
        var A9Map = new google.maps.Polygon({
            paths: A9,
            strokeColor: "#009D57",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#009D57",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'A9(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS5.push(A9Map);

        var A10 = [
            { lng: 30.810685, lat: -29.731663 },
            { lng: 30.806522, lat: -29.73457 },
            { lng: 30.801845, lat: -29.736135 },
            { lng: 30.798154, lat: -29.737402 },
            { lng: 30.797081, lat: -29.739302 },
            { lng: 30.800557, lat: -29.742284 },
            { lng: 30.811544, lat: -29.747612 },
            { lng: 30.815363, lat: -29.7494 },
            { lng: 30.81811, lat: -29.748022 },
            { lng: 30.819826, lat: -29.747537 },
            { lng: 30.821414, lat: -29.747388 },
            { lng: 30.842292, lat: -29.746885 },
            { lng: 30.839803, lat: -29.744482 },
            { lng: 30.836306, lat: -29.73893 },
            { lng: 30.831564, lat: -29.736079 },
            { lng: 30.828495, lat: -29.733042 },
            { lng: 30.82607, lat: -29.734961 },
            { lng: 30.823324, lat: -29.734048 },
            { lng: 30.820427, lat: -29.735092 },
            { lng: 30.813346, lat: -29.73444 },
            { lng: 30.810685, lat: -29.731663 },
        ];
        var A10Map = new google.maps.Polygon({
            paths: A10,
            strokeColor: "#4186F0",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#4186F0",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'A10(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS5.push(A10Map);

        var A11 = [
            { lng: 30.800245, lat: -29.777798 },
            { lng: 30.800314, lat: -29.778417 },
            { lng: 30.800023, lat: -29.778612 },
            { lng: 30.798926, lat: -29.779374 },
            { lng: 30.797505, lat: -29.780899 },
            { lng: 30.797446, lat: -29.782452 },
            { lng: 30.796593, lat: -29.784209 },
            { lng: 30.795799, lat: -29.787445 },
            { lng: 30.79559, lat: -29.788356 },
            { lng: 30.794833, lat: -29.788355 },
            { lng: 30.793964, lat: -29.788523 },
            { lng: 30.793868, lat: -29.789044 },
            { lng: 30.790625, lat: -29.791463 },
            { lng: 30.790527, lat: -29.793518 },
            { lng: 30.797575, lat: -29.794009 },
            { lng: 30.801523, lat: -29.793124 },
            { lng: 30.803926, lat: -29.792877 },
            { lng: 30.80605, lat: -29.792528 },
            { lng: 30.8056, lat: -29.791309 },
            { lng: 30.806329, lat: -29.790246 },
            { lng: 30.807596, lat: -29.789465 },
            { lng: 30.811952, lat: -29.785554 },
            { lng: 30.812789, lat: -29.783133 },
            { lng: 30.80987, lat: -29.78115 },
            { lng: 30.807553, lat: -29.780935 },
            { lng: 30.806732, lat: -29.780503 },
            { lng: 30.805997, lat: -29.780015 },
            { lng: 30.805093, lat: -29.779538 },
            { lng: 30.804039, lat: -29.778958 },
            { lng: 30.803443, lat: -29.779545 },
            { lng: 30.802749, lat: -29.778874 },
            { lng: 30.802388, lat: -29.778374 },
            { lng: 30.80178, lat: -29.778545 },
            { lng: 30.801409, lat: -29.778381 },
            { lng: 30.80181, lat: -29.777743 },
            { lng: 30.801367, lat: -29.77749 },
            { lng: 30.80092, lat: -29.777261 },
            { lng: 30.800494, lat: -29.777004 },
            { lng: 30.80029, lat: -29.776897 },
            { lng: 30.800117, lat: -29.77678 },
            { lng: 30.799656, lat: -29.776561 },
            { lng: 30.799644, lat: -29.776621 },
            { lng: 30.799962, lat: -29.777086 },
            { lng: 30.800245, lat: -29.777798 },
        ];
        var A11Map = new google.maps.Polygon({
            paths: A11,
            strokeColor: "#F4B400",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F4B400",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'A11(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS5.push(A11Map);

        var A12 = [
            { lng: 30.797515, lat: -29.780899 },
            { lng: 30.798937, lat: -29.779374 },
            { lng: 30.800325, lat: -29.778417 },
            { lng: 30.800146, lat: -29.77747 },
            { lng: 30.799666, lat: -29.776561 },
            { lng: 30.796952, lat: -29.774174 },
            { lng: 30.793904, lat: -29.773914 },
            { lng: 30.793282, lat: -29.773998 },
            { lng: 30.792286, lat: -29.774951 },
            { lng: 30.790086, lat: -29.77839 },
            { lng: 30.786718, lat: -29.782301 },
            { lng: 30.782812, lat: -29.782812 },
            { lng: 30.778691, lat: -29.785178 },
            { lng: 30.778195, lat: -29.785667 },
            { lng: 30.77757, lat: -29.786268 },
            { lng: 30.776342, lat: -29.787434 },
            { lng: 30.778151, lat: -29.787607 },
            { lng: 30.7788, lat: -29.7877 },
            { lng: 30.781288, lat: -29.787902 },
            { lng: 30.78132, lat: -29.79062 },
            { lng: 30.782399, lat: -29.791765 },
            { lng: 30.783402, lat: -29.792304 },
            { lng: 30.790484, lat: -29.793369 },
            { lng: 30.790636, lat: -29.791463 },
            { lng: 30.791298, lat: -29.790942 },
            { lng: 30.791671, lat: -29.790737 },
            { lng: 30.792246, lat: -29.790215 },
            { lng: 30.793014, lat: -29.789731 },
            { lng: 30.793567, lat: -29.789377 },
            { lng: 30.793857, lat: -29.789165 },
            { lng: 30.793975, lat: -29.788523 },
            { lng: 30.794844, lat: -29.788355 },
            { lng: 30.795579, lat: -29.788356 },
            { lng: 30.795743, lat: -29.787836 },
            { lng: 30.79581, lat: -29.787445 },
            { lng: 30.796056, lat: -29.78646 },
            { lng: 30.796593, lat: -29.784191 },
            { lng: 30.797456, lat: -29.782452 },
            { lng: 30.797515, lat: -29.780899 },
        ];
        var A12Map = new google.maps.Polygon({
            paths: A12,
            strokeColor: "#0BA9CC",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#0BA9CC",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'A12(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS5.push(A12Map);

        var A13 = [
            { lng: 30.831564, lat: -29.736079 },
            { lng: 30.836306, lat: -29.73893 },
            { lng: 30.839803, lat: -29.744482 },
            { lng: 30.842292, lat: -29.746885 },
            { lng: 30.843472, lat: -29.745656 },
            { lng: 30.845726, lat: -29.744165 },
            { lng: 30.848472, lat: -29.744016 },
            { lng: 30.847197, lat: -29.739594 },
            { lng: 30.844235, lat: -29.736296 },
            { lng: 30.843003, lat: -29.734594 },
            { lng: 30.836847, lat: -29.732083 },
            { lng: 30.831564, lat: -29.736079 },
        ];
        var A13Map = new google.maps.Polygon({
            paths: A13,
            strokeColor: "#F4B400",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F4B400",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'A13(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS5.push(A13Map);

        var A14 = [
            { lng: 30.771407, lat: -29.767914 },
            { lng: 30.771957, lat: -29.768385 },
            { lng: 30.772721, lat: -29.76887 },
            { lng: 30.773657, lat: -29.769113 },
            { lng: 30.774433, lat: -29.769243 },
            { lng: 30.777511, lat: -29.771539 },
            { lng: 30.77955, lat: -29.777506 },
            { lng: 30.783499, lat: -29.779986 },
            { lng: 30.784076, lat: -29.780421 },
            { lng: 30.784781, lat: -29.780409 },
            { lng: 30.785636, lat: -29.780248 },
            { lng: 30.786556, lat: -29.779993 },
            { lng: 30.790108, lat: -29.77839 },
            { lng: 30.791556, lat: -29.776157 },
            { lng: 30.792286, lat: -29.774951 },
            { lng: 30.793304, lat: -29.773998 },
            { lng: 30.792314, lat: -29.771551 },
            { lng: 30.789482, lat: -29.768459 },
            { lng: 30.788538, lat: -29.762871 },
            { lng: 30.786052, lat: -29.759981 },
            { lng: 30.77837, lat: -29.762328 },
            { lng: 30.77425, lat: -29.764862 },
            { lng: 30.771407, lat: -29.767914 },
        ];
        var A14Map = new google.maps.Polygon({
            paths: A14,
            strokeColor: "#F4B400",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F4B400",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'A14(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS5.push(A14Map);
    },

    DrawSubSector6: function (map) {
        var NoGoZoneS6 = [
            { lng: 31.002136, lat: -29.784715 },
            { lng: 31.000779, lat: -29.78656 },
            { lng: 30.998654, lat: -29.789558 },
            { lng: 30.997058, lat: -29.792723 },
            { lng: 30.996637, lat: -29.796262 },
            { lng: 30.995013, lat: -29.797044 },
            { lng: 30.994098, lat: -29.798626 },
            { lng: 30.992524, lat: -29.800293 },
            { lng: 30.992539, lat: -29.801492 },
            { lng: 30.99525, lat: -29.800825 },
            { lng: 30.999021, lat: -29.801908 },
            { lng: 30.999813, lat: -29.801997 },
            { lng: 31.003655, lat: -29.802588 },
            { lng: 31.007346, lat: -29.804992 },
            { lng: 31.008477, lat: -29.808351 },
            { lng: 31.009254, lat: -29.806409 },
            { lng: 31.009684, lat: -29.802704 },
            { lng: 31.009462, lat: -29.802197 },
            { lng: 31.009469, lat: -29.801214 },
            { lng: 31.009276, lat: -29.800078 },
            { lng: 31.009297, lat: -29.798831 },
            { lng: 31.010143, lat: -29.797238 },
            { lng: 31.010088, lat: -29.796114 },
            { lng: 31.009775, lat: -29.794337 },
            { lng: 31.010734, lat: -29.793225 },
            { lng: 31.009776, lat: -29.792435 },
            { lng: 31.008946, lat: -29.790247 },
            { lng: 31.007373, lat: -29.788776 },
            { lng: 31.00371, lat: -29.787174 },
            { lng: 31.002136, lat: -29.784715 },
        ];
        var ENoZoneMap = new google.maps.Polygon({
            paths: NoGoZoneS6,
            strokeColor: "#000000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#000000",
            fillOpacity: 0.55,
            draggable: false,
            geodesic: false,
            name: 'NoCoverage',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS6.push(ENoZoneMap);

        var E1OT = [
            { lng: 31.017151, lat: -29.811083 },
            { lng: 31.011809, lat: -29.807373 },
            { lng: 31.012323, lat: -29.801638 },
            { lng: 31.01477, lat: -29.797132 },
            { lng: 31.016357, lat: -29.794921 },
            { lng: 31.016968, lat: -29.791783 },
            { lng: 31.01992, lat: -29.789795 },
            { lng: 31.020412, lat: -29.78926 },
            { lng: 31.023585, lat: -29.781358 },
            { lng: 31.023382, lat: -29.778044 },
            { lng: 31.024615, lat: -29.775175 },
            { lng: 31.016912, lat: -29.775101 },
            { lng: 31.016934, lat: -29.767502 },
            { lng: 31.009208, lat: -29.76739 },
            { lng: 31.00171, lat: -29.767464 },
            { lng: 30.99377, lat: -29.767297 },
            { lng: 30.99377, lat: -29.775064 },
            { lng: 30.993491, lat: -29.775026 },
            { lng: 30.985938, lat: -29.774878 },
            { lng: 30.985917, lat: -29.77512 },
            { lng: 30.985842, lat: -29.782793 },
            { lng: 30.993395, lat: -29.78283 },
            { lng: 30.993652, lat: -29.790996 },
            { lng: 30.993427, lat: -29.790996 },
            { lng: 30.985895, lat: -29.790987 },
            { lng: 30.986088, lat: -29.798435 },
            { lng: 30.977248, lat: -29.798454 },
            { lng: 30.978742, lat: -29.802909 },
            { lng: 30.977892, lat: -29.806451 },
            { lng: 30.994672, lat: -29.813303 },
            { lng: 31.000156, lat: -29.813764 },
            { lng: 31.014318, lat: -29.812423 },
            { lng: 31.017151, lat: -29.811083 },
        ];

        var E1IN = [
            { lng: 31.009684, lat: -29.802704 },
            { lng: 31.009462, lat: -29.802197 },
            { lng: 31.0093928, lat: -29.7985327 },
            { lng: 31.010143, lat: -29.797238 },
            { lng: 31.009775, lat: -29.794337 },
            { lng: 31.010734, lat: -29.793225 },
            { lng: 31.009776, lat: -29.792435 },
            { lng: 31.008946, lat: -29.790247 },
            { lng: 31.007373, lat: -29.788776 },
            { lng: 31.00371, lat: -29.787174 },
            { lng: 31.002136, lat: -29.784715 },
            { lng: 30.998654, lat: -29.789558 },
            { lng: 30.997058, lat: -29.792723 },
            { lng: 30.996637, lat: -29.796262 },
            { lng: 30.995013, lat: -29.797044 },
            { lng: 30.9938246, lat: -29.7989199 },
            { lng: 30.992524, lat: -29.800293 },
            { lng: 30.992539, lat: -29.801492 },
            { lng: 30.99525, lat: -29.800825 },
            { lng: 30.999021, lat: -29.801908 },
            { lng: 31.003655, lat: -29.802588 },
            { lng: 31.007346, lat: -29.804992 },
            { lng: 31.008477, lat: -29.808351 },
            { lng: 31.009254, lat: -29.806409 },
            { lng: 31.009684, lat: -29.802704 },
        ];
        var E1Map = new google.maps.Polygon({
            paths: [E1OT, E1IN],
            strokeColor: "#F9A825",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F9A825",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'E1',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS6.push(E1Map);

        var E2 = [
            { lng: 31.060372, lat: -29.713261 },
            { lng: 31.066974, lat: -29.718333 },
            { lng: 31.068002, lat: -29.719565 },
            { lng: 31.072764, lat: -29.722566 },
            { lng: 31.073879, lat: -29.72334 },
            { lng: 31.076368, lat: -29.724823 },
            { lng: 31.079102, lat: -29.725634 },
            { lng: 31.080125, lat: -29.725873 },
            { lng: 31.080654, lat: -29.725868 },
            { lng: 31.08132, lat: -29.726167 },
            { lng: 31.081792, lat: -29.726484 },
            { lng: 31.082098, lat: -29.725771 },
            { lng: 31.083434, lat: -29.724164 },
            { lng: 31.084627, lat: -29.722317 },
            { lng: 31.085422, lat: -29.721086 },
            { lng: 31.086314, lat: -29.719612 },
            { lng: 31.086749, lat: -29.719067 },
            { lng: 31.087264, lat: -29.71832 },
            { lng: 31.087737, lat: -29.717667 },
            { lng: 31.088571, lat: -29.716489 },
            { lng: 31.089233, lat: -29.715348 },
            { lng: 31.090284, lat: -29.71367 },
            { lng: 31.091351, lat: -29.712125 },
            { lng: 31.089056, lat: -29.71127 },
            { lng: 31.092881, lat: -29.7026 },
            { lng: 31.0884965, lat: -29.7018353 },
            { lng: 31.0863292, lat: -29.7009686 },
            { lng: 31.0816086, lat: -29.7055302 },
            { lng: 31.0857713, lat: -29.7081349 },
            { lng: 31.0855675, lat: -29.7095513 },
            { lng: 31.0858357, lat: -29.7102782 },
            { lng: 31.0827673, lat: -29.7083772 },
            { lng: 31.0812438, lat: -29.7107814 },
            { lng: 31.0798534, lat: -29.7130433 },
            { lng: 31.0780681, lat: -29.7123468 },
            { lng: 31.0770558, lat: -29.7140064 },
            { lng: 31.0773289, lat: -29.7142509 },
            { lng: 31.0776604, lat: -29.7147771 },
            { lng: 31.0780069, lat: -29.714949 },
            { lng: 31.0776325, lat: -29.7156981 },
            { lng: 31.0771411, lat: -29.7153979 },
            { lng: 31.0763461, lat: -29.7148674 },
            { lng: 31.0753429, lat: -29.71572 },
            { lng: 31.0734332, lat: -29.7141825 },
            { lng: 31.0732079, lat: -29.7142198 },
            { lng: 31.0706759, lat: -29.7121512 },
            { lng: 31.07, lat: -29.7118716 },
            { lng: 31.0702575, lat: -29.71039 },
            { lng: 31.0707295, lat: -29.70957 },
            { lng: 31.0718239, lat: -29.7089549 },
            { lng: 31.0710299, lat: -29.7071285 },
            { lng: 31.0716737, lat: -29.7047988 },
            { lng: 31.068478, lat: -29.704568 },
            { lng: 31.065584, lat: -29.708207 },
            { lng: 31.064545, lat: -29.709153 },
            { lng: 31.063815, lat: -29.709798 },
            { lng: 31.063255, lat: -29.710394 },
            { lng: 31.062775, lat: -29.710932 },
            { lng: 31.062154, lat: -29.711546 },
            { lng: 31.061509, lat: -29.712049 },
            { lng: 31.060372, lat: -29.713261 },
        ];
        var E2Map = new google.maps.Polygon({
            paths: E2,
            strokeColor: "#E65100",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#E65100",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'E2',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS6.push(E2Map);

        var E3 = [
            { lng: 31.049425, lat: -29.7907 },
            { lng: 31.025, lat: -29.790625 },
            { lng: 31.020412, lat: -29.78926 },
            { lng: 31.016968, lat: -29.791783 },
            { lng: 31.016357, lat: -29.794921 },
            { lng: 31.01477, lat: -29.797132 },
            { lng: 31.012323, lat: -29.801638 },
            { lng: 31.011809, lat: -29.807373 },
            { lng: 31.017151, lat: -29.811083 },
            { lng: 31.021614, lat: -29.811753 },
            { lng: 31.025476, lat: -29.814359 },
            { lng: 31.027408, lat: -29.818837 },
            { lng: 31.031399, lat: -29.817943 },
            { lng: 31.033115, lat: -29.820205 },
            { lng: 31.033684, lat: -29.822467 },
            { lng: 31.038523, lat: -29.822784 },
            { lng: 31.049425, lat: -29.7907 },
        ];
        var E3Map = new google.maps.Polygon({
            paths: E3,
            strokeColor: "#0288D1",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#0288D1",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'E3',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS6.push(E3Map);

        var E4 = [
            { lng: 31.029077, lat: -29.784289 },
            { lng: 31.030147, lat: -29.784633 },
            { lng: 31.031094, lat: -29.785213 },
            { lng: 31.031555, lat: -29.785588 },
            { lng: 31.031955, lat: -29.786244 },
            { lng: 31.03214, lat: -29.787122 },
            { lng: 31.03229, lat: -29.787569 },
            { lng: 31.03265, lat: -29.788069 },
            { lng: 31.033181, lat: -29.788493 },
            { lng: 31.034315, lat: -29.788937 },
            { lng: 31.03741, lat: -29.781802 },
            { lng: 31.037529, lat: -29.781165 },
            { lng: 31.037164, lat: -29.779991 },
            { lng: 31.036026, lat: -29.778874 },
            { lng: 31.032379, lat: -29.777235 },
            { lng: 31.029077, lat: -29.784289 },
        ];
        var E4Map = new google.maps.Polygon({
            paths: E4,
            strokeColor: "#01579B",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#01579B",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'E4(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS6.push(E4Map);

        var E5OT = [
            { lng: 31.050968, lat: -29.767655 },
            { lng: 31.016934, lat: -29.767502 },
            { lng: 31.016912, lat: -29.775101 },
            { lng: 31.024615, lat: -29.775175 },
            { lng: 31.023328, lat: -29.778528 },
            { lng: 31.023585, lat: -29.781358 },
            { lng: 31.020455, lat: -29.789297 },
            { lng: 31.020263, lat: -29.789347 },
            { lng: 31.025134, lat: -29.790735 },
            { lng: 31.0335118, lat: -29.7906507 },
            { lng: 31.049832, lat: -29.790539 },
            { lng: 31.054229, lat: -29.781638 },
            { lng: 31.057962, lat: -29.774691 },
            { lng: 31.060108, lat: -29.771208 },
            { lng: 31.051979, lat: -29.767685 },
            { lng: 31.050968, lat: -29.767655 },
        ];

        var E5IN = [
            { lng: 31.037529, lat: -29.781165 },
            { lng: 31.037164, lat: -29.779991 },
            { lng: 31.036026, lat: -29.778874 },
            { lng: 31.032379, lat: -29.777235 },
            { lng: 31.029077, lat: -29.784289 },
            { lng: 31.030147, lat: -29.784633 },
            { lng: 31.031555, lat: -29.785588 },
            { lng: 31.031955, lat: -29.786244 },
            { lng: 31.03229, lat: -29.787569 },
            { lng: 31.03265, lat: -29.788069 },
            { lng: 31.033181, lat: -29.788493 },
            { lng: 31.034315, lat: -29.788937 },
            { lng: 31.03741, lat: -29.781802 },
            { lng: 31.037529, lat: -29.781165 },
        ];

        var E5Map = new google.maps.Polygon({
            paths: [E5OT, E5IN],
            strokeColor: "#AFB42B",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#AFB42B",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'E1',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS6.push(E5Map);

        var E6 = [
            { lng: 31.009208, lat: -29.76739 },
            { lng: 31.050925, lat: -29.767618 },
            { lng: 31.050461, lat: -29.76506 },
            { lng: 31.049441, lat: -29.761856 },
            { lng: 31.048175, lat: -29.761572 },
            { lng: 31.049141, lat: -29.757623 },
            { lng: 31.049576, lat: -29.75671 },
            { lng: 31.050611, lat: -29.756584 },
            { lng: 31.050139, lat: -29.754633 },
            { lng: 31.051528, lat: -29.754647 },
            { lng: 31.056855, lat: -29.757162 },
            { lng: 31.05582, lat: -29.758903 },
            { lng: 31.055707, lat: -29.76017 },
            { lng: 31.054745, lat: -29.761322 },
            { lng: 31.055176, lat: -29.762722 },
            { lng: 31.051936, lat: -29.767649 },
            { lng: 31.060108, lat: -29.771208 },
            { lng: 31.0721, lat: -29.753039 },
            { lng: 31.068864, lat: -29.751673 },
            { lng: 31.017275, lat: -29.751994 },
            { lng: 31.017539, lat: -29.744088 },
            { lng: 31.008012, lat: -29.744088 },
            { lng: 31.0077823, lat: -29.7522881 },
            { lng: 31.0171808, lat: -29.7524371 },
            { lng: 31.017024, lat: -29.759513 },
            { lng: 31.008929, lat: -29.759624 },
            { lng: 31.009208, lat: -29.76739 },
        ];
        var E6Map = new google.maps.Polygon({
            paths: E6,
            strokeColor: "#4186F0",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#4186F0",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'E6',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS6.push(E6Map);

        var E7 = [
            { lng: 31.02486, lat: -29.751671 },
            { lng: 31.068875, lat: -29.751654 },
            { lng: 31.069937, lat: -29.749214 },
            { lng: 31.070923, lat: -29.746978 },
            { lng: 31.073611, lat: -29.742263 },
            { lng: 31.047916, lat: -29.728131 },
            { lng: 31.045716, lat: -29.730879 },
            { lng: 31.040625, lat: -29.735919 },
            { lng: 31.041038, lat: -29.743473 },
            { lng: 31.024988, lat: -29.743735 },
            { lng: 31.02486, lat: -29.751671 },
        ];
        var E7Map = new google.maps.Polygon({
            paths: E7,
            strokeColor: "#9C27B0",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#9C27B0",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'E7',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS6.push(E7Map);

        var E8 = [
            { lng: 31.060372, lat: -29.713261 },
            { lng: 31.055985, lat: -29.719071 },
            { lng: 31.051313, lat: -29.722265 },
            { lng: 31.050795, lat: -29.723093 },
            { lng: 31.050298, lat: -29.724386 },
            { lng: 31.047916, lat: -29.728131 },
            { lng: 31.073611, lat: -29.742263 },
            { lng: 31.081824, lat: -29.726409 },
            { lng: 31.080654, lat: -29.725868 },
            { lng: 31.080125, lat: -29.725873 },
            { lng: 31.076368, lat: -29.724823 },
            { lng: 31.073879, lat: -29.72334 },
            { lng: 31.068002, lat: -29.719565 },
            { lng: 31.066974, lat: -29.718333 },
            { lng: 31.060372, lat: -29.713261 },
        ];
        var E8Map = new google.maps.Polygon({
            paths: E8,
            strokeColor: "#CDDC39",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#CDDC39",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'E8',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS6.push(E8Map);

        var E9 = [
            { lng: 31.0753429, lat: -29.71572 },
            { lng: 31.0763461, lat: -29.7148674 },
            { lng: 31.0776325, lat: -29.7156981 },
            { lng: 31.0780069, lat: -29.714949 },
            { lng: 31.0776604, lat: -29.7147771 },
            { lng: 31.0773289, lat: -29.7142509 },
            { lng: 31.0770558, lat: -29.7140064 },
            { lng: 31.0780681, lat: -29.7123468 },
            { lng: 31.0798534, lat: -29.7130433 },
            { lng: 31.0827673, lat: -29.7083772 },
            { lng: 31.0845483, lat: -29.7094954 },
            { lng: 31.0858357, lat: -29.7102782 },
            { lng: 31.0855675, lat: -29.7095513 },
            { lng: 31.0857713, lat: -29.7081349 },
            { lng: 31.0792482, lat: -29.7040346 },
            { lng: 31.0775102, lat: -29.7049852 },
            { lng: 31.075157, lat: -29.706085 },
            { lng: 31.07329, lat: -29.704948 },
            { lng: 31.0716737, lat: -29.7047988 },
            { lng: 31.0710299, lat: -29.7071285 },
            { lng: 31.0718239, lat: -29.7089549 },
            { lng: 31.0707295, lat: -29.70957 },
            { lng: 31.0702575, lat: -29.71039 },
            { lng: 31.07, lat: -29.7118716 },
            { lng: 31.0706759, lat: -29.7121512 },
            { lng: 31.0732079, lat: -29.7142198 },
            { lng: 31.0734332, lat: -29.7141825 },
            { lng: 31.0753429, lat: -29.71572 },
        ];
        var E9Map = new google.maps.Polygon({
            paths: E9,
            strokeColor: "#009D57",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#009D57",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'E9(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS6.push(E9Map);

        var E10 = [
            { lng: 31.068864, lat: -29.751673 },
            { lng: 31.0721, lat: -29.753039 },
            { lng: 31.0778177, lat: -29.7450763 },
            { lng: 31.0816157, lat: -29.7391889 },
            { lng: 31.0852206, lat: -29.7333944 },
            { lng: 31.0879028, lat: -29.7294069 },
            { lng: 31.0899842, lat: -29.7267796 },
            { lng: 31.0904777, lat: -29.7245807 },
            { lng: 31.0902846, lat: -29.7237236 },
            { lng: 31.0919154, lat: -29.7203693 },
            { lng: 31.094871, lat: -29.713849 },
            { lng: 31.091351, lat: -29.712125 },
            { lng: 31.0883532, lat: -29.7169132 },
            { lng: 31.086314, lat: -29.719612 },
            { lng: 31.0830319, lat: -29.7246274 },
            { lng: 31.082098, lat: -29.725771 },
            { lng: 31.0816126, lat: -29.7269315 },
            { lng: 31.085561, lat: -29.729167 },
            { lng: 31.08319, lat: -29.733097 },
            { lng: 31.080518, lat: -29.734105 },
            { lng: 31.0798776, lat: -29.7355371 },
            { lng: 31.078836, lat: -29.736478 },
            { lng: 31.077953, lat: -29.73759 },
            { lng: 31.0770023, lat: -29.7379965 },
            { lng: 31.0760738, lat: -29.7376256 },
            { lng: 31.073525, lat: -29.742561 },
            { lng: 31.0705006, lat: -29.748467 },
            { lng: 31.068864, lat: -29.751673 },
        ];
        var E10Map = new google.maps.Polygon({
            paths: E10,
            strokeColor: "#009D57",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#009D57",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'E10(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS6.push(E10Map);

        var E11 = [
            { lng: 31.08319, lat: -29.733097 },
            { lng: 31.085561, lat: -29.729167 },
            { lng: 31.0816126, lat: -29.7269315 },
            { lng: 31.0760738, lat: -29.7376256 },
            { lng: 31.077048, lat: -29.737864 },
            { lng: 31.077953, lat: -29.73759 },
            { lng: 31.0785916, lat: -29.7367489 },
            { lng: 31.0789512, lat: -29.736355 },
            { lng: 31.0795416, lat: -29.7357162 },
            { lng: 31.07987, lat: -29.735404 },
            { lng: 31.080518, lat: -29.734105 },
            { lng: 31.08319, lat: -29.733097 },
        ];
        var E11Map = new google.maps.Polygon({
            paths: E11,
            strokeColor: "#A61B4A",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#A61B4A",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'E11(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS6.push(E11Map);

        var E12 = [
            { lng: 31.001476, lat: -29.728256 },
            { lng: 31.009289, lat: -29.728256 },
            { lng: 31.017102, lat: -29.728256 },
            { lng: 31.017102, lat: -29.736068 },
            { lng: 31.024914, lat: -29.736068 },
            { lng: 31.032726, lat: -29.736068 },
            { lng: 31.040539, lat: -29.736068 },
            { lng: 31.042942, lat: -29.733473 },
            { lng: 31.045716, lat: -29.730879 },
            { lng: 31.047787, lat: -29.728318 },
            { lng: 31.050298, lat: -29.724386 },
            { lng: 31.050795, lat: -29.723093 },
            { lng: 31.051313, lat: -29.722265 },
            { lng: 31.055985, lat: -29.719071 },
            { lng: 31.059918, lat: -29.713906 },
            { lng: 31.062775, lat: -29.710932 },
            { lng: 31.065584, lat: -29.708207 },
            { lng: 31.068478, lat: -29.704568 },
            { lng: 31.067534, lat: -29.702052 },
            { lng: 31.064358, lat: -29.696609 },
            { lng: 31.062298, lat: -29.690197 },
            { lng: 31.058178, lat: -29.686693 },
            { lng: 31.04539, lat: -29.672077 },
            { lng: 31.040068, lat: -29.676402 },
            { lng: 31.032429, lat: -29.676626 },
            { lng: 31.032555, lat: -29.689044 },
            { lng: 31.031912, lat: -29.692399 },
            { lng: 31.03025, lat: -29.694533 },
            { lng: 31.029422, lat: -29.695067 },
            { lng: 31.028414, lat: -29.696018 },
            { lng: 31.027664, lat: -29.69667 },
            { lng: 31.026718, lat: -29.69736 },
            { lng: 31.025098, lat: -29.698043 },
            { lng: 31.023585, lat: -29.697851 },
            { lng: 31.021021, lat: -29.697618 },
            { lng: 31.017533, lat: -29.698615 },
            { lng: 31.009806, lat: -29.701299 },
            { lng: 31.000792, lat: -29.699137 },
            { lng: 30.99487, lat: -29.706353 },
            { lng: 30.991691, lat: -29.710826 },
            { lng: 30.992935, lat: -29.713212 },
            { lng: 30.993664, lat: -29.720442 },
            { lng: 30.993664, lat: -29.728256 },
            { lng: 30.993664, lat: -29.736068 },
            { lng: 31.001476, lat: -29.736068 },
            { lng: 31.001476, lat: -29.728256 },
        ];
        var E12Map = new google.maps.Polygon({
            paths: E12,
            strokeColor: "#795046",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#795046",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'E12',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS6.push(E12Map);

        var E13 = [
            { lng: 30.993739, lat: -29.72034 },
            { lng: 30.99301, lat: -29.713109 },
            { lng: 30.991767, lat: -29.710724 },
            { lng: 30.994945, lat: -29.706251 },
            { lng: 31.000867, lat: -29.699034 },
            { lng: 31.009882, lat: -29.701196 },
            { lng: 31.017608, lat: -29.698512 },
            { lng: 31.021107, lat: -29.697487 },
            { lng: 31.025184, lat: -29.697913 },
            { lng: 31.026342, lat: -29.697499 },
            { lng: 31.027139, lat: -29.697052 },
            { lng: 31.030336, lat: -29.694402 },
            { lng: 31.031987, lat: -29.692296 },
            { lng: 31.032801, lat: -29.68909 },
            { lng: 31.032801, lat: -29.681278 },
            { lng: 31.032801, lat: -29.673466 },
            { lng: 31.032801, lat: -29.665653 },
            { lng: 31.024989, lat: -29.665653 },
            { lng: 31.017177, lat: -29.665653 },
            { lng: 31.009364, lat: -29.665653 },
            { lng: 31.001551, lat: -29.665653 },
            { lng: 31.001551, lat: -29.673466 },
            { lng: 31.001551, lat: -29.681278 },
            { lng: 30.993739, lat: -29.681278 },
            { lng: 30.985927, lat: -29.681278 },
            { lng: 30.985927, lat: -29.68909 },
            { lng: 30.978114, lat: -29.68909 },
            { lng: 30.978114, lat: -29.696903 },
            { lng: 30.978114, lat: -29.704716 },
            { lng: 30.978114, lat: -29.712528 },
            { lng: 30.978114, lat: -29.72034 },
            { lng: 30.985927, lat: -29.72034 },
            { lng: 30.993739, lat: -29.72034 },
        ];
        var E13Map = new google.maps.Polygon({
            paths: E13,
            strokeColor: "#F8971B",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F8971B",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'E13',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS6.push(E13Map);

        var E14 = [
            { lng: 31.049141, lat: -29.757623 },
            { lng: 31.048175, lat: -29.761572 },
            { lng: 31.049441, lat: -29.761856 },
            { lng: 31.050461, lat: -29.76506 },
            { lng: 31.050925, lat: -29.767618 },
            { lng: 31.051936, lat: -29.767649 },
            { lng: 31.055176, lat: -29.762722 },
            { lng: 31.054745, lat: -29.761322 },
            { lng: 31.055707, lat: -29.76017 },
            { lng: 31.05582, lat: -29.758903 },
            { lng: 31.056855, lat: -29.757162 },
            { lng: 31.051528, lat: -29.754647 },
            { lng: 31.050139, lat: -29.754633 },
            { lng: 31.050611, lat: -29.756584 },
            { lng: 31.049576, lat: -29.75671 },
            { lng: 31.049141, lat: -29.757623 },
        ];
        var E14Map = new google.maps.Polygon({
            paths: E14,
            strokeColor: "#E65100",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#E65100",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'E14',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS6.push(E14Map);

        var NoGoZone = [
            { lng: 30.995027, lat: -29.884612 },
            { lng: 30.9914782, lat: -29.8850666 },
            { lng: 30.9875905, lat: -29.8894339 },
            { lng: 30.9856822, lat: -29.8974388 },
            { lng: 30.9914141, lat: -29.8999585 },
            { lng: 30.9962022, lat: -29.9035941 },
            { lng: 31.000046, lat: -29.907657 },
            { lng: 31.011529, lat: -29.907553 },
            { lng: 31.016679, lat: -29.90147 },
            { lng: 31.027236, lat: -29.891667 },
            { lng: 31.0409166, lat: -29.8856992 },
            { lng: 31.04126, lat: -29.8831317 },
            { lng: 31.0318186, lat: -29.8771781 },
            { lng: 31.0122492, lat: -29.8779223 },
            { lng: 31.0070993, lat: -29.8872991 },
            { lng: 30.995027, lat: -29.884612 },
        ];
        var ENoGoZoneMap = new google.maps.Polygon({
            paths: NoGoZone,
            strokeColor: "#000000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#000000",
            fillOpacity: 0.55,
            draggable: false,
            geodesic: false,
            name: 'NoCoverage',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS6.push(ENoGoZoneMap);
    },

    DrawSubSector7: function (map) {
        var GNoGoZone = [
            { lng: 31.060581, lat: -29.868782 },
            { lng: 31.056933, lat: -29.873248 },
            { lng: 31.054659, lat: -29.875481 },
            { lng: 31.047535, lat: -29.88296 },
            { lng: 31.043458, lat: -29.884783 },
            { lng: 31.036677, lat: -29.887276 },
            { lng: 31.031699, lat: -29.889732 },
            { lng: 31.033759, lat: -29.891332 },
            { lng: 31.038008, lat: -29.89215 },
            { lng: 31.038094, lat: -29.894457 },
            { lng: 31.038609, lat: -29.895387 },
            { lng: 31.039295, lat: -29.896355 },
            { lng: 31.040175, lat: -29.897006 },
            { lng: 31.041741, lat: -29.898401 },
            { lng: 31.042557, lat: -29.899666 },
            { lng: 31.047707, lat: -29.89349 },
            { lng: 31.052771, lat: -29.888132 },
            { lng: 31.062641, lat: -29.875481 },
            { lng: 31.060581, lat: -29.868782 },
        ];
        var GNoGoZoneMap = new google.maps.Polygon({
            paths: GNoGoZone,
            strokeColor: "#000000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#000000",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'NoCoverage',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS7.push(GNoGoZoneMap);

        var G2 = [
            { lng: 31.016679, lat: -29.90147 },
            { lng: 31.020327, lat: -29.905107 },
            { lng: 31.022558, lat: -29.90705 },
            { lng: 31.02421, lat: -29.905804 },
            { lng: 31.025391, lat: -29.906939 },
            { lng: 31.027021, lat: -29.908501 },
            { lng: 31.028674, lat: -29.907218 },
            { lng: 31.031367, lat: -29.90958 },
            { lng: 31.032321, lat: -29.908836 },
            { lng: 31.035883, lat: -29.908148 },
            { lng: 31.040475, lat: -29.902679 },
            { lng: 31.042385, lat: -29.899629 },
            { lng: 31.04157, lat: -29.898364 },
            { lng: 31.040003, lat: -29.896968 },
            { lng: 31.039124, lat: -29.896317 },
            { lng: 31.037922, lat: -29.89442 },
            { lng: 31.037836, lat: -29.892113 },
            { lng: 31.033587, lat: -29.891295 },
            { lng: 31.031699, lat: -29.889732 },
            { lng: 31.027236, lat: -29.891667 },
            { lng: 31.018503, lat: -29.89935 },
            { lng: 31.016679, lat: -29.90147 },
        ];
        var G2Map = new google.maps.Polygon({
            paths: G2,
            strokeColor: "#4186F0",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#4186F0",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'G2',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS7.push(G2Map);

        var G3 = [
            { lng: 31.02421, lat: -29.905804 },
            { lng: 31.022623, lat: -29.906892 },
            { lng: 31.02038, lat: -29.905297 },
            { lng: 31.016679, lat: -29.90147 },
            { lng: 31.013138, lat: -29.905879 },
            { lng: 31.011529, lat: -29.907553 },
            { lng: 31.00934, lat: -29.909189 },
            { lng: 31.011808, lat: -29.911459 },
            { lng: 31.01316, lat: -29.912854 },
            { lng: 31.014297, lat: -29.914527 },
            { lng: 31.011916, lat: -29.916732 },
            { lng: 31.009127, lat: -29.919452 },
            { lng: 31.010232, lat: -29.920403 },
            { lng: 31.011653, lat: -29.921948 },
            { lng: 31.01394, lat: -29.92464 },
            { lng: 31.014295, lat: -29.925027 },
            { lng: 31.01495, lat: -29.92571 },
            { lng: 31.016029, lat: -29.92662 },
            { lng: 31.01685, lat: -29.927416 },
            { lng: 31.021335, lat: -29.922469 },
            { lng: 31.029382, lat: -29.915011 },
            { lng: 31.035883, lat: -29.908148 },
            { lng: 31.032321, lat: -29.908836 },
            { lng: 31.031367, lat: -29.90958 },
            { lng: 31.028674, lat: -29.907218 },
            { lng: 31.027021, lat: -29.908501 },
            { lng: 31.02421, lat: -29.905804 },
        ];
        var G3Map = new google.maps.Polygon({
            paths: G3,
            strokeColor: "#DB4436",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#DB4436",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'G3',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS7.push(G3Map);

        var G4 = [
            { lng: 30.974493, lat: -29.924217 },
            { lng: 30.971403, lat: -29.928643 },
            { lng: 30.96767, lat: -29.93292 },
            { lng: 30.962391, lat: -29.938722 },
            { lng: 30.954752, lat: -29.946457 },
            { lng: 30.950804, lat: -29.943259 },
            { lng: 30.948186, lat: -29.948018 },
            { lng: 30.946426, lat: -29.955306 },
            { lng: 30.963205, lat: -29.961033 },
            { lng: 30.975008, lat: -29.973152 },
            { lng: 30.992861, lat: -29.95421 },
            { lng: 30.988355, lat: -29.952573 },
            { lng: 30.990801, lat: -29.949022 },
            { lng: 30.990565, lat: -29.938536 },
            { lng: 30.983656, lat: -29.932846 },
            { lng: 30.989771, lat: -29.924514 },
            { lng: 30.989213, lat: -29.923957 },
            { lng: 30.986981, lat: -29.923957 },
            { lng: 30.984149, lat: -29.924998 },
            { lng: 30.981402, lat: -29.924812 },
            { lng: 30.979943, lat: -29.922543 },
            { lng: 30.974493, lat: -29.924217 },
        ];
        var G4Map = new google.maps.Polygon({
            paths: G4,
            strokeColor: "#009D57",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#009D57",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'G4',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS7.push(G4Map);

        var G5 = [
            { lng: 30.946609, lat: -29.955102 },
            { lng: 30.937125, lat: -29.966739 },
            { lng: 30.92631, lat: -29.969119 },
            { lng: 30.918714, lat: -29.977334 },
            { lng: 30.914895, lat: -29.982948 },
            { lng: 30.913908, lat: -29.982948 },
            { lng: 30.911504, lat: -29.984397 },
            { lng: 30.908543, lat: -29.986553 },
            { lng: 30.910217, lat: -29.99566 },
            { lng: 30.907256, lat: -29.998187 },
            { lng: 30.904681, lat: -30.001755 },
            { lng: 30.90511, lat: -30.00391 },
            { lng: 30.907986, lat: -30.009039 },
            { lng: 30.909273, lat: -30.011232 },
            { lng: 30.912706, lat: -30.008779 },
            { lng: 30.914337, lat: -30.007292 },
            { lng: 30.91644, lat: -30.004988 },
            { lng: 30.91983, lat: -30.004951 },
            { lng: 30.923349, lat: -30.003799 },
            { lng: 30.924765, lat: -30.003948 },
            { lng: 30.92601, lat: -30.00759 },
            { lng: 30.931846, lat: -30.007218 },
            { lng: 30.936953, lat: -30.008667 },
            { lng: 30.941545, lat: -30.005174 },
            { lng: 30.943262, lat: -30.004654 },
            { lng: 30.943348, lat: -30.003204 },
            { lng: 30.947296, lat: -29.999897 },
            { lng: 30.949828, lat: -29.997184 },
            { lng: 30.955965, lat: -29.991051 },
            { lng: 30.953733, lat: -29.989007 },
            { lng: 30.938905, lat: -29.979528 },
            { lng: 30.941072, lat: -29.974064 },
            { lng: 30.950836, lat: -29.956886 },
            { lng: 30.946609, lat: -29.955102 },
        ];
        var G5Map = new google.maps.Polygon({
            paths: G5,
            strokeColor: "#7C3592",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#7C3592",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'G5',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS7.push(G5Map);

        var G6 = [
            { lng: 30.876925, lat: -30.025668 },
            { lng: 30.876968, lat: -30.026021 },
            { lng: 30.882719, lat: -30.029105 },
            { lng: 30.885444, lat: -30.030108 },
            { lng: 30.891388, lat: -30.034808 },
            { lng: 30.901172, lat: -30.03856 },
            { lng: 30.910485, lat: -30.030814 },
            { lng: 30.927522, lat: -30.0161 },
            { lng: 30.934539, lat: -30.011083 },
            { lng: 30.93677, lat: -30.008872 },
            { lng: 30.931664, lat: -30.007423 },
            { lng: 30.925827, lat: -30.007794 },
            { lng: 30.924582, lat: -30.004152 },
            { lng: 30.923166, lat: -30.004004 },
            { lng: 30.91984, lat: -30.005174 },
            { lng: 30.916257, lat: -30.005193 },
            { lng: 30.914905, lat: -30.006735 },
            { lng: 30.912523, lat: -30.008983 },
            { lng: 30.90909, lat: -30.011436 },
            { lng: 30.907652, lat: -30.009076 },
            { lng: 30.907159, lat: -30.009299 },
            { lng: 30.903447, lat: -30.010154 },
            { lng: 30.901194, lat: -30.010656 },
            { lng: 30.900142, lat: -30.011956 },
            { lng: 30.898747, lat: -30.012867 },
            { lng: 30.895636, lat: -30.012997 },
            { lng: 30.893104, lat: -30.015654 },
            { lng: 30.892224, lat: -30.0174 },
            { lng: 30.890722, lat: -30.018812 },
            { lng: 30.888984, lat: -30.019277 },
            { lng: 30.887246, lat: -30.019481 },
            { lng: 30.88628, lat: -30.020262 },
            { lng: 30.885036, lat: -30.021952 },
            { lng: 30.883834, lat: -30.022268 },
            { lng: 30.882161, lat: -30.021674 },
            { lng: 30.880873, lat: -30.021785 },
            { lng: 30.880101, lat: -30.022417 },
            { lng: 30.879028, lat: -30.024256 },
            { lng: 30.876925, lat: -30.025668 },
        ];
        var G6Map = new google.maps.Polygon({
            paths: G6,
            strokeColor: "#009D57",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#009D57",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'G6(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS7.push(G6Map);

        var G7 = [
            { lng: 30.876925, lat: -30.025668 },
            { lng: 30.877032, lat: -30.03191 },
            { lng: 30.876818, lat: -30.03544 },
            { lng: 30.871453, lat: -30.037706 },
            { lng: 30.867891, lat: -30.040641 },
            { lng: 30.866561, lat: -30.045136 },
            { lng: 30.862656, lat: -30.049296 },
            { lng: 30.86729, lat: -30.052194 },
            { lng: 30.870981, lat: -30.05264 },
            { lng: 30.875058, lat: -30.053494 },
            { lng: 30.877418, lat: -30.054683 },
            { lng: 30.877805, lat: -30.058026 },
            { lng: 30.879221, lat: -30.059363 },
            { lng: 30.880294, lat: -30.059586 },
            { lng: 30.882096, lat: -30.058694 },
            { lng: 30.883942, lat: -30.058954 },
            { lng: 30.889349, lat: -30.052231 },
            { lng: 30.891495, lat: -30.051711 },
            { lng: 30.891409, lat: -30.049482 },
            { lng: 30.901172, lat: -30.03856 },
            { lng: 30.891388, lat: -30.034808 },
            { lng: 30.885444, lat: -30.030108 },
            { lng: 30.882719, lat: -30.029105 },
            { lng: 30.87759, lat: -30.026374 },
            { lng: 30.876925, lat: -30.025668 },
        ];
        var G7Map = new google.maps.Polygon({
            paths: G7,
            strokeColor: "#F4EB37",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F4EB37",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'G7(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS7.push(G7Map);

        var G8 = [
            { lng: 30.852227, lat: -30.066754 },
            { lng: 30.850832, lat: -30.06772 },
            { lng: 30.855832, lat: -30.071118 },
            { lng: 30.857999, lat: -30.072659 },
            { lng: 30.865424, lat: -30.075148 },
            { lng: 30.870445, lat: -30.075259 },
            { lng: 30.872161, lat: -30.075723 },
            { lng: 30.872805, lat: -30.078638 },
            { lng: 30.87347, lat: -30.075723 },
            { lng: 30.875638, lat: -30.071861 },
            { lng: 30.879178, lat: -30.067627 },
            { lng: 30.882311, lat: -30.063541 },
            { lng: 30.883942, lat: -30.058954 },
            { lng: 30.882289, lat: -30.05862 },
            { lng: 30.880294, lat: -30.059586 },
            { lng: 30.879221, lat: -30.059363 },
            { lng: 30.877826, lat: -30.058156 },
            { lng: 30.87759, lat: -30.056651 },
            { lng: 30.877418, lat: -30.054683 },
            { lng: 30.875058, lat: -30.053494 },
            { lng: 30.87214, lat: -30.052788 },
            { lng: 30.86729, lat: -30.052194 },
            { lng: 30.862656, lat: -30.049296 },
            { lng: 30.862527, lat: -30.050745 },
            { lng: 30.861025, lat: -30.053383 },
            { lng: 30.857549, lat: -30.057988 },
            { lng: 30.854416, lat: -30.062966 },
            { lng: 30.852227, lat: -30.066754 },
        ];
        var G8Map = new google.maps.Polygon({
            paths: G8,
            strokeColor: "#A61B4A",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#A61B4A",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'G8(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS7.push(G8Map);

        var G9 = [
            { lng: 30.850832, lat: -30.06772 },
            { lng: 30.849094, lat: -30.068797 },
            { lng: 30.847549, lat: -30.070988 },
            { lng: 30.843687, lat: -30.072176 },
            { lng: 30.839224, lat: -30.073662 },
            { lng: 30.836906, lat: -30.075482 },
            { lng: 30.836005, lat: -30.077747 },
            { lng: 30.838323, lat: -30.080161 },
            { lng: 30.841713, lat: -30.081089 },
            { lng: 30.828624, lat: -30.085397 },
            { lng: 30.825877, lat: -30.088182 },
            { lng: 30.82253, lat: -30.09119 },
            { lng: 30.820427, lat: -30.093343 },
            { lng: 30.819869, lat: -30.095497 },
            { lng: 30.821843, lat: -30.096945 },
            { lng: 30.824847, lat: -30.097985 },
            { lng: 30.828366, lat: -30.099581 },
            { lng: 30.83343, lat: -30.100695 },
            { lng: 30.837851, lat: -30.099098 },
            { lng: 30.841412, lat: -30.095014 },
            { lng: 30.844974, lat: -30.092749 },
            { lng: 30.846348, lat: -30.095163 },
            { lng: 30.849609, lat: -30.0952 },
            { lng: 30.851884, lat: -30.096611 },
            { lng: 30.852485, lat: -30.097873 },
            { lng: 30.851111, lat: -30.101734 },
            { lng: 30.84712, lat: -30.107229 },
            { lng: 30.843387, lat: -30.112464 },
            { lng: 30.834718, lat: -30.124937 },
            { lng: 30.833816, lat: -30.128166 },
            { lng: 30.845146, lat: -30.13481 },
            { lng: 30.847979, lat: -30.132323 },
            { lng: 30.847292, lat: -30.127832 },
            { lng: 30.850081, lat: -30.121262 },
            { lng: 30.853472, lat: -30.113949 },
            { lng: 30.85815, lat: -30.104296 },
            { lng: 30.862098, lat: -30.09895 },
            { lng: 30.863728, lat: -30.095794 },
            { lng: 30.863171, lat: -30.093937 },
            { lng: 30.868192, lat: -30.085806 },
            { lng: 30.872805, lat: -30.078638 },
            { lng: 30.872161, lat: -30.075723 },
            { lng: 30.870445, lat: -30.075259 },
            { lng: 30.865424, lat: -30.075148 },
            { lng: 30.857999, lat: -30.072659 },
            { lng: 30.850832, lat: -30.06772 },
        ];
        var G9Map = new google.maps.Polygon({
            paths: G9,
            strokeColor: "#F4B400",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F4B400",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'G9(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS7.push(G9Map);

        var G11OT = [
            { lng: 30.990792, lat: -29.948687 },
            { lng: 30.988355, lat: -29.952573 },
            { lng: 30.992571, lat: -29.95407 },
            { lng: 31.001765, lat: -29.944188 },
            { lng: 31.004598, lat: -29.941548 },
            { lng: 31.007247, lat: -29.939726 },
            { lng: 31.009468, lat: -29.936417 },
            { lng: 31.010436, lat: -29.935115 },
            { lng: 31.012858, lat: -29.93293 },
            { lng: 31.014164, lat: -29.93067 },
            { lng: 31.014889, lat: -29.929014 },
            { lng: 31.01685, lat: -29.927416 },
            { lng: 31.009127, lat: -29.919452 },
            { lng: 31.001979, lat: -29.926112 },
            { lng: 31.002, lat: -29.924402 },
            { lng: 31.002504, lat: -29.922487 },
            { lng: 31.001813, lat: -29.92192 },
            { lng: 30.998591, lat: -29.923366 },
            { lng: 30.997861, lat: -29.924072 },
            { lng: 30.99683, lat: -29.923624 },
            { lng: 30.995821, lat: -29.923121 },
            { lng: 30.995558, lat: -29.922377 },
            { lng: 30.994973, lat: -29.921912 },
            { lng: 30.992275, lat: -29.922711 },
            { lng: 30.990003, lat: -29.924673 },
            { lng: 30.989704, lat: -29.924482 },
            { lng: 30.987794, lat: -29.927195 },
            { lng: 30.985928, lat: -29.929815 },
            { lng: 30.983656, lat: -29.932846 },
            { lng: 30.990565, lat: -29.938536 },
            { lng: 30.99065, lat: -29.943073 },
            { lng: 30.990792, lat: -29.948687 },
        ];

        var G11IN = [
            { lng: 30.990801, lat: -29.949022 },
            { lng: 30.994383, lat: -29.942961 },
            { lng: 30.990792, lat: -29.948687 },
            { lng: 30.990801, lat: -29.949022 },
        ];

        var G11Map = new google.maps.Polygon({
            paths: [G11OT, G11IN],
            strokeColor: "#F4EB37",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F4EB37",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'G11',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS7.push(G11Map);

        var G15 = [
            { lng: 31.011529, lat: -29.907553 },
            { lng: 31.000046, lat: -29.907657 },
            { lng: 30.9962022, lat: -29.9035941 },
            { lng: 30.9914141, lat: -29.8999585 },
            { lng: 30.9856822, lat: -29.8974388 },
            { lng: 30.9827698, lat: -29.8967756 },
            { lng: 30.9805439, lat: -29.900428 },
            { lng: 30.9780119, lat: -29.9018413 },
            { lng: 30.978125, lat: -29.907812 },
            { lng: 30.977755, lat: -29.915253 },
            { lng: 30.978141, lat: -29.915997 },
            { lng: 30.974493, lat: -29.924217 },
            { lng: 30.979943, lat: -29.922543 },
            { lng: 30.981402, lat: -29.924812 },
            { lng: 30.984149, lat: -29.924998 },
            { lng: 30.986981, lat: -29.923957 },
            { lng: 30.989213, lat: -29.923957 },
            { lng: 30.989771, lat: -29.924514 },
            { lng: 30.992275, lat: -29.922711 },
            { lng: 30.994973, lat: -29.921912 },
            { lng: 30.995558, lat: -29.922377 },
            { lng: 30.995821, lat: -29.923121 },
            { lng: 30.997861, lat: -29.924072 },
            { lng: 30.998591, lat: -29.923366 },
            { lng: 31.001813, lat: -29.92192 },
            { lng: 31.002504, lat: -29.922487 },
            { lng: 31.002, lat: -29.924402 },
            { lng: 31.001979, lat: -29.926112 },
            { lng: 31.004137, lat: -29.924124 },
            { lng: 31.009127, lat: -29.919452 },
            { lng: 31.014297, lat: -29.914527 },
            { lng: 31.013063, lat: -29.912714 },
            { lng: 31.011808, lat: -29.911459 },
            { lng: 31.00934, lat: -29.909189 },
            { lng: 31.011529, lat: -29.907553 },
        ];
        var G15Map = new google.maps.Polygon({
            paths: G15,
            strokeColor: "#93D7E8",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#93D7E8",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'G15',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS7.push(G15Map);
    },

    DrawSubSector8: function (map) {
        var N2 = [
            { lng: 31.2323714, lat: -29.5078994 },
            { lng: 31.2258164, lat: -29.5142203 },
            { lng: 31.2276348, lat: -29.5154108 },
            { lng: 31.2268167, lat: -29.5172664 },
            { lng: 31.2243892, lat: -29.5213441 },
            { lng: 31.2242604, lat: -29.5232113 },
            { lng: 31.2240029, lat: -29.5235474 },
            { lng: 31.2215138, lat: -29.5220536 },
            { lng: 31.2204409, lat: -29.5219789 },
            { lng: 31.2179518, lat: -29.5242942 },
            { lng: 31.2158061, lat: -29.5241448 },
            { lng: 31.2144757, lat: -29.5240328 },
            { lng: 31.212287, lat: -29.5257132 },
            { lng: 31.2134273, lat: -29.5266246 },
            { lng: 31.2130303, lat: -29.5276048 },
            { lng: 31.213084, lat: -29.5278476 },
            { lng: 31.2142749, lat: -29.5281183 },
            { lng: 31.2158788, lat: -29.5286084 },
            { lng: 31.2156053, lat: -29.5295793 },
            { lng: 31.2121345, lat: -29.5327672 },
            { lng: 31.2114156, lat: -29.5331406 },
            { lng: 31.2103428, lat: -29.5333646 },
            { lng: 31.2091113, lat: -29.5341709 },
            { lng: 31.207974, lat: -29.5346376 },
            { lng: 31.2072659, lat: -29.5357204 },
            { lng: 31.2059785, lat: -29.53811 },
            { lng: 31.2052274, lat: -29.5402196 },
            { lng: 31.2049485, lat: -29.5421424 },
            { lng: 31.2063003, lat: -29.5421984 },
            { lng: 31.2081457, lat: -29.5410037 },
            { lng: 31.2088323, lat: -29.5421798 },
            { lng: 31.2085319, lat: -29.5428145 },
            { lng: 31.2071157, lat: -29.5443826 },
            { lng: 31.2106562, lat: -29.5470053 },
            { lng: 31.2096102, lat: -29.5474627 },
            { lng: 31.2086338, lat: -29.5480927 },
            { lng: 31.2078399, lat: -29.5489327 },
            { lng: 31.2071157, lat: -29.5494134 },
            { lng: 31.2082208, lat: -29.5500854 },
            { lng: 31.2101627, lat: -29.5487274 },
            { lng: 31.2116486, lat: -29.5479107 },
            { lng: 31.2123943, lat: -29.5475467 },
            { lng: 31.2125284, lat: -29.5467813 },
            { lng: 31.2131882, lat: -29.5462213 },
            { lng: 31.2139875, lat: -29.54561 },
            { lng: 31.2148458, lat: -29.5451106 },
            { lng: 31.215613, lat: -29.5448633 },
            { lng: 31.2164283, lat: -29.5444666 },
            { lng: 31.2168682, lat: -29.5433605 },
            { lng: 31.2173886, lat: -29.5424598 },
            { lng: 31.2178177, lat: -29.541475 },
            { lng: 31.2203497, lat: -29.5401683 },
            { lng: 31.2233967, lat: -29.5366772 },
            { lng: 31.2236757, lat: -29.5351837 },
            { lng: 31.2251777, lat: -29.5319725 },
            { lng: 31.2269158, lat: -29.5273236 },
            { lng: 31.2360372, lat: -29.5074261 },
            { lng: 31.2549321, lat: -29.4880143 },
            { lng: 31.2580556, lat: -29.4844577 },
            { lng: 31.2477897, lat: -29.4825446 },
            { lng: 31.2447242, lat: -29.4897387 },
            { lng: 31.2432707, lat: -29.4945259 },
            { lng: 31.2424358, lat: -29.4963425 },
            { lng: 31.2407232, lat: -29.4959788 },
            { lng: 31.2369118, lat: -29.4953637 },
            { lng: 31.2361557, lat: -29.4989143 },
            { lng: 31.232884, lat: -29.4991426 },
            { lng: 31.2286359, lat: -29.5001648 },
            { lng: 31.2246454, lat: -29.5042868 },
            { lng: 31.2323714, lat: -29.5078994 },
        ];
        var N2Map = new google.maps.Polygon({
            paths: N2,
            strokeColor: "#4186F0",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#4186F0",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'N2',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS8.push(N2Map);

        var N1 = [
            { lng: 31.193018, lat: -29.5287752 },
            { lng: 31.1963869, lat: -29.5303435 },
            { lng: 31.1946703, lat: -29.534955 },
            { lng: 31.1962152, lat: -29.5355337 },
            { lng: 31.1986185, lat: -29.5368219 },
            { lng: 31.200378, lat: -29.5377367 },
            { lng: 31.2009788, lat: -29.5376993 },
            { lng: 31.2049056, lat: -29.5420118 },
            { lng: 31.2052274, lat: -29.5402196 },
            { lng: 31.2059785, lat: -29.53811 },
            { lng: 31.2073196, lat: -29.5354403 },
            { lng: 31.207974, lat: -29.5346376 },
            { lng: 31.2091113, lat: -29.5341709 },
            { lng: 31.2103428, lat: -29.5333646 },
            { lng: 31.2114156, lat: -29.5331406 },
            { lng: 31.2121345, lat: -29.5327672 },
            { lng: 31.2143768, lat: -29.5307321 },
            { lng: 31.2150795, lat: -29.5300787 },
            { lng: 31.2156053, lat: -29.5295793 },
            { lng: 31.2158788, lat: -29.5286084 },
            { lng: 31.2142749, lat: -29.5281183 },
            { lng: 31.213084, lat: -29.5278476 },
            { lng: 31.2130303, lat: -29.5276048 },
            { lng: 31.2134273, lat: -29.5266246 },
            { lng: 31.212287, lat: -29.5257132 },
            { lng: 31.2114287, lat: -29.5256385 },
            { lng: 31.2108953, lat: -29.5245615 },
            { lng: 31.2099267, lat: -29.5208213 },
            { lng: 31.2091702, lat: -29.5191828 },
            { lng: 31.2053239, lat: -29.5180298 },
            { lng: 31.2041116, lat: -29.5143046 },
            { lng: 31.2065793, lat: -29.5106259 },
            { lng: 31.2049056, lat: -29.5102151 },
            { lng: 31.1988307, lat: -29.5175968 },
            { lng: 31.1954642, lat: -29.5227258 },
            { lng: 31.193018, lat: -29.5287752 },
        ];
        var N1Map = new google.maps.Polygon({
            paths: N1,
            strokeColor: "#7C3592",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#7C3592",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'N1',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS8.push(N1Map);
    },

    DrawSubSector9: function (map) {
        var C14 = [
            { lng: 30.895244, lat: -29.818636 },
            { lng: 30.892627, lat: -29.820721 },
            { lng: 30.889059, lat: -29.821154 },
            { lng: 30.888995, lat: -29.821623 },
            { lng: 30.888652, lat: -29.822722 },
            { lng: 30.888094, lat: -29.823094 },
            { lng: 30.887766, lat: -29.823602 },
            { lng: 30.887568, lat: -29.82396 },
            { lng: 30.887294, lat: -29.824579 },
            { lng: 30.887128, lat: -29.825198 },
            { lng: 30.88724, lat: -29.825845 },
            { lng: 30.887203, lat: -29.826101 },
            { lng: 30.887171, lat: -29.826287 },
            { lng: 30.886881, lat: -29.826818 },
            { lng: 30.886345, lat: -29.827283 },
            { lng: 30.885969, lat: -29.827991 },
            { lng: 30.885953, lat: -29.829102 },
            { lng: 30.885937, lat: -29.829787 },
            { lng: 30.88724, lat: -29.829865 },
            { lng: 30.88797, lat: -29.830368 },
            { lng: 30.88812, lat: -29.83142 },
            { lng: 30.889032, lat: -29.832515 },
            { lng: 30.889521, lat: -29.83238 },
            { lng: 30.891924, lat: -29.829848 },
            { lng: 30.893313, lat: -29.829359 },
            { lng: 30.893785, lat: -29.829168 },
            { lng: 30.894064, lat: -29.829056 },
            { lng: 30.894332, lat: -29.828982 },
            { lng: 30.894611, lat: -29.828889 },
            { lng: 30.894719, lat: -29.828814 },
            { lng: 30.894964, lat: -29.828753 },
            { lng: 30.895136, lat: -29.828591 },
            { lng: 30.894342, lat: -29.828241 },
            { lng: 30.894805, lat: -29.827635 },
            { lng: 30.894987, lat: -29.827358 },
            { lng: 30.895094, lat: -29.827275 },
            { lng: 30.895277, lat: -29.827312 },
            { lng: 30.89534, lat: -29.82705 },
            { lng: 30.89577, lat: -29.827219 },
            { lng: 30.895807, lat: -29.826739 },
            { lng: 30.895833, lat: -29.826035 },
            { lng: 30.896318, lat: -29.826278 },
            { lng: 30.896575, lat: -29.825795 },
            { lng: 30.89667, lat: -29.825253 },
            { lng: 30.896801, lat: -29.824865 },
            { lng: 30.897053, lat: -29.824978 },
            { lng: 30.898163, lat: -29.824216 },
            { lng: 30.898463, lat: -29.824557 },
            { lng: 30.898624, lat: -29.824463 },
            { lng: 30.8993, lat: -29.824529 },
            { lng: 30.899492, lat: -29.824658 },
            { lng: 30.900105, lat: -29.824752 },
            { lng: 30.900593, lat: -29.824016 },
            { lng: 30.900936, lat: -29.822967 },
            { lng: 30.901364, lat: -29.823586 },
            { lng: 30.90177, lat: -29.823963 },
            { lng: 30.902608, lat: -29.824917 },
            { lng: 30.904259, lat: -29.824354 },
            { lng: 30.905356, lat: -29.8231 },
            { lng: 30.906547, lat: -29.822085 },
            { lng: 30.908511, lat: -29.821852 },
            { lng: 30.908798, lat: -29.821567 },
            { lng: 30.908769, lat: -29.821476 },
            { lng: 30.909171, lat: -29.821205 },
            { lng: 30.910673, lat: -29.820312 },
            { lng: 30.911231, lat: -29.819307 },
            { lng: 30.910839, lat: -29.819272 },
            { lng: 30.910275, lat: -29.819236 },
            { lng: 30.909353, lat: -29.819106 },
            { lng: 30.908687, lat: -29.818985 },
            { lng: 30.907798, lat: -29.818897 },
            { lng: 30.906242, lat: -29.818398 },
            { lng: 30.905448, lat: -29.818203 },
            { lng: 30.903999, lat: -29.817886 },
            { lng: 30.902926, lat: -29.817608 },
            { lng: 30.901446, lat: -29.81737 },
            { lng: 30.898785, lat: -29.817705 },
            { lng: 30.897433, lat: -29.818082 },
            { lng: 30.895244, lat: -29.818636 },
        ];
        var C14Map = new google.maps.Polygon({
            paths: C14,
            strokeColor: "#4186F0",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#4186F0",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'C14(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS9.push(C14Map);

        var C15 = [
            { lng: 30.900088, lat: -29.824738 },
            { lng: 30.899476, lat: -29.824644 },
            { lng: 30.8993, lat: -29.824529 },
            { lng: 30.898624, lat: -29.824463 },
            { lng: 30.898463, lat: -29.824557 },
            { lng: 30.898163, lat: -29.824216 },
            { lng: 30.897053, lat: -29.824978 },
            { lng: 30.896801, lat: -29.824865 },
            { lng: 30.89667, lat: -29.825253 },
            { lng: 30.896575, lat: -29.825795 },
            { lng: 30.896318, lat: -29.826278 },
            { lng: 30.895833, lat: -29.826035 },
            { lng: 30.89577, lat: -29.827219 },
            { lng: 30.89534, lat: -29.82705 },
            { lng: 30.895277, lat: -29.827312 },
            { lng: 30.895094, lat: -29.827275 },
            { lng: 30.894987, lat: -29.827358 },
            { lng: 30.894825, lat: -29.827599 },
            { lng: 30.894342, lat: -29.828241 },
            { lng: 30.895136, lat: -29.828591 },
            { lng: 30.894964, lat: -29.828753 },
            { lng: 30.894719, lat: -29.828814 },
            { lng: 30.894611, lat: -29.828889 },
            { lng: 30.894064, lat: -29.829056 },
            { lng: 30.893241, lat: -29.829392 },
            { lng: 30.891881, lat: -29.829867 },
            { lng: 30.895679, lat: -29.831319 },
            { lng: 30.898682, lat: -29.834291 },
            { lng: 30.899272, lat: -29.835259 },
            { lng: 30.900282, lat: -29.835743 },
            { lng: 30.905528, lat: -29.836097 },
            { lng: 30.908168, lat: -29.835897 },
            { lng: 30.912062, lat: -29.834213 },
            { lng: 30.912878, lat: -29.832798 },
            { lng: 30.912748, lat: -29.831644 },
            { lng: 30.910882, lat: -29.831737 },
            { lng: 30.909562, lat: -29.831579 },
            { lng: 30.907684, lat: -29.83304 },
            { lng: 30.906826, lat: -29.832854 },
            { lng: 30.906826, lat: -29.831867 },
            { lng: 30.905882, lat: -29.829727 },
            { lng: 30.904638, lat: -29.82928 },
            { lng: 30.904488, lat: -29.829038 },
            { lng: 30.902706, lat: -29.829298 },
            { lng: 30.90174, lat: -29.828852 },
            { lng: 30.901332, lat: -29.827511 },
            { lng: 30.900302, lat: -29.825929 },
            { lng: 30.900088, lat: -29.824738 },
        ];
        var C15Map = new google.maps.Polygon({
            paths: C15,
            strokeColor: "#F8971B",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F8971B",
            fillOpacity: 0.35,
            draggable: false,
            geodesic: false,
            name: 'C15(SP)',
            map: map
        });
        CrmJs.Sales.Map.polygonsSS9.push(C15Map);
    },

    SetSector: function (marker) {
        var sectorFound = false;
        var blueSubSector = CrmJs.Sales.Map.IsValidValue(parent.Xrm.Page.getAttribute("tct_territory")) ? parent.Xrm.Page.getAttribute("tct_territory") : parent.Xrm.Page.getAttribute("tct_sector");

        if (CrmJs.Sales.Map.IsValidValue(blueSubSector)) {
            for (let i = 0; i < CrmJs.Sales.Map.polygons.length; i++) {
                var inSector = google.maps.geometry.poly.containsLocation(marker.getPosition(), CrmJs.Sales.Map.polygons[i]) ? true : false;

                if (inSector) {
                    sectorFound = true;

                    switch (CrmJs.Sales.Map.polygons[i].name) {
                        case "S1": {
                            CrmJs.Sales.Map.SetSubSector(marker, CrmJs.Sales.Map.polygonsSS, blueSubSector);
                            break;
                        } break;
                        case "S2": {
                            CrmJs.Sales.Map.SetSubSector(marker, CrmJs.Sales.Map.polygonsSS2, blueSubSector);
                            break;
                        } break;
                        case "S3": {
                            CrmJs.Sales.Map.SetSubSector(marker, CrmJs.Sales.Map.polygonsSS3, blueSubSector);
                            break;
                        } break;
                        case "S4": {
                            CrmJs.Sales.Map.SetSubSector(marker, CrmJs.Sales.Map.polygonsSS4, blueSubSector);
                            break;
                        } break;
                        case "S5": {
                            CrmJs.Sales.Map.SetSubSector(marker, CrmJs.Sales.Map.polygonsSS5, blueSubSector);
                            break;
                        } break;
                        case "S6": {
                            CrmJs.Sales.Map.SetSubSector(marker, CrmJs.Sales.Map.polygonsSS6, blueSubSector);
                            break;
                        } break;
                        case "S7": {
                            CrmJs.Sales.Map.SetSubSector(marker, CrmJs.Sales.Map.polygonsSS7, blueSubSector);
                            break;
                        } break;
                        case "S8": {
                            CrmJs.Sales.Map.SetSubSector(marker, CrmJs.Sales.Map.polygonsSS8, blueSubSector);
                            break;
                        } break
                        case "S9": {
                            CrmJs.Sales.Map.SetSubSector(marker, CrmJs.Sales.Map.polygonsSS9, blueSubSector);
                            break;
                        } break;
                    }
                }
            }

            //Set to NoCoverage
            if (!sectorFound) {
                CrmJs.Sales.Map.SetTerritory("NoCoverage", blueSubSector);
            }
        }
    },

    SetSubSector: function (marker, polygonsSub, blueSubSector) {
        for (let i = 0; i < polygonsSub.length; i++) {
            var inSubSector = google.maps.geometry.poly.containsLocation(marker.getPosition(), polygonsSub[i]) ? true : false;

            if (inSubSector) {
                //alert(polygonsSub[i].name);
                CrmJs.Sales.Map.SetTerritory(polygonsSub[i].name, blueSubSector);
            }
        }
    },

    SetTerritory: function (territoryName, blueSubSector) {
        parent.Xrm.WebApi.online.retrieveMultipleRecords("territory", "?$select=name,_parentterritoryid_value,territoryid&$filter=contains(name, '" + territoryName + "') and  _parentterritoryid_value ne null&$top=1").then(
            function success(results) {
                for (var i = 0; i < results.entities.length; i++) {
                    var name = results.entities[i]["name"];
                    var logicalname = results.entities[i]["_parentterritoryid_value@Microsoft.Dynamics.CRM.lookuplogicalname"];
                    var territoryid = results.entities[i]["territoryid"];

                    CrmJs.Sales.Map.SetLookupValue(blueSubSector, territoryid, name, logicalname);
                    var searchAddress = parent.Xrm.Page.getAttribute("tct_searchaddress");
                    if (CrmJs.Sales.Map.IsValidValue(searchAddress))
                        searchAddress.setValue(false);
                }
            },
            function (error) {
                Xrm.Utility.alertDialog(error.message);
            }
        );
    },

    CheckMapSetting: function (executionContext) {
        if (CrmJs.Sales.Map.IsValidValue(executionContext)) {
            var leadContext = executionContext.getFormContext();
            var searchAddress = leadContext.getAttribute("tct_searchaddress");

            if (CrmJs.Sales.Map.IsValidValue(searchAddress)) {
                if (searchAddress.getValue() == false) {
                    leadContext.getControl("WebResource_GoogleMap").setDisabled(false);
                    leadContext.getControl("WebResource_GoogleMap").setVisible(false);

                    if (leadContext.getControl("mapcontrol") != null) {
                        leadContext.getControl("mapcontrol").setDisabled(true);
                        leadContext.getControl("mapcontrol").setVisible(true);
                    }

                } else {
                    leadContext.getControl("WebResource_GoogleMap").setDisabled(true);
                    leadContext.getControl("WebResource_GoogleMap").setVisible(true);

                    if (leadContext.getControl("mapcontrol") != null) {
                        leadContext.getControl("mapcontrol").setDisabled(false);
                        leadContext.getControl("mapcontrol").setVisible(false);
                    }
                }
            }
        }
    },

    CheckAddress: function (executionContext) {
        if (CrmJs.Sales.Map.IsValidValue(executionContext)) {
            var leadContext = executionContext.getFormContext();
            var streetAddress = leadContext.getAttribute("address1_line1");

            if (CrmJs.Sales.Map.IsValidValue(streetAddress)
                && CrmJs.Sales.Map.IsValidValue(streetAddress.getValue())) {
                leadContext.getControl("address1_line1").setDisabled(true);
            } else {
                leadContext.getControl("address1_line1").setDisabled(false);
            }
        }
    },

    SetAddressFields: function (streetNumber, streetName, premise, address2, postalTown, stateorprovince, city, suburb, country, postalCode, place) {
        if (CrmJs.Sales.Map.IsValidValue(parent)
            && CrmJs.Sales.Map.IsValidValue(parent.Xrm)) {

            switch (parent.Xrm.Page.data.entity.getEntityName()) {
                case "lead": {
                    CrmJs.Sales.Map.SetAddress1(streetNumber, streetName, premise, address2, postalTown, stateorprovince, city, suburb, country, postalCode, place);
                } break;
                case "opportunity": {
                    CrmJs.Sales.Map.SetOppAddress(streetNumber, streetName, premise, address2, postalTown, stateorprovince, city, suburb, country, postalCode, place);
                } break;
                case "quote": {
                    CrmJs.Sales.Map.SetBillAddress(streetNumber, streetName, premise, address2, postalTown, stateorprovince, city, suburb, country, postalCode, place);
                } break;
                case "account": {
                    CrmJs.Sales.Map.SetAddress1(streetNumber, streetName, premise, address2, postalTown, stateorprovince, city, suburb, country, postalCode, place);
                } break;
                case "contact": {
                    CrmJs.Sales.Map.SetAddress1(streetNumber, streetName, premise, address2, postalTown, stateorprovince, city, suburb, country, postalCode, place);
                } break;
                case "salesorder": {
                    CrmJs.Sales.Map.SetShipAddress(streetNumber, streetName, premise, address2, postalTown, stateorprovince, city, suburb, country, postalCode, place);
                } break;
            }
        }
    },

    SetAddress1: function (streetNumber, streetName, premise, address2, postalTown, stateorprovince, city, suburb, country, postalCode, place) {
        var xrmPage = parent.Xrm.Page;

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("address1_line1"))
            && CrmJs.Sales.Map.IsValidValue(streetNumber) && streetNumber.length > 0
            && CrmJs.Sales.Map.IsValidValue(streetName) && streetName.length > 0) {

            xrmPage.getAttribute("address1_line1").setValue(streetNumber[0].long_name + " " + streetName[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("address1_line1"))
            && CrmJs.Sales.Map.IsValidValue(streetNumber) && streetNumber.length > 0) {

            xrmPage.getAttribute("address1_line1").setValue(streetNumber[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("address1_line1"))
            && CrmJs.Sales.Map.IsValidValue(streetName) && streetName.length > 0) {

            xrmPage.getAttribute("address1_line1").setValue(streetName[0].long_name);
        } else {
            xrmPage.getAttribute("address1_line1").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("address1_line2"))
            && CrmJs.Sales.Map.IsValidValue(premise) && premise.length > 0) {

            xrmPage.getAttribute("address1_line2").setValue(premise[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("address1_line2"))
            && CrmJs.Sales.Map.IsValidValue(suburb) && suburb.length > 0) {

            xrmPage.getAttribute("address1_line2").setValue(suburb[0].long_name);
        } else {
            xrmPage.getAttribute("address1_line2").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("address1_line3"))
            && CrmJs.Sales.Map.IsValidValue(address2) && address2.length > 0
            && CrmJs.Sales.Map.IsValidValue(postalTown) && postalTown.length > 0) {

            xrmPage.getAttribute("address1_line3").setValue(address2[0].long_name + ", " + postalTown[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("address1_line3"))
            && CrmJs.Sales.Map.IsValidValue(address2) && address2.length > 0) {

            xrmPage.getAttribute("address1_line3").setValue(address2[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("address1_line3"))
            && CrmJs.Sales.Map.IsValidValue(postalTown) && postalTown.length > 0) {

            xrmPage.getAttribute("address1_line3").setValue(postalTown[0].long_name);
        } else {
            xrmPage.getAttribute("address1_line3").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("address1_stateorprovince"))
            && CrmJs.Sales.Map.IsValidValue(stateorprovince) && stateorprovince.length > 0) {

            xrmPage.getAttribute("address1_stateorprovince").setValue(stateorprovince[0].long_name);
        } else {
            xrmPage.getAttribute("address1_stateorprovince").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("address1_city"))
            && CrmJs.Sales.Map.IsValidValue(city) && city.length > 0) {

            xrmPage.getAttribute("address1_city").setValue(city[0].long_name);
        } else {
            xrmPage.getAttribute("address1_city").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("address1_postalcode"))
            && CrmJs.Sales.Map.IsValidValue(postalCode) && postalCode.length > 0) {

            xrmPage.getAttribute("address1_postalcode").setValue(postalCode[0].long_name);
        } else {
            xrmPage.getAttribute("address1_postalcode").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("address1_country"))
            && CrmJs.Sales.Map.IsValidValue(country) && country.length > 0) {

            xrmPage.getAttribute("address1_country").setValue(country[0].long_name);
        } else {
            xrmPage.getAttribute("address1_country").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("address1_latitude"))
            && CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("address1_longitude"))) {
            xrmPage.getAttribute("address1_latitude").setValue(place.geometry.location.lat());
            xrmPage.getAttribute("address1_longitude").setValue(place.geometry.location.lng());
            xrmPage.ui.clearFormNotification("geocode");
        }
    },

    SetOppAddress: function (streetNumber, streetName, premise, address2, postalTown, stateorprovince, city, suburb, country, postalCode, place) {
        var xrmPage = parent.Xrm.Page;

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("tct_streetaddress"))
            && CrmJs.Sales.Map.IsValidValue(streetNumber) && streetNumber.length > 0
            && CrmJs.Sales.Map.IsValidValue(streetName) && streetName.length > 0) {

            xrmPage.getAttribute("tct_streetaddress").setValue(streetNumber[0].long_name + " " + streetName[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("tct_streetaddress"))
            && CrmJs.Sales.Map.IsValidValue(streetNumber) && streetNumber.length > 0) {

            xrmPage.getAttribute("tct_streetaddress").setValue(streetNumber[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("tct_streetaddress"))
            && CrmJs.Sales.Map.IsValidValue(streetName) && streetName.length > 0) {

            xrmPage.getAttribute("tct_streetaddress").setValue(streetName[0].long_name);
        } else {
            xrmPage.getAttribute("tct_streetaddress").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("tct_suburb"))
            && CrmJs.Sales.Map.IsValidValue(premise) && premise.length > 0) {

            xrmPage.getAttribute("tct_suburb").setValue(premise[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("tct_suburb"))
            && CrmJs.Sales.Map.IsValidValue(suburb) && suburb.length > 0) {

            xrmPage.getAttribute("tct_suburb").setValue(suburb[0].long_name);
        } else {
            xrmPage.getAttribute("tct_suburb").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("tct_district"))
            && CrmJs.Sales.Map.IsValidValue(address2) && address2.length > 0
            && CrmJs.Sales.Map.IsValidValue(postalTown) && postalTown.length > 0) {

            xrmPage.getAttribute("tct_district").setValue(address2[0].long_name + ", " + postalTown[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("tct_district"))
            && CrmJs.Sales.Map.IsValidValue(address2) && address2.length > 0) {

            xrmPage.getAttribute("tct_district").setValue(address2[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("tct_district"))
            && CrmJs.Sales.Map.IsValidValue(postalTown) && postalTown.length > 0) {

            xrmPage.getAttribute("tct_district").setValue(postalTown[0].long_name);
        } else {
            xrmPage.getAttribute("tct_district").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("tct_province"))
            && CrmJs.Sales.Map.IsValidValue(stateorprovince) && stateorprovince.length > 0) {

            xrmPage.getAttribute("tct_province").setValue(stateorprovince[0].long_name);
        } else {
            xrmPage.getAttribute("tct_province").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("tct_city"))
            && CrmJs.Sales.Map.IsValidValue(city) && city.length > 0) {

            xrmPage.getAttribute("tct_city").setValue(city[0].long_name);
        } else {
            xrmPage.getAttribute("tct_city").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("tct_postalcode"))
            && CrmJs.Sales.Map.IsValidValue(postalCode) && postalCode.length > 0) {

            xrmPage.getAttribute("tct_postalcode").setValue(postalCode[0].long_name);
        } else {
            xrmPage.getAttribute("tct_postalcode").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("tct_country"))
            && CrmJs.Sales.Map.IsValidValue(country) && country.length > 0) {

            xrmPage.getAttribute("tct_country").setValue(country[0].long_name);
        } else {
            xrmPage.getAttribute("tct_country").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("tct_latitude"))
            && CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("tct_longitude"))) {
            xrmPage.getAttribute("tct_latitude").setValue(place.geometry.location.lat());
            xrmPage.getAttribute("tct_longitude").setValue(place.geometry.location.lng());
            //xrmPage.ui.clearFormNotification("geocode");
        }
    },

    SetBillAddress: function (streetNumber, streetName, premise, address2, postalTown, stateorprovince, city, suburb, country, postalCode, place) {
        var xrmPage = parent.Xrm.Page;

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("billto_line1"))
            && CrmJs.Sales.Map.IsValidValue(streetNumber) && streetNumber.length > 0
            && CrmJs.Sales.Map.IsValidValue(streetName) && streetName.length > 0) {

            xrmPage.getAttribute("billto_line1").setValue(streetNumber[0].long_name + " " + streetName[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("billto_line1"))
            && CrmJs.Sales.Map.IsValidValue(streetNumber) && streetNumber.length > 0) {

            xrmPage.getAttribute("billto_line1").setValue(streetNumber[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("billto_line1"))
            && CrmJs.Sales.Map.IsValidValue(streetName) && streetName.length > 0) {

            xrmPage.getAttribute("billto_line1").setValue(streetName[0].long_name);
        } else {
            xrmPage.getAttribute("billto_line1").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("billto_line2"))
            && CrmJs.Sales.Map.IsValidValue(premise) && premise.length > 0) {

            xrmPage.getAttribute("billto_line2").setValue(premise[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("billto_line2"))
            && CrmJs.Sales.Map.IsValidValue(suburb) && suburb.length > 0) {

            xrmPage.getAttribute("billto_line2").setValue(suburb[0].long_name);
        } else {
            xrmPage.getAttribute("billto_line2").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("billto_line3"))
            && CrmJs.Sales.Map.IsValidValue(address2) && address2.length > 0
            && CrmJs.Sales.Map.IsValidValue(postalTown) && postalTown.length > 0) {

            xrmPage.getAttribute("billto_line3").setValue(address2[0].long_name + ", " + postalTown[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("billto_line3"))
            && CrmJs.Sales.Map.IsValidValue(address2) && address2.length > 0) {

            xrmPage.getAttribute("billto_line3").setValue(address2[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("billto_line3"))
            && CrmJs.Sales.Map.IsValidValue(postalTown) && postalTown.length > 0) {

            xrmPage.getAttribute("billto_line3").setValue(postalTown[0].long_name);
        } else {
            xrmPage.getAttribute("billto_line3").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("billto_stateorprovince"))
            && CrmJs.Sales.Map.IsValidValue(stateorprovince) && stateorprovince.length > 0) {

            xrmPage.getAttribute("billto_stateorprovince").setValue(stateorprovince[0].long_name);
        } else {
            xrmPage.getAttribute("billto_stateorprovince").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("billto_city"))
            && CrmJs.Sales.Map.IsValidValue(city) && city.length > 0) {

            xrmPage.getAttribute("billto_city").setValue(city[0].long_name);
        } else {
            xrmPage.getAttribute("billto_city").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("billto_postalcode"))
            && CrmJs.Sales.Map.IsValidValue(postalCode) && postalCode.length > 0) {

            xrmPage.getAttribute("billto_postalcode").setValue(postalCode[0].long_name);
        } else {
            xrmPage.getAttribute("billto_postalcode").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("billto_country"))
            && CrmJs.Sales.Map.IsValidValue(country) && country.length > 0) {

            xrmPage.getAttribute("billto_country").setValue(country[0].long_name);
        } else {
            xrmPage.getAttribute("billto_country").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("tct_latitude"))
            && CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("tct_longitude"))) {
            xrmPage.getAttribute("tct_latitude").setValue(place.geometry.location.lat());
            xrmPage.getAttribute("tct_longitude").setValue(place.geometry.location.lng());
            //xrmPage.ui.clearFormNotification("geocode");
        }
    },

    SetShipAddress: function (streetNumber, streetName, premise, address2, postalTown, stateorprovince, city, suburb, country, postalCode, place) {
        var xrmPage = parent.Xrm.Page;

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("shipto_line1"))
            && CrmJs.Sales.Map.IsValidValue(streetNumber) && streetNumber.length > 0
            && CrmJs.Sales.Map.IsValidValue(streetName) && streetName.length > 0) {

            xrmPage.getAttribute("shipto_line1").setValue(streetNumber[0].long_name + " " + streetName[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("shipto_line1"))
            && CrmJs.Sales.Map.IsValidValue(streetNumber) && streetNumber.length > 0) {

            xrmPage.getAttribute("shipto_line1").setValue(streetNumber[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("shipto_line1"))
            && CrmJs.Sales.Map.IsValidValue(streetName) && streetName.length > 0) {

            xrmPage.getAttribute("shipto_line1").setValue(streetName[0].long_name);
        } else {
            xrmPage.getAttribute("shipto_line1").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("shipto_line2"))
            && CrmJs.Sales.Map.IsValidValue(premise) && premise.length > 0) {

            xrmPage.getAttribute("shipto_line2").setValue(premise[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("shipto_line2"))
            && CrmJs.Sales.Map.IsValidValue(suburb) && suburb.length > 0) {

            xrmPage.getAttribute("shipto_line2").setValue(suburb[0].long_name);
        } else {
            xrmPage.getAttribute("shipto_line2").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("shipto_line3"))
            && CrmJs.Sales.Map.IsValidValue(address2) && address2.length > 0
            && CrmJs.Sales.Map.IsValidValue(postalTown) && postalTown.length > 0) {

            xrmPage.getAttribute("shipto_line3").setValue(address2[0].long_name + ", " + postalTown[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("shipto_line3"))
            && CrmJs.Sales.Map.IsValidValue(address2) && address2.length > 0) {

            xrmPage.getAttribute("shipto_line3").setValue(address2[0].long_name);
        } else if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("shipto_line3"))
            && CrmJs.Sales.Map.IsValidValue(postalTown) && postalTown.length > 0) {

            xrmPage.getAttribute("shipto_line3").setValue(postalTown[0].long_name);
        } else {
            xrmPage.getAttribute("shipto_line3").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("shipto_stateorprovince"))
            && CrmJs.Sales.Map.IsValidValue(stateorprovince) && stateorprovince.length > 0) {

            xrmPage.getAttribute("shipto_stateorprovince").setValue(stateorprovince[0].long_name);
        } else {
            xrmPage.getAttribute("shipto_stateorprovince").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("shipto_city"))
            && CrmJs.Sales.Map.IsValidValue(city) && city.length > 0) {

            xrmPage.getAttribute("shipto_city").setValue(city[0].long_name);
        } else {
            xrmPage.getAttribute("shipto_city").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("shipto_postalcode"))
            && CrmJs.Sales.Map.IsValidValue(postalCode) && postalCode.length > 0) {

            xrmPage.getAttribute("shipto_postalcode").setValue(postalCode[0].long_name);
        } else {
            xrmPage.getAttribute("shipto_postalcode").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("shipto_country"))
            && CrmJs.Sales.Map.IsValidValue(country) && country.length > 0) {

            xrmPage.getAttribute("shipto_country").setValue(country[0].long_name);
        } else {
            xrmPage.getAttribute("shipto_country").setValue("");
        }

        if (CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("tct_latitude"))
            && CrmJs.Sales.Map.IsValidValue(xrmPage.getAttribute("tct_longitude"))) {
            xrmPage.getAttribute("tct_latitude").setValue(place.geometry.location.lat());
            xrmPage.getAttribute("tct_longitude").setValue(place.geometry.location.lng());
            //xrmPage.ui.clearFormNotification("geocode");
        }
    },

    SetLookupValue: function (lookupControl, id, name, entityType) {
        if (lookupControl != null)
            lookupControl.setValue([{ id: id.replace(/[{}]/g, ''), name: name, entityType: entityType }]);
    },

    IsValidValue: function (value) {
        if (value == null || value == "" || value == "undefined" || value == "null")
            return false;
        return true;
    },
}