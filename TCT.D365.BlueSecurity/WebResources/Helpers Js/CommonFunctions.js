﻿function OnLoad(executionContext) {
    var formContext = executionContext.getFormContext();
    formContext.getAttribute("customerid").addOnChange(GetAccountPurchaseOrderValue)

}


//******************************************************************** */
//CUSTOM FUNCTIONS are added below here. Below this point you add all types of functions you need. 
//******************************************************************** */
function GetAccountPurchaseOrderValue() {
    //var CustomerField = formContext.getAttribute("customerid").getValue();
    var CustomerField = Xrm.Page.getAttribute("customerid").getValue(); 
    if (CustomerField != null) {
        if (CustomerField[0].entityType == "account") {
            var CustomerGUID = CustomerField[0].id;
            CustomerGUID = CustomerGUID.replace("{", "");
            CustomerGUID = CustomerGUID.replace("}", "");

            Xrm.WebApi.online.retrieveRecord("account", CustomerGUID, "?$select=tct_purchaseorderrequired").then(
                function success(result) {
                    var purchaseorder = result["tct_purchaseorderrequired"];

                    if (purchaseorder == true) {
                        Xrm.Page.getControl("tct_purchaseorder").setVisible(true);
                        Xrm.Page.getAttribute("tct_purchaseorder").setRequiredLevel("required"); // Business Required
                    }
                },
                function (error) {
                    Xrm.Utility.alertDialog(error.message);
                }
            );


        }
    }
    else {
        Xrm.Page.getAttribute("tct_purchaseorder").setRequiredLevel("none"); //Not Required
        Xrm.Page.getAttribute("tct_purchaseorder").setValue(null); 
        Xrm.Page.getControl("tct_purchaseorder").setVisible(false);
    }

}