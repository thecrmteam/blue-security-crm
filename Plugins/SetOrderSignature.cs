﻿using Microsoft.Xrm.Sdk;
using System;


namespace TCT.D365.BlueSecurity.Plugins
{
    public class SetOrderSignature : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            var context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            var service = factory.CreateOrganizationService(context.UserId);


            Entity entity = (Entity)context.InputParameters["Target"];


            string signaturetext = string.Empty;
            string attributeToUpdate = string.Empty;
            //Entity OrderEntity = null;


            tracingService.Trace("Start");
            try
            {
                // OrderEntity = new Entity("salesorder",entity.Id);


                tracingService.Trace("Start");

                if (entity.Contains("tct_customersignature") && entity.GetAttributeValue<string>("tct_customersignature") != null)
                {
                    //to split base64 and remaining text
                    signaturetext = entity.GetAttributeValue<string>("tct_customersignature");

                    string[] substring = signaturetext.Split(',');
                    attributeToUpdate = substring[1];

                }
                Entity updateOrder = new Entity("salesorder");
                updateOrder.Id = entity.Id;
                updateOrder["tct_customersignaturetext"] = attributeToUpdate;
                service.Update(updateOrder);
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException("An error occurred in the SARPostUpdate plug-in.", ex);
            }
        }

    }


}