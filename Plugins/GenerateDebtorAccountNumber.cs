﻿using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using TCT.D365.BlueSecurity.Plugins.Manager;

namespace TCT.D365.BlueSecurity.Plugins
{
    public class GenerateDebtorAccountNumber : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            var context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            var service = factory.CreateOrganizationService(context.UserId);

            tracingService.Trace($"Line17");
            Entity entity = (Entity)context.InputParameters["Target"];
            Entity preEntity = (Entity)context.InputParameters["Target"];

            if (context.Depth > 1) return;

            if (context.MessageName.ToUpper() == "UPDATE") {
                preEntity = context.PreEntityImages["Image"]; ;
            }

            tracingService.Trace($"Line21");
            bool? isDebtor = AttributeManager.GetBoolValue(entity, preEntity, "tct_isdebtor");

            if (isDebtor != true) return;
            tracingService.Trace($"Line25");
            OptionSetValue accountType = AttributeManager.GetOptionSetValueValue(entity, preEntity, "tct_accounttype");
            string accountName = AttributeManager.GetStringValue(entity, preEntity, "name");
            string accountLastName = AttributeManager.GetStringValue(entity, preEntity, "tct_lastname");
            string customerAccountNumber = AttributeManager.GetStringValue(entity, preEntity, "accountnumber");

            tracingService.Trace($"Line31");
            string first3Letters = string.Empty;

            // Check if Account Number is empty


            // If Acc number has data copy from ex. acc number
            if (customerAccountNumber != string.Empty)
            {
                // Update debtor acc number

                entity.Attributes["tct_debtoraccountnumber"] = customerAccountNumber;
                service.Update(entity);
                tracingService.Trace($"Line44");
                return;
            }


            try
            {

                // 1 Get counter value
                tracingService.Trace($"Line53");
                string counterFetchXml = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
                                          "<entity name='tct_numbergenerator'>" +
                                            "<attribute name='tct_numbergeneratorid' />" +
                                            "<attribute name='tct_name' />" +
                                            "<attribute name='createdon' />" +
                                            "<attribute name='tct_autonumber' />" +
                                            "<order attribute='tct_name' descending='false' />" +
                                          "</entity>" +
                                        "</fetch>";

                EntityCollection resultCollection = service.RetrieveMultiple(new FetchExpression(counterFetchXml.ToString()));
                tracingService.Trace($"Line65");
                if (resultCollection.Entities.Count > 0)
                {
                    Entity counterEntity = resultCollection.Entities[0];


                    int accountNumber = int.Parse(counterEntity.Attributes["tct_autonumber"].ToString());
                    tracingService.Trace($"Line72");
                    // 2 Add zeros 
                    string calcAccountNumber = string.Empty;

                    string zeros = string.Empty;

                    switch (accountNumber.ToString().Length)
                    {
                        case 0:
                            zeros = "0000000";
                            break;

                        case 1:
                            zeros = "000000";
                            break;

                        case 2:
                            zeros = "00000";
                            break;

                        case 3:
                            zeros = "0000";
                            break;

                        case 4:
                            zeros = "000";
                            break;

                        case 5:
                            zeros = "00";
                            break;
                    }

                    // 3 Get Customer last name 3 letters (Account)
                    //Entity entityRecord = service.Retrieve(entity.LogicalName, entity.Id, new ColumnSet("tct_accounttype", "accountnumber", "name", "tct_lastname"));

                    // Commercial
                    tracingService.Trace($"Line109");
                    if (accountType.Value == 2)
                    {


                        tracingService.Trace($"Line114");
                        if (accountName.Length > 2)
                        {
                            first3Letters = accountName.Substring(0, 3);
                        }

                        else
                        {
                            first3Letters = accountName;
                        }

                        first3Letters = first3Letters.ToUpper();

                        calcAccountNumber = first3Letters + zeros + accountNumber;
                        tracingService.Trace($"calcAccountNumber: {calcAccountNumber}");

                        // Update Account
                        tracingService.Trace($"Line131");
                        entity.Attributes["tct_debtoraccountnumber"] = calcAccountNumber;
                        service.Update(entity);
                        tracingService.Trace($"Line132");
                    }

                    // Residential
                    else if (accountType.Value == 1)
                    {
                        // Get last name

                        //Entity contactEntity = service.Retrieve("contact", customerER.Id, new ColumnSet("lastname", "tct_accountnumber"));

                        //accountLastName = CustomerAccountRecord.GetAttributeValue<string>("tct_lastname");

                        tracingService.Trace($"Line146");


                        if (accountLastName.Contains("-"))
                        {
                            string[] pieces = accountLastName.Split(new char[] { '-' });
                            first3Letters = pieces[0].Substring(0, 3);
                        }
                        else if (accountLastName.Contains(" "))
                        {
                            string[] pieces = accountLastName.Split(new char[] { ' ' });
                            first3Letters = pieces[pieces.Length - 1].Substring(0, 3);
                        }
                        else
                        {
                            first3Letters = accountLastName.Substring(0, 3);
                        }

                        tracingService.Trace($"Line164");
                        first3Letters = first3Letters.ToUpper();

                        calcAccountNumber = first3Letters + zeros + accountNumber;

                        // Update Account

                        entity.Attributes["tct_debtoraccountnumber"] = calcAccountNumber;
                        service.Update(entity);
                        tracingService.Trace($"Line173");

                    }

                }
            }
            catch (Exception ex)
            {

                throw new InvalidPluginExecutionException("An error occurred in the GenerateDebtorAccountNumber plug-in.", ex);
            }
        }
    }
}
