﻿using Microsoft.Xrm.Sdk;
using System;
using System.IO;
using System.ServiceModel;
using System.Xml.Serialization;
using TCT.D365.BlueSecurity.Plugins.Linkserv;

namespace TCT.D365.BlueSecurity.Plugins.Manager
{
    public class LinkservWebServiceManager
    {
        public static string ValidateSouthAfricanAccountNumber(string _branch, string _account)
        {

            try
            {
                var binding = new BasicHttpBinding();
                binding.OpenTimeout = new TimeSpan(0, 2, 0);
                binding.CloseTimeout = new TimeSpan(0, 2, 0);
                binding.SendTimeout = new TimeSpan(0, 2, 0);
                binding.ReceiveTimeout = new TimeSpan(0, 2, 0);
                binding.Security.Mode = BasicHttpSecurityMode.Transport;
                var endPoint = new EndpointAddress("https://ebureau.mercantile.co.za/Soap/MEB_SOAP_BranchLookUp.php/");
                var request = new CdvPortTypeClient(binding, endPoint);

                var response = request.CdvEFT("CDV", "LCDV", "PasswordCDV!@#", _branch, _account);


                using (TextReader reader = new StringReader(response))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(result.cdv));
                    result.cdv resultTest = (result.cdv)serializer.Deserialize(reader);

                    var LinkservResponse = resultTest.resultField;
                    var stringLinkservResponse = LinkservResponse.ToString();

                    return stringLinkservResponse;
                }

            }

            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException("An error occurred in the LinkservWebServiceManager class.", ex);
            }

        }

        public class result
        {

            // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
            /// <remarks/>
            [System.SerializableAttribute()]
            [System.ComponentModel.DesignerCategoryAttribute("code")]
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
            [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
            public partial class cdv
            {

                public byte resultField;

                /// <remarks/>
                public byte result
                {
                    get
                    {
                        return this.resultField;
                    }
                    set
                    {
                        this.resultField = value;
                    }
                }
            }


        }
    }

    
}
