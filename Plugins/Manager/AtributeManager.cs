﻿using Microsoft.Xrm.Sdk;
using System;

namespace TCT.D365.BlueSecurity.Plugins.Manager
{
    public class AttributeManager
    {
        #region Entity Attribute & Value Getter
        public static string GetStringValue(Entity entity, Entity preEntity, string attributeName)
        {
            if (entity.Attributes.Contains(attributeName))
            {
                return (string)entity[attributeName];
            }
            if (preEntity.Attributes.Contains(attributeName))
            {
                return (string)preEntity[attributeName];
            }
            return string.Empty;
        }

        public static OptionSetValue GetOptionSetValueValue(Entity entity, Entity preEntity, string attributeName)
        {
            if (entity.Attributes.Contains(attributeName))
            {
                return (OptionSetValue)entity[attributeName];
            }
            if (preEntity.Attributes.Contains(attributeName))
            {
                return (OptionSetValue)preEntity[attributeName];
            }
            return null;
        }

        public static string GetOptionSetValueText(Entity entity, Entity preEntity, string attributeName)
        {
            if (entity.Attributes.Contains(attributeName))
            {
                return entity.FormattedValues[attributeName];
            }
            return preEntity.Attributes.Contains(attributeName) ? preEntity.FormattedValues[attributeName] : string.Empty;
        }

        public static DateTime? GetDateTimeValue(Entity entity, Entity preEntity, string attributeName)
        {
            if (entity.Attributes.Contains(attributeName))
            {
                return (DateTime)entity[attributeName];
            }
            if (preEntity.Attributes.Contains(attributeName))
            {
                return (DateTime)preEntity[attributeName];
            }
            return null;
        }

        public static Money GetMoneyValue(Entity entity, Entity preEntity, string attributeName)
        {
            if (entity.Attributes.Contains(attributeName))
            {
                return (Money)entity[attributeName];
            }
            if (preEntity.Attributes.Contains(attributeName))
            {
                return (Money)preEntity[attributeName];
            }
            return null;
        }

        public static int? GetIntegerValue(Entity entity, Entity preEntity, string attributeName)
        {
            if (entity.Attributes.Contains(attributeName))
            {
                return (int)entity[attributeName];
            }
            if (preEntity.Attributes.Contains(attributeName))
            {
                return (int)preEntity[attributeName];
            }
            return null;
        }

        public static EntityReference GetEntityReferenceValue(Entity entity, Entity preEntity, string attributeName)
        {
            if (entity.Attributes.Contains(attributeName))
            {
                return (EntityReference)entity[attributeName];
            }
            if (preEntity.Attributes.Contains(attributeName))
            {
                return (EntityReference)preEntity[attributeName];
            }
            return null;
        }

        public static bool? GetBoolValue(Entity entity, Entity preEntity, string attributeName)
        {
            if (entity.Attributes.Contains(attributeName))
            {
                return (bool)entity[attributeName];
            }
            if (preEntity.Attributes.Contains(attributeName))
            {
                return (bool)preEntity[attributeName];
            }
            return null;
        }
        #endregion

        #region Entity Attribute & Value Setter

        public static void SetValue(Entity entity, string value, string attributeName, bool ifNullUpdateStill = true)
        {
            if (!ifNullUpdateStill && string.IsNullOrEmpty(value)) return;
            if (entity.Attributes.Contains(attributeName))
            {
                var currentValue = (string)entity[attributeName];
                if (!string.IsNullOrEmpty(value) && currentValue != value)
                {
                    entity[attributeName] = value;
                }
            }
            else
            {
                entity[attributeName] = value;
            }
        }

        public static void SetValue(Entity entity, OptionSetValue value, string attributeName, bool ifNullUpdateStill = true)
        {
            if (!ifNullUpdateStill && value == null) return;
            if (entity.Attributes.Contains(attributeName))
            {
                var currentValue = (OptionSetValue)entity[attributeName];
                if (value != null && currentValue.Value != value.Value)
                {
                    entity[attributeName] = value;
                }
                else
                {
                    entity[attributeName] = value;
                }
            }
            else
            {
                entity[attributeName] = value;
            }
        }

        public static void SetValue(Entity entity, DateTime value, string attributeName, bool ifNullUpdateStill = true)
        {
            if (!ifNullUpdateStill && value <= DateTime.MinValue && value >= DateTime.MaxValue) return;
            if (entity.Attributes.Contains(attributeName))
            {
                var currentValue = (DateTime)entity[attributeName];
                if (currentValue != value)
                {
                    entity[attributeName] = value;
                }
            }
            else
            {
                entity[attributeName] = value;
            }
        }

        public static void SetValue(Entity entity, int value, string attributeName, bool ifNullUpdateStill = true)
        {
            if (entity.Attributes.Contains(attributeName))
            {
                var currentValue = (int)entity[attributeName];
                if (currentValue != value)
                {
                    entity[attributeName] = value;
                }
            }
            else
            {
                entity[attributeName] = value;
            }
        }

        public static void SetValue(Entity entity, bool value, string attributeName, bool ifNullUpdateStill = true)
        {
            if (entity.Attributes.Contains(attributeName))
            {
                var currentValue = (bool)entity[attributeName];
                if (currentValue != value)
                {
                    entity[attributeName] = value;
                }
            }
            else
            {
                entity[attributeName] = value;
            }
        }

        public static void SetValue(Entity entity, EntityReference value, string attributeName, bool ifNullUpdateStill = true)
        {
            if (!ifNullUpdateStill && value == null) return;
            if (entity.Attributes.Contains(attributeName))
            {
                var currentValue = (EntityReference)entity[attributeName];
                if (currentValue.Id != value.Id)
                {
                    entity[attributeName] = value;
                }
            }
            else
            {
                entity[attributeName] = value;
            }
        }

        public static void SetValue(Entity entity, Money value, string attributeName, bool ifNullUpdateStill = true)
        {
            if (!ifNullUpdateStill && value == null) return;
            if (entity.Attributes.Contains(attributeName))
            {
                var currentValue = (Money)entity[attributeName];
                if (currentValue.Value != value.Value)
                {
                    entity[attributeName] = value;
                }
            }
            else
            {
                entity[attributeName] = value;
            }
        }
        #endregion
    }
}
