﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using TCT.D365.BlueSecurity.Plugins.Manager;
using System;


namespace TCT.D365.BlueSecurity.Plugins
{
    public class UpdateWorkOrderAllTaskDone : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            var context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            var service = factory.CreateOrganizationService(context.UserId);


            Entity entity = (Entity)context.InputParameters["Target"];
            var preEntity = context.PreEntityImages["Image"];
            var postEntity = context.PostEntityImages["Image"];

            if (context.Depth > 1) return;

            try
            {
                int systemstatusvalue = 0;

                tracingService.Trace("Start");

                OptionSetValue systemstatus = new OptionSetValue();
                
                if (entity.Attributes.Contains("msdyn_systemstatus")) {
                    systemstatus = (OptionSetValue)entity.Attributes["msdyn_systemstatus"];

                    systemstatusvalue = systemstatus.Value;
                    tracingService.Trace("29");
                };

                // System status Open - Completed

                if (systemstatusvalue == 690970003)
                {
                    tracingService.Trace("37");
                    // retrieve work order incident lookup
                    string workOrderIncidentFetchXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>" +
                                                                         "<entity name='msdyn_workorderincident'>" +
                                                                             "<attribute name='msdyn_name' />" +
                                                                             "<attribute name='msdyn_workorder' />" +
                                                                             "<attribute name='msdyn_workorderincidentid' />" +
                                                                             "<attribute name='msdyn_taskspercentcompleted' />" +
                                                                             "<attribute name='msdyn_incidentresolved' />" +
                                                                             "<order attribute='msdyn_name' descending='false' />" +
                                                                               "<filter type='and'>" +
                                                                                   "<condition attribute='msdyn_workorder' operator='eq' uitype='msdyn_workorder' value='{" + entity.Id + "}' />" +
                                                                               "</filter>" +
                                                                         "</entity>" +
                                                                     "</fetch>";

                    EntityCollection resultCollection = service.RetrieveMultiple(new FetchExpression(workOrderIncidentFetchXml.ToString()));
                    tracingService.Trace("53");

                    if (resultCollection.Entities.Count > 0)
                    {


                        Entity workOrderIncident = resultCollection.Entities[0];
                        tracingService.Trace("60");

                        bool incidentResolved = false;
                        incidentResolved = (bool)workOrderIncident["msdyn_incidentresolved"];


                        //1.If Work INcident "Tasks Completed = 100%"

                        if (incidentResolved == true)
                        {
                            //2.Update WorkOrder "All tasks DOne" field to Yes / True
                            tracingService.Trace("69");
                            entity.Attributes["tct_allservicetasksdone"] = true;
                            service.Update(entity);
                            tracingService.Trace("72");
                        }
                        tracingService.Trace("74");

                    }
                }
          

                // System status Closed - Posted

                EntityReference workordertype = (AttributeManager.GetEntityReferenceValue(entity, preEntity, "msdyn_workordertype"));
                tracingService.Trace("93");
                string workordertypename = string.Empty;

                if (workordertype!=null)
                {
                    workordertypename = workordertype.Name;
                }

                tracingService.Trace("101*");
                if (systemstatusvalue == 690970004 && workordertypename == "Repairs")
                {
                    tracingService.Trace("104");
                    Guid customerId = (AttributeManager.GetEntityReferenceValue(entity, preEntity, "msdyn_serviceaccount")).Id;
                    tracingService.Trace("105");
                    string entitytype = (AttributeManager.GetEntityReferenceValue(entity, preEntity, "msdyn_serviceaccount")).LogicalName;
                    tracingService.Trace("107");
                    Guid pricelistId = (AttributeManager.GetEntityReferenceValue(entity, preEntity, "msdyn_pricelist")).Id;
                    tracingService.Trace("109");
                    EntityReference order = AttributeManager.GetEntityReferenceValue(entity, preEntity, "tct_order");
                    tracingService.Trace("111");
                    // Create an invoice
                    Entity invoice = new Entity("invoice");
                    tracingService.Trace("105");
                    invoice["customerid"] = new EntityReference(entitytype, customerId);
                    invoice["pricelevelid"] = new EntityReference("pricelevel", pricelistId);
                    invoice["tct_workorder"] = new EntityReference(entity.LogicalName, entity.Id);
                    if (order != null)
                    {
                        invoice["salesorderid"] = new EntityReference("salesorder", order.Id);
                    }

                    tracingService.Trace("106");
                    var invoiceGuid = service.Create(invoice);

                    // Create Invoice Product - W.O Product
                    tracingService.Trace("118");
                    string productFetchXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>" +
                                                                     "<entity name='msdyn_workorderproduct'>" +
                                                                         "<attribute name='msdyn_unit' />" +
                                                                         "<attribute name='msdyn_name' />" +
                                                                         "<attribute name='msdyn_lineorder' />" +
                                                                         "<attribute name='msdyn_workorderproductid' />" +
                                                                         "<order attribute='msdyn_name' descending='false' />" +
                                                                           "<filter type='and'>" +
                                                                               "<condition attribute='msdyn_workorder' operator='eq' uitype='msdyn_workorder' value='{" + entity.Id + "}' />" +
                                                                           "</filter>" +
                                                                     "</entity>" +
                                                                 "</fetch>";

                    EntityCollection productResultCollection = service.RetrieveMultiple(new FetchExpression(productFetchXml.ToString()));
                    tracingService.Trace("133");
                    if (productResultCollection.Entities.Count > 0)
                    {
                        Guid productGuid = new Guid();

                        foreach (var productFetch in productResultCollection.Entities)
                        {
                            productGuid = (Guid)productFetch["msdyn_workorderproductid"];

                            Entity invoiceProduct = new Entity("invoicedetail");
                            invoiceProduct["msdyn_workorderid"] = new EntityReference("msdyn_workorder", entity.Id);
                            invoiceProduct["msdyn_workorderproductid"] = new EntityReference("msdyn_workorderproduct", productGuid);
                            invoiceProduct["productname"] = (string)productFetch["msdyn_name"];
                            invoiceProduct["msdyn_lineorder"] = (int)productFetch["msdyn_lineorder"];
                            invoiceProduct["invoiceid"] = new EntityReference("invoice", invoiceGuid);
                            //invoiceProduct["priceperunit"] = new EntityReference("invoice", invoiceGuid);
                            //invoiceProduct["quantity"] = new EntityReference("invoice", invoiceGuid);
                            //invoiceProduct["baseamount"] = new EntityReference("invoice", invoiceGuid);
                            //invoiceProduct["extendedamount"] = new EntityReference("invoice", invoiceGuid);
                            //invoiceProduct["tax"] = new EntityReference("invoice", invoiceGuid);

                            service.Create(invoiceProduct);
                        }
                    }

                    // Create Invoice Product - W.O Service
                }
            

            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException("An error occurred in the UpdateWorkOrderAllTaskDone plug-in.", ex);
            }
        }
    }
}
