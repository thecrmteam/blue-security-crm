﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TCT.D365.BlueSecurity.Plugins
{
    public sealed partial class GetUseronCounter : CodeActivity
    {

        [RequiredArgument]
        [Input("Counter")]
        [ReferenceTarget("tct_assignmentcounter")]
        public InArgument<EntityReference> InputEntity { get; set; }

        [Output("user")]
        [ReferenceTarget("systemuser")]
        public OutArgument<EntityReference> OutputEntity { get; set; }

        [RequiredArgument]
        [Input("Territory")]
        [ReferenceTarget("territory")]
        public InArgument<EntityReference> TerritoryInput { get; set; }

        [Output("NewCounter")]
        public OutArgument<int> OutputCounter { get; set; }


        //[RequiredArgument]
        [Input("CheckUser")]
        public InArgument<bool> InputCheckUser { get; set; }

        protected override void Execute(CodeActivityContext executionContext)
        {
            try
            {
                IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();

                IOrganizationServiceFactory serviceFactory =
                  executionContext.GetExtension<IOrganizationServiceFactory>();

                IOrganizationService service =
                    serviceFactory.CreateOrganizationService(context.UserId);

                ITracingService tracingService = (ITracingService)executionContext.GetExtension<ITracingService>();

                Guid userId = context.InitiatingUserId;
                tracingService.Trace($"userId: {userId}");

                // Bypass the auto-assign rep rotation if User is a Sales Consultant 

                string territoryMember = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>" +
                                              "<entity name='territory'>" +
                                                  "<attribute name='name' />" +
                                                  "<attribute name='managerid' />" +
                                                  "<attribute name='territoryid' />" +
                                                  "<order attribute='name' descending='false' />" +
                                                     "<link-entity name='systemuser' from='territoryid' to='territoryid' link-type='inner' alias='ab'>" +
                                                      "<filter type='and'>" +
                                                          "<condition attribute='systemuserid' operator='eq'  uitype='systemuser' value='{" + userId + "}' />" +
                                                      "</filter>" +
                                                     "</link-entity>" +
                                              "</entity>" +
                                          "</fetch>";


                var fetchMemberResult = service.RetrieveMultiple(new FetchExpression(territoryMember));

                tracingService.Trace($"fetchMemberResult: {fetchMemberResult.Entities.Count}");

                //
                var checkUser = this.InputCheckUser.Get<bool>(executionContext);

                if (checkUser == true)
                {
                    if (fetchMemberResult.Entities.Count > 0)
                    {
                        return;
                    }
                }

                  

                // Get sub-sector name
                var subsector = this.TerritoryInput.Get<EntityReference>(executionContext);

                // Retrieve sub-sector parent
                Entity subsectorRecord = service.Retrieve(subsector.LogicalName, subsector.Id, new ColumnSet("parentterritoryid"));

                var parentSector = (EntityReference)subsectorRecord["parentterritoryid"];

                var territoryName = parentSector.Name;

                string sectorTrim = Regex.Replace(territoryName, @"\s+", "");
                var territoryNameTrim = sectorTrim.ToLower();

                var currentCounter = string.Empty;

                if (territoryNameTrim == "sector1" || territoryNameTrim == "sector5")
                {
                    currentCounter = "tct_sector_1_5_participant";

                    territoryName = "Sector 1 + 5";
                }

                else if (territoryNameTrim == "sector4" || territoryNameTrim == "sector9")
                {
                    currentCounter = "tct_sector_4_9_participant";

                    territoryName = "Sector 4 + 9";
                }

                else
                {
                    currentCounter = "tct_" + territoryNameTrim + "participant";
                }

                tracingService.Trace($"territoryName {territoryName}");

                //Get entity from InArgument
                Entity counter = service.Retrieve("tct_assignmentcounter",
                   this.InputEntity.Get(executionContext).Id, new ColumnSet("" + currentCounter + ""));

                if (counter.Contains("" + currentCounter + ""))
                {
                    //Fetch xml to get users of a team. Here one user will be returned per page and page position will be
                    //decided on current counter value.
                    EntityCollection users = new EntityCollection();

                    var pagePosition = (int)counter["" + currentCounter + ""];


                    users = GetTeamMembers(service, pagePosition, territoryName);

                    tracingService.Trace($"Users count: {users.TotalRecordCount}");

                    if (users != null && users.TotalRecordCount > 0)
                    {
                        
                        this.OutputEntity.Set(executionContext, new EntityReference("systemuser", new Guid(users[0].Attributes["systemuserid"].ToString())));
                        tracingService.Trace($"User Name 1: {users[0].Attributes["fullname"]}");

                        int newcounter = 1;
                        //Counter value should not exceed to total member count of team, in case if it exceeds
                        //that means user has been deleted from team.
                        if (pagePosition >= users.TotalRecordCount)
                        {
                            //Set default value 1
                            newcounter = 1;
                        }
                        else
                        {
                            //Increment by 1 current counter value
                            newcounter = pagePosition + 1;

                        }

                        //Set out argument OutputCounter- new counter value, that will be further
                        //used by workflow to update counter entity.
                        this.OutputCounter.Set(executionContext, newcounter);

                        // UPDATE Counter 
                        counter["" + currentCounter + ""] = newcounter;
                        service.Update(counter);

                    }                  

                }

            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException("GetUseronCounter ERROR>>>>>: " + ex.StackTrace.ToString(), ex);
            }
        }

        //private static bool UserHasRole(IOrganizationService service, Guid userId, string roleName)
        //{
        //    bool hasRole = false;
        //    QueryExpression qe = new QueryExpression("systemuserroles");
        //    qe.Criteria.AddCondition("systemuserid", ConditionOperator.Equal, userId);

        //    LinkEntity link = qe.AddLink("role", "roleid", "roleid", JoinOperator.Inner);
        //    link.LinkCriteria.AddCondition("name", ConditionOperator.Equal, roleName);

        //    EntityCollection results = service.RetrieveMultiple(qe);

        //    hasRole = results.Entities.Count > 0;

        //    return hasRole;
        //}

        //private static bool UserIsTeamManager(IOrganizationService service, Guid retrieveUserId, string territoryName)
        //{
        //    bool isManager = false;
        //    string result = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
        //                        "<entity name='territory'>" +
        //                            "<attribute name='name' />" +
        //                            "<attribute name='managerid' />" +
        //                            "<attribute name='territoryid' />" +
        //                            "<order attribute='name' descending='false' />" +
        //                             "<filter type='and'>" +
        //                               "<condition attribute='name' operator='eq' value='{" + territoryName + "}' />" +
        //                               "<condition attribute='managerid' operator='eq' uitype='systemuser' value='{" + retrieveUserId + "}' />" +
        //                             "</filter>" +
        //                           "</entity>" +
        //                         "</fetch>";

        //    var results = service.RetrieveMultiple(new FetchExpression(result));

        //    isManager = results.Entities.Count > 0;


        //    return isManager;
        //}

        public static EntityCollection GetTeamMembers(IOrganizationService service, int counter, string territoryName)
        {
            var fetchXml = new StringBuilder();
            fetchXml.Append("<fetch version=\"1.0\"  mapping=\"logical\" distinct=\"true\" page=\"" + counter + "\" count=\"1\" returntotalrecordcount=\"true\">");
            fetchXml.Append("   <entity name=\"systemuser\">");
            fetchXml.Append("       attribute name=\"systemuserid\" />");
            fetchXml.Append("       <order attribute=\"fullname\" descending=\"false\" />");
            fetchXml.Append("       <link-entity name=\"territory\" from=\"territoryid\" to=\"territoryid\" alias=\"ab\">");
            fetchXml.Append("       	<filter type=\"and\">");
            fetchXml.Append("           	<condition attribute=\"name\" operator=\"eq\" value=\"" + territoryName + "\" />");
            fetchXml.Append("       	</filter>");
            fetchXml.Append("       </link-entity>");
            fetchXml.Append("   </entity>");
            fetchXml.Append("</fetch>");
            var entityCollection = service.RetrieveMultiple(new FetchExpression(fetchXml.ToString()));
            return entityCollection;
        }

    }
}

