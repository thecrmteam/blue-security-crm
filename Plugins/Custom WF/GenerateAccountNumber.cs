﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Diagnostics;

namespace TCT.D365.BlueSecurity.Plugins.Custom_WF
{
    public class GenerateAccountNumber : CodeActivity
    {
        [ReferenceTarget("account")]
        [Input("Account")]
        public InArgument<EntityReference> AccountInput { get; set; }

        [Input("Counter")]
        [ReferenceTarget("tct_numbergenerator")]
        public InArgument<EntityReference> counterInput { get; set; }

        [Output("Increment Counter?")]
        public OutArgument<bool> incrementCounter { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        protected override void Execute(CodeActivityContext context)
        {
            ITracingService tracingService = (ITracingService)context.GetExtension<ITracingService>();
            IWorkflowContext workflowContext = (IWorkflowContext)context.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)context.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(workflowContext.UserId);

            try
            {
                Guid entityId = workflowContext.PrimaryEntityId;
                var entityLogicalName = workflowContext.PrimaryEntityName;

                // 1 Get counter value
                Entity counterEntity = service.Retrieve("tct_numbergenerator", this.counterInput.Get(context).Id, new ColumnSet("tct_autonumber"));

                int accountNumber = int.Parse(counterEntity.Attributes["tct_autonumber"].ToString());

                // 2 Add zeros 
                string calcAccountNumber = string.Empty;

                string zeros = string.Empty;

                switch (accountNumber.ToString().Length)
                {
                    case 0:
                        zeros = "0000000";
                        break;

                    case 1:
                        zeros = "000000";
                        break;

                    case 2:
                        zeros = "00000";
                        break;

                    case 3:
                        zeros = "0000";
                        break;

                    case 4:
                        zeros = "000";
                        break;

                    case 5:
                        zeros = "00";
                        break;
                }

                // 3 Get Customer last name 3 letters (Account/Contact)
                tracingService.Trace($"Line80");
                //Entity entityRecord = service.Retrieve(entityLogicalName, entityId, new ColumnSet("customerid"));
                //EntityReference customerER = (EntityReference)entityRecord["customerid"];

                var CustomerAccountER = this.AccountInput.Get<EntityReference>(context);

                Entity CustomerAccountRecord = service.Retrieve(CustomerAccountER.LogicalName, CustomerAccountER.Id, new ColumnSet("tct_accounttype", "accountnumber", "name", "tct_lastname"));
                int AccountType = ((OptionSetValue)CustomerAccountRecord["tct_accounttype"]).Value;

                tracingService.Trace($"AccountType: {AccountType}");

                string checkCustomerType = CustomerAccountER.LogicalName;
                tracingService.Trace($"Customer Record logical Name: {checkCustomerType}");

                string accountName = string.Empty;
                string accountLastName = string.Empty;
                string customerAccountNumber = string.Empty;
                string first3Letters = string.Empty;

                // Check if Account Number is empty
                customerAccountNumber = CustomerAccountRecord.GetAttributeValue<string>("accountnumber");

                if (customerAccountNumber != null)
                {
                    this.incrementCounter.Set(context, false);
                    return;
                }

                this.incrementCounter.Set(context, true);

                accountName = CustomerAccountRecord.GetAttributeValue<string>("name");

                Entity accountEntity = new Entity("account");

                // Commercial
                if (AccountType == 2) 
                {
  
                    accountName = (CustomerAccountRecord.Attributes.Contains("name")
                      && CustomerAccountRecord.Attributes["name"].ToString().Length > 0) ? CustomerAccountRecord.Attributes["name"].ToString() : string.Empty;

                    tracingService.Trace($"Line104");
                    if (accountName.Length>2)
                    {
                        first3Letters = accountName.Substring(0, 3);
                    }

                    else
                    {
                        first3Letters = accountName;
                    }
               
                    first3Letters = first3Letters.ToUpper();

                    calcAccountNumber = first3Letters + zeros + accountNumber;
                    tracingService.Trace($"calcAccountNumber: {calcAccountNumber}");

                    // Update Account
                    tracingService.Trace($"GUID: {CustomerAccountER.Id}");
                    Guid accountGuid = new Guid(CustomerAccountER.Id.ToString());

                    accountEntity.Id = CustomerAccountER.Id;
                    accountEntity.Attributes["accountnumber"] = calcAccountNumber;
                    service.Update(accountEntity);
                    tracingService.Trace($"Line128");
                }

                // Residential
                else if (AccountType == 1) 
                {
                    // Get last name

                    //Entity contactEntity = service.Retrieve("contact", customerER.Id, new ColumnSet("lastname", "tct_accountnumber"));

                    //accountLastName = CustomerAccountRecord.GetAttributeValue<string>("tct_lastname");

                    tracingService.Trace($"Line116");
                    accountLastName = (CustomerAccountRecord.Attributes.Contains("tct_lastname")
                        && CustomerAccountRecord.Attributes["tct_lastname"].ToString().Length > 0) ? CustomerAccountRecord.Attributes["tct_lastname"].ToString() : string.Empty;

                    if (accountLastName.Contains("-"))
                    {
                        string[] pieces = accountLastName.Split(new char[] { '-' });
                        first3Letters = pieces[0].Substring(0, 3);
                    }
                    else if (accountLastName.Contains(" "))
                    {
                        string[] pieces = accountLastName.Split(new char[] { ' ' });
                        first3Letters = pieces[pieces.Length - 1].Substring(0, 3);
                    }
                    else
                    {
                        first3Letters = accountLastName.Substring(0, 3);
                    }

                    tracingService.Trace($"Line135");
                    first3Letters = first3Letters.ToUpper();

                    calcAccountNumber = first3Letters + zeros + accountNumber;

                    // Update Account
                    accountEntity.Id = CustomerAccountER.Id;
                    accountEntity.Attributes["accountnumber"] = calcAccountNumber;
                    service.Update(accountEntity);
                    tracingService.Trace($"Line167");

                }

                // 4 Update Customer Account Number

                // 5 Increment counter value
            }
            catch (Exception ex)
            {

                throw new InvalidPluginExecutionException("An error occured in the GenerateAccountNumber", ex);
            }
        }
    }
}
