﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Diagnostics;

namespace TCT.D365.BlueSecurity.Plugins.Custom_WF
{
    public sealed partial class GenerateWorkOrder:CodeActivity
    {
        [ReferenceTarget("account")]
        [Input("Account")]
        public InArgument<EntityReference> AccountInput { get; set; }

        [ReferenceTarget("pricelevel")]
        [Input("PriceList")]
        public InArgument<EntityReference> PriceListInput { get; set; }

        protected override void Execute(CodeActivityContext executionContext)
        {
            try
            {
                IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();

                IOrganizationServiceFactory serviceFactory =
                  executionContext.GetExtension<IOrganizationServiceFactory>();

                IOrganizationService service =
                    serviceFactory.CreateOrganizationService(context.UserId);

                ITracingService tracingService = (ITracingService)executionContext.GetExtension<ITracingService>();

                var defaultAccount = this.AccountInput.Get<EntityReference>(executionContext);

                var customerER = new EntityReference();

                Guid entityId = context.PrimaryEntityId;
                var entityLogicalName = context.PrimaryEntityName;
                tracingService.Trace("40");
                // Order
                if (entityLogicalName == "salesorder")
                {
                    Entity entityRecord = service.Retrieve(entityLogicalName, entityId, new ColumnSet("customerid", "pricelevelid", "description","tct_casetypelist","tct_casesubtypelist"));
                    tracingService.Trace("45");
                    if (entityRecord.Contains("customerid") && entityRecord["customerid"] != null)
                    {
                        EntityReference ordercustomerER = (EntityReference)entityRecord["customerid"];

                        EntityReference priceList = (EntityReference)entityRecord["pricelevelid"];

                        int caseTypeListvalue = 0;
                        int caseSubTypeListvalue = 0;

                        OptionSetValue caseTypeList = new OptionSetValue();
                        OptionSetValue caseSubTypeList = new OptionSetValue();

                        if (entityRecord.Attributes.Contains("tct_casetypelist"))
                        {
                            caseTypeList = (OptionSetValue)entityRecord.Attributes["tct_casetypelist"];

                            caseTypeListvalue = caseTypeList.Value;
                        };

                        if (entityRecord.Attributes.Contains("tct_casesubtypelist"))
                        {
                            caseSubTypeList = (OptionSetValue)entityRecord.Attributes["tct_casesubtypelist"];

                            caseSubTypeListvalue = caseSubTypeList.Value;
                        };
                        //EntityReference casetypeER = new EntityReference() ;
                        //casetypeER = entityRecord.GetAttributeValue<EntityReference>("tct_casetype");



                        //EntityReference casesubtypeER = new EntityReference();
                        //casesubtypeER = entityRecord.GetAttributeValue<EntityReference>("tct_casesubtype");

                        var description = string.Empty;
                        tracingService.Trace("57");
                        description = (entityRecord.Attributes.Contains("description")) ? entityRecord["description"].ToString() : string.Empty;

                    
                        var checkCustomer = ordercustomerER.LogicalName;

                        if (checkCustomer == "contact")
                        {
                            customerER = this.AccountInput.Get<EntityReference>(executionContext);
                        }
                        
                        else
                        {
                            customerER = new EntityReference("account", ordercustomerER.Id);
                        }
                        tracingService.Trace("72");
                        // CREATE NEW WORK ORDER

                        Entity newWordOrder = new Entity("msdyn_workorder");

                        newWordOrder["msdyn_serviceaccount"] = new EntityReference("account", customerER.Id);
                        //newWordOrder["tct_servicecustomer"] = new EntityReference(checkCustomer, ordercustomerER.Id);
                        newWordOrder["msdyn_pricelist"] = new EntityReference("pricelevel", priceList.Id);
                        newWordOrder["tct_order"] = new EntityReference("salesorder", entityId);
                        newWordOrder["tct_casetypelist"] = new OptionSetValue(caseTypeListvalue);
                        newWordOrder["tct_casesubtypelist"] = new OptionSetValue(caseSubTypeListvalue);
                        //if (casetypeER!=null)
                        //{
                        //    tracingService.Trace("83");
                        //    newWordOrder["tct_casetype"] = new EntityReference("tct_casetype", casetypeER.Id);
                        //}
                        //if (casesubtypeER!=null)
                        //{
                        //    tracingService.Trace("88");
                        //    newWordOrder["tct_casesubtype"] = new EntityReference("tct_casetype", casesubtypeER.Id);
                        //}
                        tracingService.Trace("89");
                        newWordOrder["tct_technicalnotes"] = description;

                        var Workorder = service.Create(newWordOrder);

                        // RETRIEVE ALL PRODUCTS FROM ORDER

                        string productFetchXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>" +
                                                                      "<entity name='salesorderdetail'>" +
                                                                          "<attribute name='productid' />" +
                                                                          "<attribute name='priceperunit' />" +
                                                                          "<attribute name='quantity' />" +
                                                                          "<attribute name='extendedamount' />" +
                                                                          "<attribute name='salesorderdetailid' />" +
                                                                          "<order attribute='productid' descending='false' />" +
                                                                            "<filter type='and'>" +
                                                                                "<condition attribute='salesorderid' operator='eq' uitype='salesorder' value='{" + entityRecord.Id + "}' />" +
                                                                            "</filter>" +
                                                                             "<link-entity name='product' from='productid' to='productid' visible='false' link-type='outer' alias='a_53d1d1017db2ea11a812000d3a25b83c'>" +
                                                                                "<attribute name='msdyn_fieldserviceproducttype' />" +
                                                                            "</link-entity>"+
                                                                      "</entity>" +
                                                                  "</fetch>";

                        EntityCollection resultCollection = service.RetrieveMultiple(new FetchExpression(productFetchXml.ToString()));

                        if (resultCollection.Entities.Count > 0)
                        {
                            EntityReference productER = new EntityReference();
                            Entity product = new Entity();
                            OptionSetValue productType = new OptionSetValue();
                            int productTypeValue = 0;
                            Decimal quantity = 0;

                            foreach (var productFetch in resultCollection.Entities)
                            {
                                // CREATE NEW WORK ORDER PRODUCT 

                                productER = (EntityReference)productFetch["productid"];
                                quantity = (Decimal)productFetch["quantity"];

                                product = service.Retrieve(productER.LogicalName, productER.Id, new ColumnSet("msdyn_fieldserviceproducttype"));
                                productType = (OptionSetValue)product.Attributes["msdyn_fieldserviceproducttype"];

                                productTypeValue = productType.Value;

                                // inventory and non-inventory product
                                if (productTypeValue == 690970000 || productTypeValue == 690970001)
                                {
                                    var newWorkOrderProduct = new Entity("msdyn_workorderproduct");
                                    newWorkOrderProduct["msdyn_name"] = productER.Name;
                                    newWorkOrderProduct["msdyn_product"] = new EntityReference("product", productER.Id);
                                    newWorkOrderProduct["msdyn_workorder"] = new EntityReference("msdyn_workorder", Workorder);
                                    newWorkOrderProduct["msdyn_totalamount"] = (Money)productFetch["extendedamount"];
                                    //newWorkOrderProduct["msdyn_quantity"] = (Double)quantity; 
                                    newWorkOrderProduct["msdyn_estimatequantity"] = (Double)quantity; //msdyn_estimatequantity
                                    service.Create(newWorkOrderProduct);
                                }
                               
                                // service product
                                else if (productTypeValue == 690970002)
                                {
                                    var newWorkOrderService = new Entity("msdyn_workorderservice");
                                    newWorkOrderService["msdyn_name"] = productER.Name;
                                    newWorkOrderService["msdyn_service"] = new EntityReference("product", productER.Id);
                                    newWorkOrderService["msdyn_workorder"] = new EntityReference("msdyn_workorder", Workorder);
                                    newWorkOrderService["msdyn_totalamount"] = (Money)productFetch["extendedamount"];

                                    service.Create(newWorkOrderService);
                                }
                               

                            }
                        }

                    }

                    else
                    {
                        throw new InvalidPluginExecutionException("Potential Customer field cannot be empty");
                    } 
                }


                // Case

                else if (entityLogicalName == "incident")
                {
                    Entity entityRecord = service.Retrieve(entityLogicalName, entityId, new ColumnSet("customerid"));

                    var servicePriceList = this.PriceListInput.Get<EntityReference>(executionContext);

                    if (entityRecord.Contains("customerid") && entityRecord["customerid"] != null)
                    {
                        EntityReference casecustomerER = (EntityReference)entityRecord["customerid"];

                        var checkCustomer = casecustomerER.LogicalName;

                        if (checkCustomer == "contact")
                        {
                            customerER = this.AccountInput.Get<EntityReference>(executionContext);
                        }

                        else
                        {
                            customerER = new EntityReference("account", casecustomerER.Id);
                        }

                        // CREATE NEW WORK ORDER 6b54d236-11bd-ea11-a812-000d3aab18bd
                        Guid priceList = new Guid("6b54d236-11bd-ea11-a812-000d3aab18bd");

                        Entity newWordOrder = new Entity("msdyn_workorder");

                        newWordOrder["msdyn_serviceaccount"] = new EntityReference("account", customerER.Id);
                        newWordOrder["tct_servicecustomer"] = new EntityReference(checkCustomer, casecustomerER.Id);
                        newWordOrder["msdyn_pricelist"] = new EntityReference("pricelevel", servicePriceList.Id);
                        newWordOrder["msdyn_servicerequest"] = new EntityReference("incident", entityId);
                        // appoitnment time

                        var Workorder = service.Create(newWordOrder);
                    }

                    else
                    {
                        throw new InvalidPluginExecutionException("Potential Customer field cannot be empty");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException("GenerateWorkOrder ERROR>>>>>: " + ex.StackTrace.ToString(), ex);
            }
        }
    }
}
