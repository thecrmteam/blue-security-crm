﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using TCT.D365.BlueSecurity.Plugins.Manager;

namespace TCT.D365.BlueSecurity.Plugins.Custom_WF
{
    public sealed partial class VerifyBankAccount: CodeActivity
    {
        [RequiredArgument]
        [Input("Branch Code")]
        public InArgument<string> BranchCodeInput { get; set; }

        [RequiredArgument]
        [Input("Account Number")]
        public InArgument<string> AccountNumberInput { get; set; }

        [Output("Linkser message")]
        public OutArgument<string> LinkserMessageOutput { get; set; }
        protected override void Execute(CodeActivityContext executionContext)
        {
            try
            {
                IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();

                IOrganizationServiceFactory serviceFactory =
                  executionContext.GetExtension<IOrganizationServiceFactory>();

                IOrganizationService service =
                    serviceFactory.CreateOrganizationService(context.UserId);

                ITracingService tracingService = (ITracingService)executionContext.GetExtension<ITracingService>();

                // GET BANK ACCOUNT DETAILS
                var branchCode = this.BranchCodeInput.Get<string>(executionContext);
                var accountNumber = this.AccountNumberInput.Get<string>(executionContext);


                // CALL LINKSER TO VALIDATE BANK ACCOUNT
                var accountVerificationCodeResponse = LinkservWebServiceManager.ValidateSouthAfricanAccountNumber(branchCode, accountNumber);
                //tracingService.Trace($"accountVerificationResponse:{accountVerificationCodeResponse}");

                // CONVERT LINKSERV CODE RESPONSE TO DESCRIPTION
                var accountVerificationDescription = string.Empty;

                switch (accountVerificationCodeResponse)
                {
                    case "0":
                        accountVerificationDescription = "Account Passed";
                        break;

                    case "4":
                        accountVerificationDescription = "Branch Code Not Found";
                        break;

                    case "8":
                        accountVerificationDescription = "Credit Card Number Invalid";
                        break;

                    case "10":
                        accountVerificationDescription = "Invalid Expiry Date";
                        break;

                    case "50":
                        accountVerificationDescription = "Account Number Error";
                        break;

                    default:
                        accountVerificationDescription = string.Empty;
                        break;
                }

                // UPDATE ENTITY WITH LINKSERV MESSAGE
                this.LinkserMessageOutput.Set(executionContext, accountVerificationDescription);

            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException("VerifyBankAccount ERROR>>>>>: " + ex.StackTrace.ToString(), ex);
            }
        }
    }
}
